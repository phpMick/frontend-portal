<?php

namespace App\Observers;

use App\Models\User;
use App\Events\UserCreated;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
        event(new UserCreated($user));
        session()->flash('success', 'User created');
        writeJSONLogInfo('user-created',['user_id' => $user->id]);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        session()->flash('success', 'User updated');
        writeJSONLogInfo('user-updated',['user_id' => $user->id]);
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {
        session()->flash('success', "User {$user->name} deleted");
        writeJSONLogInfo('user-deleted',['user_id' => $user->id]);
    }


}
