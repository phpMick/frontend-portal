<?php

namespace App\Observers;

use App\Models\Tenant;


class TenantObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param Tenant $tenant
     * @return void
     */
    public function created(Tenant $tenant)
    {
        session()->flash('success', 'Tenant created');
        writeJSONLogInfo('tenant-created',['tenant_id' => $tenant->id]);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param Tenant $tenant
     * @return void
     */
    public function updated(Tenant $tenant)
    {
        session()->flash('success', 'Tenant updated');
        writeJSONLogInfo('tenant-updated',['tenant_id' => $tenant->id]);
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param Tenant $tenant
     * @return void
     */
    public function deleted(Tenant $tenant)
    {
        session()->flash('success', 'Tenant "' . $tenant->name . '" deleted');
        writeJSONLogInfo('tenant-deleted',['tenant_id' => $tenant->id]);
    }


}
