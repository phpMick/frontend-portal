<?php

namespace App\Observers;

use App\Models\Report;


class ReportObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param Report $report
     * @return void
     */
    public function created(Report $report)
    {

        session()->flash('success', 'Report created');
        writeJSONLogInfo('report-created',['report_id' => $report->id]);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param Report $report
     * @return void
     */
    public function updated(Report $report)
    {
        session()->flash('success', 'Report updated');
        writeJSONLogInfo('report-updated',['report_id' => $report->id]);
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param Report $report
     * @return void
     */
    public function deleted(Report $report)
    {
        session()->flash('success', 'Report "' . $report->name . '" deleted');
        writeJSONLogInfo('report-deleted',['report_id' => $report->id]);
    }


}
