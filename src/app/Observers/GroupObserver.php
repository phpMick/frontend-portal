<?php

namespace App\Observers;

use App\Models\Group;


class GroupObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param Group $group
     * @return void
     */
    public function created(Group $group)
    {
        session()->flash('success', 'Group created');
        writeJSONLogInfo('group-created',['group_id' => $group->id]);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param Group $group
     * @return void
     */
    public function updated(Group $group)
    {
        session()->flash('success', 'Group updated');
        writeJSONLogInfo('group-updated',['group_id' => $group->id]);
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param Group $group
     * @return void
     */
    public function deleted(Group $group)
    {
        session()->flash('success', 'Group "'.$group->name.'" deleted');
        writeJSONLogInfo('group-deleted',['group_id' => $group->id]);
    }


}
