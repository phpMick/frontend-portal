<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 10/09/2017
 * Time: 12:49
 */

namespace App\Connectors\MicrosoftAzureActiveDirectoryAuth;


use League\OAuth2\Client\Token\AccessToken;

class AuthState
{

    public $oauthCode = NULL;
    public $oauthCodeTime = 0;
    public $oauthState = NULL;
    public $intendedUrl = NULL;
    public $user = NULL;
    public $tokens = [];

    /**
     * @param $resourceId
     * @return AccessToken|null
     */
    public function getToken($resourceId) {
        if(array_key_exists($resourceId, $this->tokens)) {
            return $this->tokens[$resourceId];
        }
        return NULL;
    }

    public function setToken($resourceId, AccessToken $token) {
        $this->tokens[$resourceId] = $token;
    }

    public function deleteToken($resourceId) {
        if(array_key_exists($resourceId, $this->tokens)) {
            unset($this->tokens[$resourceId]);
        }
    }



}