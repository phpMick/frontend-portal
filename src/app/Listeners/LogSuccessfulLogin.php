<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    private $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     * Send new user email.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;

        writeJSONLogInfo('user-login',['userAgent' => $this->request->server('HTTP_USER_AGENT')]);

    }
}
