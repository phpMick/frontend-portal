<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Mail\NewUserMail;


use Illuminate\Support\Facades\Mail;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNewUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * Send new user email.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {

        $user = $event->user;

        //send email
        Mail::to($user)->send(new NewUserMail($user));


    }
}
