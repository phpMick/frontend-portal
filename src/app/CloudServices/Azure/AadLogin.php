<?php

namespace App\CloudServices\Azure;

//Laravel
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

//Models
use App\Models\User;

//Connectors
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;

class AadLogin
{
    protected $MAADA;

    /**
     * @var User
     */
    private $user;

    /**
     * AadLogin constructor.
     * @param MicrosoftAzureActiveDirectoryAuth $MAADA
     */
    public function __construct(MicrosoftAzureActiveDirectoryAuth $MAADA)
    {
        $this->MAADA = $MAADA;
    }

    /**
     * Called when we have logged in and  we have an auth code.
     * Gets the access token and sets it in _stateData
     * Initiates the customer and user checks.
     * @param $requestArray
     * @return mixed
     * @throws \Exception
     */
    public function attemptOAuthLogin($requestArray)
    {
        if ($this->LoginWithMSAllowed($requestArray)) {

            Auth::login($this->user);

            // ensure user is marked as active (if they came here as part of invite acceptance)
            // this ensures that they can't accept the invite and add a password later
            if ($this->user->status == User::STATUS_INVITED) {
                $this->user->status = User::STATUS_ACTIVE;
                $this->user->save();
            }

            return redirect()->route('home');

        } else {
            session(['message-title' => 'Microsoft login not allowed.']);
            session(['message-detail' => 'Please contact your administrator to activate this login type.']);
            return view('message-page');
        }
    }

    /**
     * Do we have a User record and do all their tenants
     * allow MS login
     * @param $requestArray
     * @return bool
     */
    private function LoginWithMSAllowed($requestArray)
    {
        $passed = FALSE;

        $userDetailsArray = $this->MAADA->getUserDetails($requestArray['code']);

        $this->user = User::where('email', '=', $userDetailsArray['email'])->first();

        if ($this->user) {
            $passed = $this->tenantsAllowMSLogin();
        }

        return $passed;
    }


    /**
     * Iterate through the users tenants and check if
     * they all allow_ms_login
     * @param $user
     * @return bool
     */
    private function tenantsAllowMSLogin()
    {
        $tenants = $this->user->tenants;

        if ($tenants->count() > 0) {
            foreach ($tenants as $tenant) {
                if ($tenant->allow_ms_login != TRUE) {
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }

        return TRUE;
    }

}