<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            $message = $exception->getMessage();
            $statusCode = 500;
            switch (get_class($exception)) {
                case 'Illuminate\Database\Eloquent\ModelNotFoundException':
                    $message = 'Object not found';
                    break;
                // others here...
            }
            return response()->json(['errors'=>[['code'=>$exception->getCode(), 'message'=>$message, 'type'=>get_class($exception)]]], $statusCode);
        }

        return parent::render($request, $exception);
    }
}
