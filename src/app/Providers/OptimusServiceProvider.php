<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jenssegers\Optimus\Optimus;

class OptimusServiceProvider extends ServiceProvider
{

    protected $prime = 944178197;
    protected $inverse = 2121444157;
    protected $xor = 120336823;

    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Optimus::class, function ($app) {
            return new Optimus($this->prime, $this->inverse, $this->xor);
        });
    }

    public function provides()
    {
        return [Optimus::class];
    }
}
