<?php

namespace App\Providers;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Optimus\Optimus;
use Ramsey\Uuid\Uuid;
use Tuupola\Base62;
use App\Models\User;
use App\Observers\UserObserver;
use App\Models\Group;
use App\Observers\GroupObserver;
use App\Models\Report;
use App\Observers\ReportObserver;
use App\Models\Tenant;
use App\Observers\TenantObserver;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        User::observe(UserObserver::class);
        Report::observe(ReportObserver::class);
        Tenant::observe(TenantObserver::class);
        Group::observe(GroupObserver::class);

        // set default length for indexes for older versions of MariaDB/MySQL
        // which do not support indexing utf8mb4 strings of length>191 characters:
        Schema::defaultStringLength(191);

        // force HTTPS in production:
        if($this->app->environment()=='production') {
            URL::forceScheme('https');
        }

        /**
         * CUSTOM VALIDATION RULES:
         */
        // add uuid validator for lumen<5.7
        app('validator')->extend('uuid', function ($attribute, $value, $parameters, $validator) {
            return Uuid::isValid($value);
        }, 'The provided :attribute is invalid');
        // obfuscated id exists
        app('validator')->extend('url_id', function ($attribute, $value, $parameters, $validator) {
            if (!array($parameters) OR count($parameters)==0) {
                throw new \Exception('Database table must be specified');
            }
            if (in_array($value, ['-', '+', '', '*'])) {
                return FALSE;
            }
            $decodedId = intval(app(Base62::class)->decode($value));
            $actualId = app(Optimus::class)->decode($decodedId);
            return app('db')->table($parameters[0])->where('id', '=', $actualId)->count() == 1;
        }, 'The selected :attribute is invalid');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
