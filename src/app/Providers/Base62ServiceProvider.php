<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Tuupola\Base62;

class Base62ServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Base62::class, function ($app) {
            return new Base62();
        });
    }

    public function provides()
    {
        return [Base62::class];
    }
}
