<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.nav.main-menu-user', function($view) {
            $view->with('reports', auth()->user()->allReports());
        });

        view()->composer('partials.nav.right-menu-user', function($view) {
            $view->with('allowedTenants', auth()->user()->allowedTenants());
        });



    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
