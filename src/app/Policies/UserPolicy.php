<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Customer;
use App\Models\UserGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    /**
     * Overrides the policy for Global Admins
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        return $user->isGlobalAdmin();
    }



    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return FALSE;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return FALSE;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return FALSE;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return FALSE;
    }


    /**
     * @param User $user
     * @return bool
     */
    public function edit(User $user)
    {
        return FALSE;
    }


    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return FALSE;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return FALSE;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function assignGroups(User $user)
    {
        return FALSE;
    }




}
