<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Tenant;

class TenantPolicy
{

    /**
     * Overrides the policy for Global Admins
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        // currently, only allow global admins to do anything with tenants
        return $user->isGlobalAdmin();
    }

    /**
     * Determine whether the view the index.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return FALSE;
    }

    /**
     * Determine whether the user can view the customer.
     *
     * @param User $user
     * @param Tenant $tenant
     * @return mixed
     */
    public function show(User $user, Tenant $tenant)
    {
        return FALSE;
    }

    /**
     * They need to be an admin to edit.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return FALSE;
    }

    /**
     * They need to be an admin to edit.
     *
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return FALSE;
    }

    /**
     * They need to be an admin to edit.
     *
     * @param User $user
     * @return bool
     */
    public function edit(User $user)
    {
        return FALSE;
    }

    /**
     * Determine whether the user can update the customer.
     * @param User $user
     * @param Tenant $tenant
     * @return mixed
     */
    public function update(User $user, Tenant $tenant)
    {
        return FALSE;
    }

    /**
     *
     * @param  User $user
     * @param Tenant $tenant
     * @return mixed
     */
    public function destroy(User $user, Tenant $tenant)
    {
        return FALSE;
    }

    public function allowMSLogin(User $user, Tenant $tenant)
    {
        return FALSE;
    }





}
