<?php

namespace App\Policies;

use App\Models\Report;
use App\Models\User;

class ReportPolicy
{
//    use HandlesAuthorization;

    /**
     * Overrides the policy for Global Admins
     * @param User $user
     * @param $ability
     * @return bool|NULL
     */
    public function before(User $user, $ability)
    {
        if ($user->isGlobalAdmin()) {
            return TRUE;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return $user->isGlobalAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->isGlobalAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->isGlobalAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function edit(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user, Report $report)
    {
        // check that the report's tenant is the same as the user's currently selected tenant
        // and that the user has been added to the report's group
        $selectedTenantId = session('selectedTenant');
        if (!isset($selectedTenantId)) {
            $selectedTenantId = -1;
        } else {
            $selectedTenantId = intval($selectedTenantId);
        }

        return ($report->group->tenant_id == $selectedTenantId) AND ($report->group->users()->where('id', '=', $user->id)->count() > 0);
    }


}
