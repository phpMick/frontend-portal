<?php

namespace App\Policies;

use App\Models\Group;
use App\Models\User;

class GroupPolicy
{
//    use HandlesAuthorization;


    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return $user->isGlobalAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->isGlobalAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->isGlobalAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function edit(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->isGlobalAdmin();
    }


    /**
     * @param User $user
     * @return bool
     */
    public function view(User $user, Group $report)
    {
        dd('add this');
    }


}
