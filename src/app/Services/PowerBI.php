<?php


namespace App\Services;


use App\Models\Report;
use Bytes\ApiConnector\ApiRecorder;
use Bytes\MicrosoftPowerBIApiConnector\MicrosoftPowerBIApiConnector;
use Bytes\MicrosoftPowerBIApiConnector\MicrosoftPowerBiAuthProvider;
use Illuminate\Cache\Repository;
use Illuminate\Filesystem\Filesystem;

class PowerBI
{
    /**
     * @var Repository
     */
    private $cache;

    /**
     * @var MicrosoftPowerBiAuthProvider
     */
    private $auth;

    /**
     * @var string
     */
    private $cachedToken;

    /**
     * @var MicrosoftPowerBIApiConnector
     */
    private $api;

    /**
     * @var ApiRecorder
     */
    private $recorder;

    /**
     * @var Filesystem
     */
    private $disk;

    /**
     * @var string 'read' or 'write' depending on whether we should be reading HTTP transactions from log files or writing HTTP transactions to log files
     */
    private $recorderMode = 'write';

    public function __construct($httpLogMode='write')
    {
        switch ($httpLogMode) {
            case 'read':
                $this->recorderMode = 'read';
                break;
            case 'write':
                $this->recorderMode = 'write';
                break;
            default:
                throw new \Exception('Invalid recorder mode requested');
        }
        $this->cache = app('cache');
        $this->auth = new MicrosoftPowerBiAuthProvider([
            'clientId' => env('PBIE_CLIENTID'),
            'tenant' => env('PBIE_TENANT'),
            'username' => env('PBIE_USERNAME'),
            'password' => env('PBIE_PASSWORD'),
        ]);
        // check for cached token:
        if ($this->cache->has('pbie_token')) {
            $this->cachedToken = $this->cache->get('pbie_token');
            $this->auth->setAccessToken($this->cachedToken);
        }
        $this->api = new MicrosoftPowerBIApiConnector($this->auth);
        $this->recorder = new ApiRecorder();
        $this->api->setRecorder($this->recorder);
    }

    public function __destruct()
    {
        // check if token has been updated:
        $newToken = $this->auth->getAccessToken();
        if (!is_null($newToken)) {
            $newToken = json_encode($newToken);
            if ($this->cachedToken!=$newToken) {
                // token has changed - update cache:
                $this->cache->forever('pbie_token', $newToken);
            }
        }
    }

    private function startHttpLog($filepath, $timestamp)
    {
        if (is_null($timestamp)) {
            $timestamp = time();
        }
        if (strlen($filepath)>0 AND substr($filepath, -1)!='/') {
            $filename = $filepath . '_' . $timestamp . '.log';
        } else {
            $filename = $filepath . $timestamp . '.log';
        }
        if ($this->recorderMode=='read') {
            $this->recorder->readFrom($filename, $this->getHttpRecordDisk());
        } else {
            $this->recorder->writeTo($filename, $this->getHttpRecordDisk());
        }
    }

    private function endHttpLog()
    {
        if ($this->recorderMode=='write') {
            $this->recorder->store();
        }
        $this->recorder->close();
    }

    public function getHttpRecordDisk()
    {
        if (is_null($this->disk)) {
            $this->disk = app('filesystem')->disk('powerbi');
        }
        return $this->disk;
    }

    private function validateString($object, $attributeName)
    {
        return isset($object->$attributeName) AND is_string($object->$attributeName) AND strlen($object->$attributeName)>0;
    }

    private function validateUuid($object, $attributeName)
    {
        return isset($object->$attributeName) AND is_string($object->$attributeName) AND strlen($object->$attributeName)==36;
    }

    /**
     * @param integer|null $timestamp
     * @return array
     * @throws \Exception
     */
    public function getGroupsList($timestamp=NULL)
    {
        $this->startHttpLog('groups', $timestamp);
        $transaction = $this->api->Groups()->getGroups();
        $data = $transaction->decodeJsonResponse();
        $result = ['data'=>[]];
        if (isset($data->value) AND is_array($data->value)) {
            foreach ($data->value as $group) {
                if ($this->validateUuid($group, 'id') AND $this->validateString($group, 'name')){
                    $result['data'][] = [
                        'id' => $group->id,
                        'name' => $group->name,
                    ];
                }
            }
        }
        $this->endHttpLog();
        return $result;
    }

    /**
     * @param string|null $group
     * @param integer|null $timestamp
     * @return array
     * @throws \Exception
     */
    public function getReportsList($group=NULL, $timestamp=NULL)
    {
        $filepath = (empty($group) ? '' : 'groups/'.$group.'/') . 'reports';
        $this->startHttpLog($filepath, $timestamp);
        $transaction = $this->api->Reports()->getReports($group);
        $data = $transaction->decodeJsonResponse();
        $result = ['data'=>[]];
        if (isset($data->value) AND is_array($data->value)) {
            foreach ($data->value as $report) {
                if ($this->validateUuid($report, 'id')
                    AND $this->validateString($report, 'name')
                    AND $this->validateString($report, 'webUrl')
                    AND $this->validateString($report, 'embedUrl')
                ) {
                    $result['data'][] = [
                        'id' => $report->id,
                        'name' => $report->name,
                        'webUrl' => $report->webUrl,
                        'embedUrl' => $report->embedUrl,
                    ];
                }
            }
        }
        $this->endHttpLog();
        return $result;
    }

    /**
     * @param Report $report
     * @return object|NULL {'token': $token, 'expires': $timestamp}
     * @throws \Exception
     */
    public function getEmbedToken(Report $report)
    {
        $cacheKey = 'pbie_token|'.$report->getRouteKey().'|'.auth()->id();
        // check if token is cached:
        if ($this->cache->has($cacheKey)) {
            return json_decode($this->cache->get($cacheKey));
        }
        $this->startHttpLog('groups/'.$report->pbi_group.'/reports/'.$report->pbi_report.'/token', time());
        $transaction = $this->api->EmbedTokens()->getReportToken($report->pbi_report, $report->pbi_group);
        $data = $transaction->decodeJsonResponse();
        if ($this->validateString($data, 'token') AND $this->validateString($data, 'expiration')) {
            $result = (object) [
                'token' => $data->token,
                'expires' => strtotime($data->expiration),
            ];
            // cache for a while - hopefully long enought to cover refreshes but not long enough to cause JS issues with expired tokens
            $duration =  intval((($result->expires - time()) / 60) - 5); // diff between expires time and now in minutes, minus 5 mins (should be 55 mins)
            $this->cache->put($cacheKey, json_encode($result), $duration);
        } else {
            $result = NULL;
        }
        $this->endHttpLog();
        return $result;
    }


}