<?php

namespace App\Traits;

use Jenssegers\Optimus\Optimus;
use Tuupola\Base62;

trait URLObfuscation
{
    /**
     * @var Optimus
     */
    static protected $_optimus = NULL;

    /**
     * @var Base62
     */
    static protected $_base62 = NULL;

    /**
     * Overridden from the model.
     * @return string
     */
    public function getRouteKey()
    {
        return self::encodeId($this->getKey());
    }

    /**
     * Encoder takes primary ID, encodes with Optimus,
     * converts to string, prepends zeroes to make length = 10,
     * then encodes with base62.
     *
     * @return string
     */
    static public function encodeId($key)
    {
        self::makeProviders();
        return self::$_base62->encode(str_pad(self::$_optimus->encode($key), 10, '0', STR_PAD_LEFT));
    }


    /**
     * @param $encodedId
     * @return mixed
     */
    public function resolveRouteBinding($encodedId)
    {
        return $this->where($this->getRouteKeyName(), self::decodeId($encodedId))->first();
    }


    /**
     * Decoder decodes base62 input, converts to integer, decodes with Optimus to get primary ID.
     * @param string $encodedId
     * @return integer
     */
    static public function decodeId($encodedId)
    {
        self::makeProviders();
        return self::$_optimus->decode(intval(self::$_base62->decode($encodedId)));
    }

    static protected function makeProviders() {
        if(is_null(self::$_optimus)) {
            self::$_optimus = app(Optimus::class);
        }
        if(is_null(self::$_base62)) {
            self::$_base62 = app(Base62::class);
        }
    }

    /**
     * @param $encodedId
     * @return static|NULL
     */
    static public function findByEncodedId($encodedId)
    {
        self::makeProviders();
        return self::find(self::decodeId($encodedId));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getObfuscatedId()
    {
        if (!isset($this->id)) {
            throw new \Exception('ID not set');
        }
        return self::encodeId($this->id);
    }

}