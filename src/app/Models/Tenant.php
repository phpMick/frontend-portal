<?php

namespace App\Models;

use App\Traits\URLObfuscation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use App\Models\Group;
use Illuminate\Support\Facades\DB;


class Tenant extends Model
{
    use SoftDeletes;
    use URLObfuscation;

    const TYPE_NONE = 0;
    const TYPE_BASIC = 10;
    const TYPE_CUSTOMER = 100;
    const TYPE_RESELLER = 150;
    const TYPE_PARTNER = 200;
    const TYPE_MASTER = 255;
    const TYPE_ALL = [
        self::TYPE_NONE, self::TYPE_BASIC, self::TYPE_CUSTOMER, self::TYPE_RESELLER, self::TYPE_PARTNER, self::TYPE_MASTER
    ];

    /**
     * Tenants/users is many to many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * A tenant has many groups
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    /**
     * Reports belong to groups.
     *
     * @return mixed
     */
    public function reports()
    {
        $groups = $this->groups->pluck('id');
        return Report::whereIn('group_id',$groups)->get();
    }


    /**
     * When a Tenant is soft deleted cascade delete it's Groups and Reports
     */
    public function deleteAndCascade(){

        DB::transaction(function() {

            foreach ($this->reports() as $report) {
                $report->delete();
            }
            $this->groups()->delete();
            $this->delete();

        });

    }


    /**
     * Get the model for the selected Tenant
     * @return Tenant|Tenant[]|\Illuminate\Database\Eloquent\Collection|Model
     */
    public static function selectedTenant()
    {
        $selectedTenant = session('selectedTenant');
        if (empty($selectedTenant)) {
            $selectedTenant = optional(auth()->user()->tenants->first())->id;
        }
        $tenant = Tenant::findOrFail($selectedTenant);
        return $tenant;
    }

    /**
     * @return array
     */
    static public function typeNames()
    {
        return [
            self::TYPE_NONE => 'free',
            self::TYPE_BASIC => 'basic',
            self::TYPE_CUSTOMER => 'customer',
            self::TYPE_RESELLER => 'reseller',
            self::TYPE_PARTNER => 'partner',
            self::TYPE_MASTER => 'master',
        ];
    }

    /**
     * @return array
     */
    static public function getTypes() {
        $types = self::typeNames();
        foreach($types as $i=>$name) {
            $types[$i] = ucwords($name);
        }
        return $types;
    }

    /**
     * @return array
     */
    static public function getObfuscatedTenants()
    {
        $tenants = self::select(['id', 'name'])->orderBy('name')->get();
        $obfuscatedTenants = [];
        foreach ($tenants as $tenant) {
            $id = self::encodeId($tenant->getKey());
            $obfuscatedTenants[$id] = $tenant->name;
        }
        return $obfuscatedTenants;
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function typeString()
    {
        $names = self::typeNames();
        if (array_key_exists($this->type, $names)) {
            return $names[$this->type];
        }
        throw new \Exception('Invalid application state: Tenant::type');
    }

    /**
     * @return array
     */
    static public function typesWithChildren()
    {
        return [self::TYPE_RESELLER, self::TYPE_PARTNER, self::TYPE_MASTER];
    }


}
