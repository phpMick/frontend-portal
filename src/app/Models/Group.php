<?php

namespace App\Models;

use App\Traits\URLObfuscation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use DB;

class Group extends Model
{
    use SoftDeletes;
    use URLObfuscation;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'tenant_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class );
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(Report::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * When a Tenant is soft deleted cascade delete it's and Reports
     */
    public function deleteAndCascade(){

        DB::transaction(function() {

            foreach ($this->reports as $report) {
                $report->delete();
            }

            $this->delete();

        });

    }


}
