<?php

namespace App\Models;

use App\Traits\URLObfuscation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;



class Report extends Model
{
    use SoftDeletes;
    use URLObfuscation;

    const TYPE_PBI_PRIVATE = 'private';
    const TYPE_PBI_PUBLIC = 'public';
    const TYPE_UNKNOWN = '';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];


    static public function typeName($type)
    {
        switch ($type) {
            case self::TYPE_PBI_PRIVATE: return 'Secure Embedded Report';
            case self::TYPE_PBI_PUBLIC: return 'Published Shared Link';
            default: return '-';
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class );
    }

    /**
     * @return string
     */
    public function type()
    {
        if (empty($this->pbi_group) AND empty($this->pbi_report)) {
            if (empty($this->pbi_config)) {
                return self::TYPE_UNKNOWN;
            } else {
                return self::TYPE_PBI_PUBLIC;
            }
        } elseif (empty($this->pbi_config)) {
            return self::TYPE_UNKNOWN;
        } else {
            return self::TYPE_PBI_PRIVATE;
        }
    }


    public function getUrl()
    {
        switch ($this->type()) {
            case self::TYPE_UNKNOWN:
                return '';
            case self::TYPE_PBI_PUBLIC:
                return 'https://app.powerbi.com/view?r='.$this->pbi_config;
            case self::TYPE_PBI_PRIVATE:
                return 'https://app.powerbi.com/reportEmbed?reportId='.$this->pbi_report.'&groupId='.$this->pbi_group.'&config='.$this->pbi_config;
        }
    }

    private function validateUrlAndGetQueryParams($url)
    {
        // read in parts from complete URL:
        $urlparts = parse_url($url);
        if (!array_key_exists('scheme',$urlparts) OR $urlparts['scheme']!='https') {
            return FALSE;
        }
        if (!array_key_exists('host', $urlparts) OR $urlparts['host']!='app.powerbi.com') {
            return FALSE;
        }
        if (!array_key_exists('path', $urlparts) OR !in_array($urlparts['path'],['/view','/reportEmbed'])) {
            return FALSE;
        }
        if (!array_key_exists('query', $urlparts)) {
            return FALSE;
        }
        $params = explode('&', $urlparts['query']);
        $paramArray = [];
        foreach ($params as $param) {
            $parts = explode('=',$param);
            if (count($parts)!=2) {
                return FALSE;
            }
            $paramArray[$parts[0]] = $parts[1];
        }
        return $paramArray;
    }

    public function setSharedUrl($url)
    {
        $params = $this->validateUrlAndGetQueryParams($url);
        if (!$params OR !array_key_exists('r', $params)) {
            return FALSE;
        }
        $this->pbi_group = NULL;
        $this->pbi_report = NULL;
        $this->pbi_config = $params['r'];
        return TRUE;
    }

    public function setEmbedUrl($url)
    {
        $params = $this->validateUrlAndGetQueryParams($url);
        if (!$params OR !array_key_exists('reportId', $params) OR !array_key_exists('groupId', $params) OR !array_key_exists('config', $params)) {
            return FALSE;
        }
        $this->pbi_group = $params['groupId'];
        $this->pbi_report = $params['reportId'];
        $this->pbi_config = $params['config'];
        return TRUE;
    }



}
