<?php

namespace App\Models;


use App\Traits\URLObfuscation;

//Laravel
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


/**
 * Class User
 * @package App\Models
 * @property integer $id
 *
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use URLObfuscation;


    const STATUS_INVITED = 0;
    const STATUS_REGISTERED = 5;
    const STATUS_ACTIVE = 10;

    const STATUS_REJECTED = -5;
    const STATUS_INACTIVE = -10;
    const STATUS_SUSPENDED = -100;
    const STATUS_DELETED = -120;

    const STATUS_ALL = [
        self::STATUS_REJECTED,
        self::STATUS_DELETED,
        self::STATUS_SUSPENDED,
        self::STATUS_INACTIVE,
        self::STATUS_INVITED,
        self::STATUS_REGISTERED,
        self::STATUS_ACTIVE,
    ];

    const ROLE_GLOBAL_ADMIN = 'GLOBAL_ADMIN';
    const ROLE_RESELLER_ADMIN = 'RESELLER_ADMIN';
    const ROLE_TENANT_ADMIN = 'TENANT_ADMIN';
    const ROLE_USER = 'USER';
    const ROLE_ALL = [
        self::ROLE_USER, self::ROLE_TENANT_ADMIN, self::ROLE_RESELLER_ADMIN, self::ROLE_GLOBAL_ADMIN
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'tenant_id',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * A user has many groups
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }


    /**
     * A user belongs to many tenants
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tenants()
    {
        return $this->belongsToMany(Tenant::class);
    }

    /**
     * This is how we currently determine which tenant the
     * user "belongs to". This will change when they can have multiple.
     *
     * @return null
     *
     */
    public function  defaultTenant()
    {
        if($this->tenants->count() > 0){
            return $this->tenants->first();
        }else {
            return null;
        }
    }


    /**
     * Selects the default Tenant for this user
     * if they don't have one, kick them out
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function selectDefaultTenant()
    {
        $passed = false;

        if ($this->defaultTenant()) {
            session()->put('selectedTenant', $this->defaultTenant()->id);
            $passed = true;
        }

        return $passed;
    }


    /**
     * Check that the selected tenant exists
     * if it doesn't select the default for this user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkSelectedTenantExists()
    {
        $tenant = Tenant::find(session('selectedTenant'));

        $passed = ($tenant ? true:false);
        return $passed;
    }



    /**
     * Check that the default tenant for this user exists
     */
    public function checkDefaultExists()
    {
        $tenant = Tenant::find(session('selectedTenant'));

        $passed = ($tenant ? true:false);
        return $passed;
    }


    /**
     * Global admins can see all tenants
     *  other users need a tenant_user record
     * @return Tenant[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function allowedTenants()
    {
        if($this->role == User::ROLE_GLOBAL_ADMIN) {
            return Tenant::all();
        } else {
            return $this->tenants;
        }
    }


    /**
     * Get all the reports this user has access to.
     * This is all the reports for all their groups.
     * @return Report[]|\Illuminate\Database\Eloquent\Collection
     */
    public function allReports()
    {
        $groups = $this->groups->pluck('id')->toArray();
        $result = Report::whereIn('group_id',$groups)->get();

        return $result;
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        $userRoles = null;

        foreach (User::ROLE_ALL as $role) {
            $userRoles[$role] = ucwords(strtolower(str_replace('_', ' ', $role)));
        }

        return $userRoles;
    }





    /**
     *
     * Just check the password reset token for a new user.
     * @param string $token
     * @return bool
     */
    public function checkToken(string $token)
    {
        $passed = false;

        $resetRecord = DB::table('password_resets')
            ->select('token')
            ->where('email', '=', $this->email)
            ->first();

        if (isset($resetRecord)) {

            if (Hash::check($token, $resetRecord->token)) {
                $passed = true;
            }
        }

        return $passed;
    }

    /**
     * @param $email
     * @return User
     */
    static public function findByEmail($email)
    {
        return self::where('email', '=', $email)->firstOrFail();
    }

    /**
     * @return string
     */
    public function statusString()
    {
        switch ($this->status) {
            case self::STATUS_INVITED:
                return 'Invited';
            case self::STATUS_REGISTERED:
                return 'Registered';
            case self::STATUS_ACTIVE:
                return 'Active';
            case self::STATUS_INACTIVE:
                return 'Inactive';
            case self::STATUS_SUSPENDED:
                return 'Suspended';
        }
        return 'INVALID';
    }

    /**
     * @return string
     */
    public function roleString()
    {
        return ucwords(strtolower(str_replace('_', ' ', $this->role)));
    }

    /**
     * Checks that this user is a Global Admin.
     * @return bool
     */
    public function isGlobalAdmin()
    {
        return self::ROLE_GLOBAL_ADMIN == $this->role;
    }

    /**
     * @param null $for
     * @return bool
     */
    public function isResellerAdmin($for = NULL)
    {
        if (is_null($for)) {
            // just check if user has reseller admin role
            return self::ROLE_RESELLER_ADMIN == $this->role;
        }

    }


}
