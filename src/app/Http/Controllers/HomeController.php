<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{

    /**
     * Ensure that a tenant is selected
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('tenantSelected');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notAuthorised()
    {
        return view('message-page');
    }


}
