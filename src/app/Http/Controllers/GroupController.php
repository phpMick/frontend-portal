<?php

namespace App\Http\Controllers;

//Models
use App\Models\Group;
use App\Models\Tenant;

//Form Requests
use App\Http\Requests\StoreGroup;

class GroupController extends Controller
{

    /**
     * Ensure that a tenant is selected
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('tenantSelected');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Group::class);
        $groups = optional(Tenant::selectedTenant())->groups;
        return view('groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Group::class);
        $group = new Group();
        $tenants = Tenant::getObfuscatedTenants();

        return view('groups.create', compact('group', 'tenants'));
    }

    /**
     * Store a newly created resource in storage
     * @param StoreGroup $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreGroup $request)
    {
        //auth in form request

        $group = new Group();
        $group->name = $request->name;
        $group->description = $request->description;
        $group->tenant_id = $request->tenant_id;
        $group->save();

        return redirect()->route('groups.show', [$group]);
    }

    /**
     * Display the specified resource.
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Group $group)
    {
        $this->authorize('show', $group);

        $users = $group->users;

        return view('groups.show', compact('group','users'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Group $group)
    {
        $this->authorize('edit', $group);
        $tenants = Tenant::getObfuscatedTenants();

        return view('groups.edit', compact('group', 'tenants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function update(StoreGroup $request, Group $group)
    {
        $group->name = $request->name;
        $group->description = $request->description;
        $group->tenant_id = $request->tenant_id;
        $group->save();

        return redirect()->route('groups.show', [$group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tenant  $tenant
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        $this->authorize('destroy', $group);

       $group->deleteAndCascade();

        return response()->json([
            'status' => 'ok',
            'message' =>  'tenant '.$group->getRouteKey().' deleted'
        ], 200);

    }
}
