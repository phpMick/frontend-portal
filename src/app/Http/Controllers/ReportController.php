<?php

namespace App\Http\Controllers;

//Models
use App\Models\Report;
use App\Models\Tenant;
use App\Models\Group;

//Laravel
use App\Http\Requests\StoreReport;
use Illuminate\Http\Response;

//Services
use App\Services\PowerBI;

class ReportController extends Controller
{

    /**
     * Ensure that a tenant is selected
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('tenantSelected');
    }

    /**
     * @param StoreReport $request
     * @return Tenant|NULL
     */
    private function findOrCreateTenant(StoreReport $request)
    {
        $tenant = NULL;
        // create tenant if desired:
        if ($request->tenant_id=='+' AND !empty($request->tenant_name)) {
            $tenant = new Tenant();
            $tenant->name = $request->tenant_name;
            $tenant->save();

        } else {
            $tenant = Tenant::findByEncodedId($request->tenant_id);
        }
        return $tenant;
    }

    /**
     * @param StoreReport $request
     * @param Tenant $tenant
     * @return Group|NULL
     */
    private function findOrCreateGroup(StoreReport $request, Tenant $tenant)
    {
        $group = NULL;
        // create the group if desired:
        if ($request->group_id=='+' AND !empty($request->group_name)) {
            $group = new Group();
            $group->name = $request->group_name;
            $group->tenant()->associate($tenant);
            $group->save();
        } else {
            $group = Group::findByEncodedId($request->group_id);
        }
        return $group;
    }

    /**
     * @param StoreReport $request
     * @param Report $report
     * @return Report
     * @throws \Exception
     */
    private function updateReport(StoreReport $request, Report $report)
    {
        $report->name = $request->name;
        $report->description = $request->description;
        if (!empty($request->pbi_group) AND $request->pbi_group!='-' AND !empty($request->pbi_report) AND $request->pbi_report!='-') {
            // load details from cache (or maybe get?)
            $reportList = json_decode(app('cache')->get('pbi_reports_'.$request->pbi_group));
            if (!$reportList OR !isset($reportList->data) OR !is_array($reportList->data)) {
                throw new \Exception('Failed to load Power BI report details');
            }
            $r = NULL;
            foreach ($reportList->data as $r) {
                if ($r->id==$request->pbi_report) {
                    break;
                }
            }
            if (!isset($r->id) OR $r->id!=$request->pbi_report) {
                throw new \Exception('Failed to find Power BI report');
            }
            if(!$report->setEmbedUrl($r->embedUrl)) {
                throw new \Exception('Failed to parse Power BI embed URL');
            }
        } elseif (!empty($request->pbi_url)) {
            if (!$report->setSharedUrl($request->pbi_url)) {
                throw new \Exception('Failed to parse Power BI shared URL');
            }
        }



        return $report;
    }

    /**
     * Display index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Report::class);
        return view('reports.index')
            ->with('reports', optional(Tenant::selectedTenant())->reports());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Report::class);
        $report = new Report();
        return view('reports.create')
            ->with('report', $report);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreReport $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(StoreReport $request)
    {
        $tenant = $this->findOrCreateTenant($request);
        $group = $this->findOrCreateGroup($request, $tenant);

        // create the report:
        $report = $this->updateReport($request, new Report());
        $report->group()->associate($group);

        if (!$report->save()) {
            session()->flash('danger', 'A problem occurred when trying to create the report');
        }

        return redirect()->route('reports.show', [$report]);

    }

    /**
     * Display the specified resource.
     *
     * @param Report $report
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Report $report)
    {
        $this->authorize('show', $report);

        return view('reports.show', compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Report $report
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Report $report)
    {
        $this->authorize('edit', $report);

        return view('reports.edit')
            ->with('report', $report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreReport $request
     * @param Report $report
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(StoreReport $request, Report $report)
    {
        $tenant = $this->findOrCreateTenant($request);
        $group = $this->findOrCreateGroup($request, $tenant);
        $report = $this->updateReport($request, $report);

        $report->group()->associate($group);

        if (!$report->save()) {
            session()->flash('danger', 'A problem occurred when trying to update the report');
        }

        return redirect()->route('reports.show', [$report]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Report $report
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Report $report)
    {
        $this->authorize('destroy', $report);
        $deleteCount = Report::destroy($report->id);

        if ($deleteCount != 1) {
            throw new \Exception('Zero/multiple reports deleted when deleting report ID ' . $report->getRouteKey());
        }

        return response()->json([
            'status' => 'ok',
            'message' => 'report ' . $report->getRouteKey() . ' deleted'
        ], 200);

    }


    /**
     * @param Report $report
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function view(Report $report)
    {
        $this->authorize('view', $report);
        $userId = auth()->user()->id;

        // check if report is public share or private embed:
        if (!empty($report->pbi_group) AND !empty($report->pbi_report) AND !empty($report->pbi_config)) {
            // private embed report - generate an embed token:
            $svc = new PowerBI();
            $token = $svc->getEmbedToken($report);


            writeJSONLogInfo('report-viewed',['report_id' => $report->id,'type' => 'private']);

            return view('reports.view_embedded')
                ->with('report', $report)
                ->with('token', $token);
        }

        //public report
        if (!empty($report->pbi_config)) {
            writeJSONLogInfo('report-viewed',['report_id' => $report->id,'type' => 'public']);
            return view('reports.view_public')
                ->with('report', $report);
        } else {
            abort(404, 'Report invalid');
        }


    }


}
