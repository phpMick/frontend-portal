<?php

namespace App\Http\Controllers;

use App\Models\User;

class DashboardController extends Controller
{


    /**
     * Ensure that a tenant is selected
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('tenantSelected');
    }


    /**
     * @return User|\Illuminate\Contracts\Auth\Authenticatable
     * @throws \Exception
     */
    /*private function user() {
        if(is_null($this->user)) {
            if(!Auth::check()) {
                throw new \Exception('User must be authenticated!');
            }
            $this->user = Auth::user();
        }
        return $this->user;
    }*/

    public function index()
    {
        // we want to work towards providing an overview of:
        //  - subscription status & upcoming renewal (TENANT_ADMIN)
        //  - number of users in tenant (TENANT_ADMIN)
        //  - list of reports available in tenant
        //  - number of managed customers (RESELLER_ADMIN/PARTNER_ADMIN/GLOBAL_ADMIN)
        //  - number of managed resellers (PARTNER_ADMIN/GLOBAL_ADMIN)
        //  - number of managed partners (GLOBAL_ADMIN)


        // TODO: use report groups, and separate them out in the dashboard for easier navigation
        $reports = auth()->user()->allReports();

        // if there's only one report, just display that report, otherwise show all available reports as tiles:
        if ($reports->count()==1) {
            return redirect()->route('reports.view', ['report'=>$reports->first()]);
        } else {
            return view('dashboard.index')
                ->with('reports', $reports);
        }

    }
}
