<?php

namespace App\Http\Controllers;

//Models
use App\Models\Tenant;
use App\Models\Group;

//Form Requests
use App\Http\Requests\StoreTenant;

class TenantController extends Controller
{

    /**
     * Ensure that a tenant is selected
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('tenantSelected');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Tenant::class);
        $tenants = auth()->user()->allowedTenants();
        return view('tenants.index', compact('tenants'));
    }

    /**
     *  Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Tenant::class);
        $tenant = new Tenant();
        $types = Tenant::getTypes();
        $groups = Group::all();

        return view('tenants.create', compact('tenant', 'groups', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreTenant $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreTenant $request)
    {
        //auth in form request

        $tenant = new Tenant();
        $tenant->name = $request->name;
        $tenant->type = $request->type;
        $tenant->allow_ms_login = $request->has('allow_ms_login');
        $tenant->save();

        return redirect()->route('tenants.show', [$tenant]);
    }

    /**
     * Display the specified resource.
     * @param Tenant $tenant
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Tenant $tenant)
    {
        $this->authorize('show', $tenant);

        return view('tenants.show', compact('tenant'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Tenant $tenant
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Tenant $tenant)
    {
        $this->authorize('edit', $tenant);

        $types = Tenant::getTypes();
        $groups = Group::all();

        return view('tenants.edit', compact('tenant', 'types','groups'));
    }

    /**
     * Update the specified resource in storage.
     * @param StoreTenant $request
     * @param Tenant $tenant
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreTenant $request, Tenant $tenant)
    {
        $tenant->name = $request->name;
        $tenant->type = $request->type;
        $tenant->allow_ms_login = $request->has('allow_ms_login');
        $tenant->save();

        return redirect()->route('tenants.show', [$tenant]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tenant  $tenant
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Tenant $tenant)
    {
        $this->authorize('destroy', $tenant);

        if( $tenant->is(auth()->user()->defaultTenant()) ){ //cannot delete your own tenant
            $message = "You cannot delete your own tenant";
            session()->flash('error', $message);
            $status = "error";
            $statusCode = 403;

        }else{
            $tenant->deleteAndCascade();
            $message = "Tenant {$tenant->name} deleted";
            $status = "error";
            $statusCode = 200;

        }

        return   response()->json([
            'status' => $status,
            'message' => $message
        ], $statusCode);
    }


    /**
     * Update the selected tenant and reload the previous page
     * @param Tenant $tenant
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function select(Tenant $tenant)
    {
        session()->put('selectedTenant', $tenant->id);

        return redirect(url()->previous());
    }

}
