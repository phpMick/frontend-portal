<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

//Form Requests
use App\Http\Requests\StoreUser;

//Models
use App\Models\User;
use App\Models\Tenant;

class UserController extends Controller
{

    /**
     * Ensure that a tenant is selected
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('tenantSelected');
    }

    /**
     * Display list of users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', User::class);
        $users = optional(Tenant::selectedTenant())->users;
        return view('users.index', compact('users'));
    }

    /**
     * Show the form to create new.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', User::class);
        $roles = User::getRoles();
//        $tenants = Tenant::getObfuscatedTenants();
        $currentTenant = Tenant::selectedTenant();
        $user = new User();

        $assignedGroups = collect();
//        $allGroups = Tenant::selectedTenant()->groups;

        return view('users.create', compact('roles', 'user', 'currentTenant', 'assignedGroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUser $request#
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->status = User::STATUS_INVITED;
        $user->save();
        $user->groups()->sync($request->groups);
        $user->tenants()->attach($request->tenant_id);


        // TODO: make this queued & set up queue workers on server


        return redirect()->route('users.show', [$user]);
    }


    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('show', $user);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('edit', $user);
        $roles = User::getRoles();
        $currentTenant = Tenant::selectedTenant();
        $assignedGroups = $user->groups;
//        $allGroups = Tenant::selectedTenant()->groups;

        return view('users.edit', compact('user', 'roles', 'currentTenant', 'assignedGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUser $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, User $user)
    {

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->tenants()->sync([$request->tenant_id]);
        $user->groups()->sync($request->groups);
        $user->save();


        //getOriginal?

        return redirect()->route('users.show', [$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $this->authorize('destroy', $user);

        if( $user->is(auth()->user()) ){ //cannot delete yourself
            $message = "You cannot delete your own user.";
            session()->flash('error', $message);
            $status = "error";
            $statusCode = 403;

        }else{
            $user->delete();


            $status = "error";
            $message = "User  {$user->getRouteKey()}  deleted";

            $statusCode = 200;

        }


        return   response()->json([
            'status' => $status,
            'message' => $message
        ], $statusCode);

    }


}
