<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserCreated;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class InvitePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.passwords.invite');
    }

    public function sendInviteEmail(Request $request)
    {
        $this->validate($request, [
             'email'=>'required|email'
        ]);

        $user = User::where('email', $request->email)->first();
        if(($user instanceof User) AND ($user->password === NULL)) {
            // ok to resend invite
            event(new UserCreated($user));
            session()->flash('success', 'Email sent');
        } else {
            session()->flash('danger', 'Invite has already been accepted.');
        }
        return redirect()->route('password.invite');
    }

    /**
     * The user has clicked the reject button on the invite
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View#
     */
    public function rejectInvite(Request $request,User $user)
    {

        if(!is_null($user) AND (!empty($user->password) OR ($user->status != User::STATUS_INVITED))) {
            session()->flash('danger', 'Your invite has already been accepted');
            return redirect()->back();
        }

        $user->status = User::STATUS_REJECTED;
        $user->save();

        session(['message-title' => 'Invite Rejected.']);
        session(['message-detail' => 'Your account has been deactivated.']);
        return view('message-page');


    }



}
