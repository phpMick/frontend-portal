<?php

namespace App\Http\Controllers\Auth;

//Bytes
use App\Auth\AzureActiveDirectory;
use App\CloudServices\Azure\AadLogin;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;

//Models

use App\Models\User;


//Laravel
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

//Just using this directly for now
use TheNetworg\OAuth2\Client\Provider\Azure;

class AzureActiveDirectoryController extends Controller
{
    /**
     * @param $application
     * @return bool|string
     * @throws \Exception
     */
    protected function applicationName($application)
    {
        if (strpos($application, 'to-') === 0) {
            $application = substr($application, 3);
        } elseif (strpos($application, 'from-') === 0) {
            $application = substr($application, 5);
        }
        // ensure application is valid:
        if (!in_array($application, MicrosoftAzureActiveDirectoryAuth::APP_ALL)) {
            throw new \Exception('Invalid application');
        }
        return $application;
    }

    /**
     * @param Request $request
     * @param AzureActiveDirectory $aadAuth
     * @param null $context
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function login(Request $request, AzureActiveDirectory $aadAuth, $context = null)
    {
        $aadAuth->setContext($context);
        if ($request->has('tenant')) {
            $aadAuth->setTenant($request->get('tenant'));
        }
        return $aadAuth->doLogin();
    }

    /**
     * @param string $application
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function logout($application = 'from-portal')
    {
        $application = $this->applicationName($application);
        return MicrosoftAzureActiveDirectoryAuth::interactive($application)->getLogoutRedirect();
    }

    /**
     * User has clicked login with MS button
     * Go to MS and get an auth code
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function loginStart(Request $request)
    {
        $authService = MicrosoftAzureActiveDirectoryAuth::interactive(MicrosoftAzureActiveDirectoryAuth::APP_PORTAL);
        return $authService->getLoginRedirect($request);
    }


    /**
     * GET here from MS with auth code.
     *
     * @param Request $request
     * @param string $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|void
     * @throws \Exception
     */
    public function loginResponse(Request $request, $application = 'to-portal')
    {

        if ($request->has('code')) {

            $application = $this->applicationName($application);

            //$authService = MicrosoftAzureActiveDirectoryAuth::interactive($application);

            //$authService->receiveResponse($request);
            //$redirect = $authService->getIntendedUrl();


            $MAADA = MicrosoftAzureActiveDirectoryAuth::interactive($application);

            //testing
            $MAADA->receiveResponse($request);
            $redirect = $MAADA->getIntendedUrl();
            if (isset($redirect)) {
                return redirect($redirect);
            }


            $aadLogin = new AadLogin($MAADA);

            return $aadLogin->attemptOAuthLogin($request->all());

        }else{
            //Dont have code, error or cancelled
            return redirect(route('login'));
        }

    }
}