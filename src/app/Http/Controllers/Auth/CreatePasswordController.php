<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class CreatePasswordController extends Controller
{

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param Request $request
     * @param string|null $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreateForm(Request $request, $token = null)
    {
        return view('auth.passwords.create')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function create(Request $request)
    {
        // first check that user's password is still empty
        $user1 = User::where('email', $request->email)->first();
        if(!is_null($user1) AND (!empty($user1->password) OR ($user1->status != User::STATUS_INVITED))) {
            session()->flash('danger', 'Your invite has already been accepted');
            return redirect()->back();
        }

        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        }
        );

        if($response == Password::PASSWORD_RESET) {
            // good to proceed - update user status
            $user1->fresh();
            $user1->status = User::STATUS_ACTIVE;
            $user1->save();
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($response)
            : $this->sendResetFailedResponse($request, $response);
    }


}
