<?php


namespace App\Http\Controllers;

use Illuminate\Cache\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

//Models
use App\Models\Group;
use App\Models\Report;
use App\Models\Tenant;
use App\Models\User;

//Services
use App\Services\PowerBI;


class ApiController extends Controller
{

    const JSON_HEADERS = ['Content-Type'=>'application/json'];

    /**
     * @var Repository
     */
    private $cache;

    /**
     * @var PowerBI
     */
    private $service;

    public function __construct()
    {
        $this->cache = app('cache');
        $this->service = new PowerBI();
        // TODO: add auth checks
    }

    /**
     * @param Collection $tenants
     * @return array
     */
    private function tenantsToJson(Collection $tenants)
    {
        $result = [];
        foreach ($tenants as $tenant) {
            if ($tenant instanceof Tenant) {
                $result[] = ['id'=>$tenant->getRouteKey(), 'name'=>$tenant->name];
            }
        }
        return ['data'=>$result];
    }

    /**
     * @param Collection $groups
     * @return array
     */
    private function groupsToJson(Collection $groups)
    {
        $result = [];
        foreach ($groups as $group) {
            if ($group instanceof Group) {
                $result[] = ['id'=>$group->getRouteKey(), 'name'=>$group->name, 'tenantId'=>$group->tenant->getRouteKey(), 'tenantName'=>$group->tenant->name];
            }
        }
        return ['data'=>$result];
    }

    /**
     * @return array
     */
    public function tenants()
    {
        /** @var User $user */
        $user = auth()->user();
        if ($user->isGlobalAdmin()) {
            $tenants = Tenant::all();
        } else {
            $tenants = $user->tenants()->get();
        }
        return $this->tenantsToJson($tenants);
    }

    /**
     * @param Tenant|NULL $tenant
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function groups(Tenant $tenant=NULL)
    {
        if ($tenant instanceof Tenant) {
            $this->authorize('show', $tenant);
            $groups = $tenant->groups()->with('tenant')->get();
        } else {
            /** @var User $user */
            $user = auth()->user();
            $groups = $user->groups()->with('tenant')->get();
        }
        return response()->json($this->groupsToJson($groups));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function pbiGroups(Request $request)
    {
        // this route is only allowed for report creators:
        $this->authorize('create', Report::class);
        $refresh = $request->get('refresh', '0');
        if ($refresh=='1' OR !$this->cache->has('pbi_groups')) {
            $groupsListJson = json_encode($this->service->getGroupsList());
            $this->cache->forever('pbi_groups', $groupsListJson);
        } else {
            $groupsListJson = $this->cache->get('pbi_groups');
        }
        return response($groupsListJson,200, self::JSON_HEADERS);
    }

    /**
     * @param $group
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function pbiReports($group, Request $request)
    {
        // this route is only allowed for report creators:
        $this->authorize('create', Report::class);
        $refresh = $request->get('refresh', '0');
        $cachekey = empty($group) ? 'pbi_reports' : 'pbi_reports_' . $group;
        if ($refresh=='1' OR !$this->cache->has($cachekey)) {
            $reportsListJson = json_encode($this->service->getReportsList($group));
            $this->cache->forever($cachekey, $reportsListJson);
        } else {
            $reportsListJson = $this->cache->get($cachekey);
        }
        return response($reportsListJson,200, self::JSON_HEADERS);
    }

    /**
     * @param Report $report
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function pbiReportToken(Report $report)
    {
        // ensure auth user can view this report:
        $this->authorize('view', $report);
        $token = $this->service->getEmbedToken($report);
        return response(json_encode($token), 200, self::JSON_HEADERS);
    }


}