<?php
use Illuminate\Support\Facades\Log;

function writeJSONLogInfo($event,$keyValuePairs=[])
{
    $authenticated_user_id = ( auth()->check() ?  auth()->user()->id: -1 );

    $keyValuePairs['authenticated_user_id'] = $authenticated_user_id;

    Log::info($event, $keyValuePairs);
}


//('user-created',['user_id' => $user->id]);