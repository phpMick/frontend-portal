<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

use App\Models\Tenant;

class StoreTenant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check()) {
            return auth()->user()->can('store', Tenant::class);
        }
        return FALSE;
    }




    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'type'=>'required|in:' . implode(',',Tenant::TYPE_ALL)
        ];


        if(isset($this->tenant->id)){ //editing existing
            $nameRule = [
                'name'=>'required|unique:tenants,name,' . $this->tenant->id
            ];
        } else {
            $nameRule = [
                'name'=>'required|unique:tenants,name'
            ];
        }

        return array_merge($rules, $nameRule);
    }
}
