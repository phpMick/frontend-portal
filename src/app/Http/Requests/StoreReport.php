<?php

namespace App\Http\Requests;

use App\Models\Report;
use App\Models\Tenant;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class StoreReport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::getUser();

        return $user->can('create', Report::class);
    }

    /**
     * @return $this
     */
    protected function prepareForValidation()
    {


    }

    public function withValidator(Validator $validator)
    {
        $validator->sometimes('pbi_url', 'required|regex:/^https\:\/\/app\.powerbi\.com\/view\?r\=.+$/', function ($input) {
            return $input->pbi_type == 'public';
        });
        $validator->sometimes('pbi_group', 'required|uuid', function ($input) {
            return $input->pbi_type == 'private';
        });
        $validator->sometimes('pbi_report', 'required|uuid', function ($input) {
            return $input->pbi_type == 'private';
        });
        // only validate tenant_id with url_id if we're not selecting/creating:
        $validator->sometimes('tenant_id', 'url_id:tenants', function($input) {
            return !in_array($input->tenant_id,['-','+']);
        });
        $validator->sometimes('tenant_name', 'required|unique:tenants,name', function ($input) {
            return $input->tenant_id=='+';
        });

        $validator->sometimes('group_id', 'required|not_in:-', function ($input) {
            return $input->tenant_id!='-';
        });
        $validator->sometimes('group_id', 'url_id:groups', function ($input) {
            return !in_array($input->group_id,['-','+']);
        });
        if (!in_array($this->input('tenant_id'), ['-','+','','*'])) {
            $tenantId = Tenant::decodeId($this->input(['tenant_id']));
            $groupRule = Rule::unique('groups', 'name')->where('tenant_id', $tenantId);
            $validator->sometimes('group_name', ['required', $groupRule], function ($input) {
                return $input->group_id=='+';
            });
        }
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required',
            'pbi_type' => 'required|in:private,public',
            'tenant_id' => 'required|not_in:-',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'pbi_type.required' => 'The type of Power BI report is required',
            'pbi_type.in' => 'The type of Power BI report must be selected from the list',
            'pbi_url.required' => 'The Power BI published share link must be provided when selecting "Published Shared Link"',
            'pbi_url.regex' => 'The Power BI published share link must be the complete link URL',
            'pbi_group.required' => 'A Power BI workspace must be selected when selecting "Secure Embedded Report"',
            'pbi_group.uuid' => 'A valid Power BI workspace must be selected when selecting "Secure Embedded Report"',
            'pbi_report.required' => 'A Power BI report must be selected when selecting "Secure Embedded Report"',
            'pbi_report.uuid' => 'A valid Power BI report must be selected when selecting "Secure Embedded Report"',
            'tenant_id.not_in' => 'A tenant must be selected/created',
            'group_id.not_in' => 'A group must be selected/created',
        ];
    }
}
