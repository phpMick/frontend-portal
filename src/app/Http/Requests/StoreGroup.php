<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

use App\Models\Tenant;
use App\Models\Group;

class StoreGroup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::getUser();

        return $user->can('create', Group::class);
    }


    /**
     * @return $this
     */
    protected function prepareForValidation()
    {

            // we need to switch out the obfuscated IDs into real numbers before validation starts (as we check existance)
            $input = $this->all();
            if(isset($input['tenant_id'])) {

                $input['tenant_id'] = Tenant::decodeId($input['tenant_id']);

                return $this->replace($input);
            }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'tenant_id' => 'required|exists:tenants,id',
              'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    //Group names should be unique in their tenant
                    $found = Group::where(['name' => $this->name, 'tenant_id' => $this->tenant_id] )->first();
                    if($found){
                        if(!$this->editingAGroupWithThisName($found)){
                            $fail("A group called $value already exists in this tenant.");
                        }
                    }
                },
            ],
        ];

        return $rules;

    }

    /**
     * @param $found
     * @return bool
     */
    private function editingAGroupWithThisName($found)
    {
        if (isset($this->group->id)) { //we are editing
            if ($this->group->id === $found->id) { //we are editing a record with this name
                return TRUE;
            }
        }

        return FALSE;
    }




    public function messages()
    {
        return [
            'tenant_id.required' => 'Please select a tenant.',
        ];
    }

}
