<?php

namespace App\Http\Requests;


use App\Models\Tenant;
use App\Models\User;
use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check()) {
            return auth()->user()->can('store', User::class);
        }
        return FALSE;
    }

    /**
     * @return $this
     */
    protected function prepareForValidation()
    {
        // we need to switch out the obfuscated IDs into real numbers before validation starts (as we check existance)
        $input = $this->all();

        if(array_key_exists('tenant_id', $input)) {
            $input['tenant_id'] = Tenant::decodeId($input['tenant_id']);
        }
        return $this->replace($input);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'=>'required',
            'email' => 'required|email',
            'tenant_id' => 'required|exists:tenants,id',

            'role'=>'required|in:' . implode(',',User::ROLE_ALL)
        ];

        if(isset($this->user->id)){
            $emailRule  = ['email'=>['required', 'email', Rule::unique('users')->ignore($this->user->id)]];
        } else {
            $emailRule  = ['email'=>'required|unique:users|email'];
        }
        return array_merge($rules, $emailRule);
    }

    public function messages()
    {
        return [
            'tenant_id.required' => 'Please select a tenant.',
        ];
    }

}
