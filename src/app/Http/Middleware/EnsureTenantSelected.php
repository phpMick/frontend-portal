<?php

namespace App\Http\Middleware;

use App\Models\Tenant;
use Closure;
use Illuminate\Support\Facades\Route;

class EnsureTenantSelected
{
    /**
     * Handle an incoming request.
     * Just make sure that a user group is selected.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            return $this->selectTenant($request, $next);

        } else {
            return $next($request);
        }


    }

    /**
     * If we have a tenant selected - check it exists - if not go to the default tenat
     * If we don't have one selected - go to the default tenant
     * @throws \Exception
     */
    private function selectTenant($request, Closure $next)
    {
        $user = auth()->user();
        $passed = false;


        if (session('selectedTenant') === null) { //no tenant selected
           $passed =  $user->selectDefaultTenant();
        } else {//we have a selected tenant
            $passed = $user->checkSelectedTenantExists();
            if (!$passed){
                $passed =  $user->selectDefaultTenant();
            }
        }

        if(!$passed){
           return $this->logoutWithMessage();
        }

        return $next($request);
    }

    /**
     * Kick the user out and display the message page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function logoutWithMessage()
    {
        auth()->logout();
        session(['message-title' => 'User is not associated with a tenant.']);
        session(['message-detail' => 'Please contact your Account Manager.']);
        return redirect(route('not-authorised'));

    }


}