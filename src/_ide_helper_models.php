<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\AadTenant
 *
 * @property int $id
 * @property string $guid
 * @property string|null $name
 * @property string|null $domain
 * @property int $tenant_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadTenant whereUpdatedAt($value)
 */
	class AadTenant extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AadUser
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $tenant_id
 * @property string|null $guid
 * @property string|null $email
 * @property int|null $is_admin
 * @property string|null $properties
 * @property string|null $tokens
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereProperties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereTokens($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AadUser whereUserId($value)
 */
	class AadUser extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Group
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $tenant_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Tenant $tenant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Report[] $reports
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Group onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Group withoutTrashed()
 */
	class Group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Report
 *
 * @package App\Models
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property string $description
 * @property string $pbi_group
 * @property string $pbi_report
 * @property string $pbi_config
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Group $group
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report wherePbiConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report wherePbiGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report wherePbiReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report withoutTrashed()
 */
	class Report extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Tenant
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $default_group_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Group|NULL $defaultGroup
 * @property AadTenant $aadTenant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tenant onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereDefaultGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tenant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tenant withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tenant withoutTrashed()
 */
	class Tenant extends \Eloquent {}
}

namespace App\Models{
/**
 * Class User
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string|null $password
 * @property int $status
 * @property string|null $role
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tenant[] $tenants
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 */
	class User extends \Eloquent {}
}

