$(document).ready(function () {

    this.$guideBar = $('#guide-bar');

    /*Toggle the side bar when the tab is clicked*/
    this.$guideBar.find('.guide-bar-tab').on('click', function () {
        $("#guide-bar").toggleClass('show-guide-bar');
    });

    //close it when the x is clicked
    $("#close-guide-bar").click(function () {
        $("#guide-bar").removeClass('show-guide-bar');
    });

});



