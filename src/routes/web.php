<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/home', function() {
    return redirect()->route('dashboard');
});

Route::get('/', 'HomeController@index')->name('home');

Route::get('/not-authorised', 'HomeController@notAuthorised')->name('not-authorised');


Route::group(['middleware'=>['auth']], function() {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('reports', 'ReportController');
    Route::get('/reports/{report}/view', 'ReportController@view')->name('reports.view');

    Route::get('/tenants/select/{tenant}', 'TenantController@select')->name('tenant.select');

    Route::resource('users','UserController');
    Route::resource('groups','GroupController');
    Route::resource('tenants', 'TenantController');

});



// don't use default auth routes, as we don't want people to be able to register themselves!
//Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::get('/password/invite', 'Auth\InvitePasswordController@showLinkRequestForm')->name('password.invite');

Route::get('/invite/reject/{user}', 'Auth\InvitePasswordController@rejectInvite')->name('invite.reject');

Route::post('/password/invite', 'Auth\InvitePasswordController@sendInviteEmail')->name('password.send');
Route::get('/password/create/{token}', 'Auth\CreatePasswordController@showCreateForm')->name('password.create');
Route::post('/password/create', 'Auth\CreatePasswordController@create')->name('password.store');

//The AAD login routes
Route::get('/login/with-aad', 'Auth\AzureActiveDirectoryController@loginStart')->name('login.aad.start');
Route::get('/login/with-aad/{application?}', 'Auth\AzureActiveDirectoryController@loginResponse')->name('login.aad.response');
Route::get('/logout/with-aad/{application?}', 'Auth\AzureActiveDirectoryController@logout');


/***********************************************************************************************************************
 *
 * API Routes (for AJAX/Axios/Vue calls
 *
 */
Route::group(['prefix'=>'/web-api/', 'middleware'=>'auth'], function() {
    Route::get('tenants', 'ApiController@tenants');
    Route::get('groups', 'ApiController@groups');
    Route::get('tenants/{tenant}/groups', 'ApiController@groups');
    Route::get('powerbi/groups', 'ApiController@pbiGroups');
    Route::get('powerbi/groups/{group}/reports', 'ApiController@pbiReports');
    Route::get('reports/{report}/token', 'ApiController@pbiReportToken');
});
