<?php

use Illuminate\Database\Seeder;


//Models
use App\Models\User;
use App\Models\Tenant;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create master tenant
        $bytes = new Tenant();
        $bytes->name = 'Bytes Software Services';
        $bytes->type = Tenant::TYPE_MASTER;
        $bytes->save();

        // create master user
        $chris = new User();
        $chris->name = 'Chris Speer (ADMIN)';
        $chris->email = 'chris.speer@bytes.co.uk';
        $chris->password = bcrypt('password');
        $chris->role = User::ROLE_GLOBAL_ADMIN;
        $chris->tenants()->attach($bytes->id);
        $chris->save();

    }

}


