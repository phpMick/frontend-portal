<?php

use Illuminate\Database\Seeder;


//Models
use App\Models\User;
use App\Models\Tenant;
use Faker\Factory as Faker;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        $tenant = Tenant::all()->first();

        for($i=0;$i<10;$i++) {
            DB::table('groups')->insert([
                'name' => $faker->name,
                'description' => $faker->word,
                'tenant_id' => $tenant->id
            ]);
        }


    }

}


