<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Group;
use App\Models\Tenant;

class MasterUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // create master tenant
        $bytes = new Tenant();
        $bytes->name = 'Bytes Software Services';
        $bytes->type = Tenant::TYPE_MASTER;
        $bytes->save();

        // create default group
        $grp = new Group();
        $grp->name = 'default';
        $grp->tenant_id = $bytes->id;
        $grp->description = 'Bytes default reporting group';
        $grp->save();


        $bytes->save();

        // create master user
        $chris = new User();
        $chris->name = 'Chris Speer (ADMIN)';
        $chris->email = 'chris.speer@bytes.co.uk';
        $chris->password = bcrypt('password');
        $chris->role = User::ROLE_GLOBAL_ADMIN;
        $chris->save();
        $chris->groups()->attach($grp);
        $chris->tenants()->attach($bytes);


    }
}
