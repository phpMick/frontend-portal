<?php

use Faker\Generator as Faker;
use App\Models\Tenant;

$factory->define(Tenant::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'type' => Tenant::TYPE_BASIC,
        'allow_ms_login' => $faker->boolean
    ];
});





$factory->state(Tenant::class, 'customer', function(Faker $faker) {
    return [
        'type' => Tenant::TYPE_CUSTOMER
    ];
});

$factory->state(Tenant::class, 'reseller', function(Faker $faker) {
    return [
        'type' => Tenant::TYPE_RESELLER
    ];
});

$factory->afterCreatingState(Tenant::class, 'reseller', function(Tenant $resellerTenant, Faker $faker) {
    $numCustomers = random_int(0,10);
    if($numCustomers>0) {
        $resellerTenant->customers()->saveMany(factory(Tenant::class, $numCustomers)->state('customer')->make());
    }
});
