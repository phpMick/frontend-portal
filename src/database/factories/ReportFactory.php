<?php

use Faker\Generator as Faker;
use App\Models\Report;
use App\Models\Tenant;
use App\Models\Group;

$factory->define(Report::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->sentence,
        //'pbi_group' => $faker->word,
        //'pbi_report' => $faker->word,
        'pbi_config' => env('PBIE_TESTREPORT'),

        'group_id' => factory(Group::class)->create()->id,

    ];
});



