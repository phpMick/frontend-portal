$(document).ready(function () {
    //Set up the Datatable
    $('#groups').DataTable({
        responsive:true,
        "dom": '<"panel panel-default" <"panel-heading" <<"#table-title"> l>> <"panel-body tight" t > <"panel-footer"p> >',
        "bInfo" : false,
        rowId: 'id',
        "language": {
            "emptyTable": "No groups have been added."
        },
        select: true,
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "description" },
            { "data": "tenant" }
        ]

    });

    //The panel title
    $("#table-title").html('<b>Groups</b>');

    var dataTable = $('#groups').dataTable();

    $("#datatable-search").keyup(function() {
        dataTable.fnFilter(this.value);
    });

    //row select
    $('#groups tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            disableButtons();
        }
        else {
            dataTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            enableButtons();
        }
    } );

    $('#button').click( function () {
        dataTable.row('.selected').remove().draw( false );
    } );

    //everything is now loaded, so we can show it
    $('.hide-while-loading').removeClass('hide-while-loading');

});

/**
 * Disable all the action buttons.
 */
function disableButtons()
{

    $("#delete-button").prop("disabled",true);
    $("#edit-button").prop("disabled",true);
    $("#view-button").prop("disabled",true);

}

/**
 * Enable all the action buttons.
 */
function enableButtons()
{
    $("#delete-button").prop("disabled",false);
    $("#edit-button").prop("disabled",false);
    $("#view-button").prop("disabled",false);

}

/**
 * Handles the Edit.
 * @returns {boolean}
 */
function btnEditClick()
{
    var selectedId = getSelectedRowId();


    if(selectedId) {
        window.location.replace('/groups/' + selectedId + '/edit');
    }

}

/**
 * Handles the Delete.
 * @returns {boolean}
 */
function btnDeleteClick()
{
    var selectedId = getSelectedRowId();

    if(selectedId) {
        destroy(selectedId);
    }
}


/**
 * Handles the View Details.
 * @returns {boolean}
 */
function btnViewClick() {
    var selectedId = getSelectedRowId();

    if (selectedId) {
        window.location.replace('/groups/' + selectedId);
    }
}



/**
 * Just gets the selected row id from the Datatable
 * @returns {boolean}
 */
function getSelectedRowId(){

    var table = $('#groups').DataTable();

    //if we have a selected row, return it
    if(table.rows( '.selected' ).any()){
        return (table.rows( '.selected' ).data()[0]['id']);
    }else{
        return false;
    }
}





