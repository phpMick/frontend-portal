

/**
 * Clicking the Delete link in the actions
 * list executes this
 *
 * @param $id
 */
function destroy(id) {
    swal({

        text: "Do you really want to delete this tenant?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            //AJAX the delete
            AxiosDestroyTenant(id);
        }
    });
}


/**
 * Do the delete via Ajax
 * @param id
 * @constructor
 */
function AxiosDestroyTenant(id){

    axios.delete('/tenants/' + id, {

    })
        .then(function (response) {
            console.log('OK: ' + response.data.message);
            window.location.replace('/tenants')

        })
        .catch(function (error) {
            if(error.response.status == 403){
                swal({
                    title: "Error",
                    text: error.response.data.message,
                    icon: "error"
                });
            }
            console.log(error);
        });


}


