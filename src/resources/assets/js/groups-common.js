


/**
 * Clicking the Delete link in the actions
 * list executes this
 *
 * @param $id
 */
function destroy(id) {
    swal({

        text: "Do you really want to delete this group?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            //AJAX the delete
            AxiosDestroyGroup(id);
        }
    });
}


/**
 * Do the delete via Ajax
 * @param id
 * @constructor
 */
function AxiosDestroyGroup(id){

    console.log('ABOUT TO DELETE GROUP: URL=/groups/'+id);

    axios.delete('/groups/' + id, {

    })
        .then(function (response) {
            console.log('OK: ' + response.data.message);
            window.location.replace('/groups')
        })
        .catch(function (error) {
            console.log(error);
        });


}


