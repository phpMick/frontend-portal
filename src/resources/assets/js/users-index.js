$(document).ready(function () {
    //Set up the Datatable

    $('#users').DataTable({
        responsive:true,
        "dom": '<"panel panel-default" <"panel-heading" <<"#table-title"> l>> <"panel-body tight" t > <"panel-footer"p> >',
        "bInfo" : false,
        rowId: 'id',
        "language": {
            "emptyTable": "No users have been created."
        },
        select: true,
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "role" },
            { "data": "status" }

        ]

    });

    //The panel title
    $("#table-title").html('<b>Users</b>');

    var dataTable = $('#users').dataTable();

    $("#datatable-search").keyup(function() {
        dataTable.fnFilter(this.value);
    });

    //row select
    $('#users tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            disableButtons();
        }
        else {
            dataTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            enableButtons();
        }
    } );



    $('#button').click( function () {
        dataTable.row('.selected').remove().draw( false );
    } );

    //everything is now loaded, so we can show it
    $('.hide-while-loading').removeClass('hide-while-loading');

});

/**
 * Disable all the action buttons.
 */
function disableButtons()
{
    $("#edit-button").prop("disabled",true);
    $("#delete-button").prop("disabled",true);
    $("#view-button").prop("disabled",true);

}

/**
 * Enable all the action buttons.
 */
function enableButtons()
{
    $("#edit-button").prop("disabled",false);
    $("#delete-button").prop("disabled",false);
    $("#view-button").prop("disabled",false);

}

/**
 * Handles the Edit.
 * @returns {boolean}
 */
function btnEditClick()
{
    var selectedId = getSelectedRowId();

    if(selectedId) {
        window.location.replace('/users/' + selectedId + '/edit');
    }
}





/**
 * Handles the Delete.
 * @returns {boolean}
 */
function btnDeleteClick()
{
    var selectedId = getSelectedRowId();

    if(selectedId) {
        destroy(selectedId);
    }
}

/**
 * Handles the View Details.
 * @returns {boolean}
 */
function btnViewClick()
{
    var selectedId = getSelectedRowId();

    if(selectedId) {
        window.location.replace('/users/' + selectedId);
    }
}


/**
 * Just gets the selected row from the Datatable
 * @returns {boolean}
 */
function getSelectedRowId(){

    var table = $('#users').DataTable();

    //if we have a selected row, return it
    if(table.rows( '.selected' ).any()){
        return (table.rows( '.selected' ).data()[0]['id']);
    }else{
        return false;
    }
}
