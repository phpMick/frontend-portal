
function pbitype_select() {
    selectedType = $('#pbi-type>select').val();
    if(selectedType=='public') {
        // pbi_selected_type = 'public';
        $('#pbi-url').removeClass('hidden');
        $('#pbi-group').addClass('hidden');
    } else {
        if(selectedType=='private') {
            // pbi_selected_type = 'private';
            $('#pbi-url').addClass('hidden');
            $('#pbi-group').removeClass('hidden');
            pbigroups_load();
        } else {
            // hide everything
            // pbi_selected_type = '';
            $('#pbi-url').addClass('hidden');
            $('#pbi-group').addClass('hidden');
        }
    }
}

function pbigroups_load(refresh)
{
    if ( refresh === undefined ) {
        refresh = false;
    }


    var url = '/web-api/powerbi/groups' + ((!!refresh) ? '?refreshIt=1' : '');
    $('#pbi-report').addClass('hidden');
    loadOptionsFromApi(url, $('#pbi-group div.input-group'), false, pbigroups_select);
}


function pbigroups_select()
{
    var newGroupId = $('#pbi-group select').val();
    if (newGroupId=='-') {
        // no group selected
        $('#pbi-report').addClass('hidden');
    } else {
        // a group is selected - load reports:
        $('#pbi-report').removeClass('hidden');
        pbireports_load(false);
    }
}

function pbireports_load(refresh)
{
    if ( refresh === undefined ) {
        refresh = false;
    }


    var grp = $('#pbi-group select').val();
    var url = '/web-api/powerbi/groups/'+grp+'/reports' + ((!!refresh) ? '?refresh=1' : '');
    loadOptionsFromApi(url, $('#pbi-report div.input-group'), false);
}

function tenant_select()
{
    var newTenantId = $('#selTenant>select').val();

    if (newTenantId=='-') {
        // hide group selector:
        $('#newTenant').addClass('hidden');
        $('#grpGroup').addClass('hidden');
    } else if (newTenantId=='+') {
        // show basic tenant creation fields:
        $('#newTenant').removeClass('hidden');//.find('input').val('');
        // show basic group creation fields:
        var grp = $('#grpGroup').removeClass('hidden');
        grp.find('span>span.glyphicon').attr('class','glyphicon'); // remove all icons
        grp.find('span>button').attr('disabled', 'disabled');
        $('#group_id').html('<option value="+" selected="selected">-- Create New --</option>');
        $('#newGroup').removeClass('hidden');//.find('input').val('');
    } else {
        // show group selector
        $('#newTenant').addClass('hidden');
        var grp = $('#grpGroup').removeClass('hidden');
        grp.find('span>button').removeAttr('disabled');
        $('#newGroup').addClass('hidden');
        groups_load();
    }
}

function groups_select()
{
    var selectedGroupId = $('#group_id').val();
    if (selectedGroupId=='+') {
        // show basic group creation fields:
        $('#newGroup').removeClass('hidden');
    } else {
        $('#newGroup').addClass('hidden');
    }
}

function tenants_load()
{
    loadOptionsFromApi('/web-api/tenants', $('#selTenant'), true, tenant_select);
    // --select-- item selected by default, so clear/hide everything:
    $('#newTenant').addClass('hidden');//.find('input').val('');
    $('#grpGroup').addClass('hidden');
    $('#newGroup').addClass('hidden');//.find('input').val('');
}

function groups_load()
{
    var tenantId = $('#selTenant>select').val();
    loadOptionsFromApi('/web-api/tenants/'+tenantId+'/groups', $('#grpGroup'), true, groups_select);
    $('#newGroup').addClass('hidden');//.find('input').val('');
}


$(document).ready(function() {
    tenants_load();
    pbitype_select();
});

function loadOptionsFromApi(url, inputGroup, includeAdd, afterLoadCallback)
{
    if ( includeAdd === undefined ) {
        includeAdd = false;
    }

    if ( afterLoadCallback === undefined ) {
        afterLoadCallback = null;
    }



    var sel = inputGroup.find('select');
    var ico = inputGroup.find('span>span.glyphicon');
    // reset options list:
    sel.html('<option value="-" selected="selected">Loading...</option>');
    // set spinner icon:
    ico.addClass('glyphicon-cog').addClass('icon-spin');
    // get data from api:
    axios.get(url).then(function(response) {
        // is there an old value present?
        var old = inputGroup.find("input[type='hidden']");
        if (old.length>0) {
            old = old.val();
        } else {
            old = '';
        }
        options = '<option value="-"' + (old=='' ? ' selected="selected"' : '') + '>-- Select --</option>';
        if (includeAdd) {
            options += '<option value="+"' + (old=='+' ? ' selected="selected"' : '') + '>-- Create New --</option>';
        }
        response.data.data.forEach(function(item) {
            options += '<option value="'+item.id+'"' + (old==item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>';
        });
        sel.html(options);
        ico.removeClass('icon-spin').removeClass('glyphicon-cog').addClass('glyphicon-ok');
        if (afterLoadCallback!=null) {
            afterLoadCallback();
        }
    }).catch(function(error) {
        var options = '<option value="-" selected="selected">-- ERROR - NO DATA --</option>';
        sel.html(options);
        ico.removeClass('icon-spin').removeClass('glyphicon-cog').addClass('glyphicon-remove');
    })
}
