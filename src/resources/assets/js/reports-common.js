


/**
 * Clicking the Delete link in the actions
 * list executes this
 *
 * @param $id
 */
function destroy(id) {
    swal({

        text: "Do you really want to delete this report?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            //AJAX the delete
            AxiosDestroyReport(id);
        }
    });
}


/**
 * Do the delete via Ajax
 * @param id
 * @constructor
 */
function AxiosDestroyReport(id){

    console.log('ABOUT TO DELETE REPORT: URL=/reports/'+id);

    axios.delete('/reports/' + id, {

    })
        .then(function (response) {
            console.log('OK: ' + response.data.message);
            window.location.replace('/reports')
        })
        .catch(function (error) {
            console.log(error);
        });


}


