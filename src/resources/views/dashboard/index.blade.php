@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            @foreach($reports as $report)
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a class="tile tile-report" href="{!! route('reports.view', ['report'=>$report]) !!}">
                        <div class="tile-title"><span>{{ $report->name }}</span></div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>


@endsection