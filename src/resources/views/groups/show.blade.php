@extends('layouts/main')

@section('title', 'Group')


@section('include-css')
    <link  href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Group</div>
                    <div class="panel-body">

                        {{--Name--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Group</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$group->name}}</span>
                            </div>
                        </div>

                        {{--Description--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Description</strong>
                            </div>

                            <div class="col-md-6 link">
                                {{$group->description}}
                            </div>
                        </div>


                        {{--Tenant--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Tenant</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$group->tenant->name}}</span>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">

                        @can('update',$group)
                            <a class="btn btn-primary" href="{{route('groups.edit', [$group]) }}">Edit</a>
                        @endcan

                        @can('destroy',$group)
                            <a class="btn btn-danger" onclick="destroy('{{$group->getRouteKey()}}')">Delete</a>
                        @endcan


                    </div>
                </div> {{--panel--}}

                @include('groups/partials/user-list')


                <div>
                    <a class="btn btn-default" a href="{{ route('groups.index') }}"><span class="icon-return"></span>Back</a>
                </div>


            </div>
        </div> {{--container--}}
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ mix('js/groups-common.js') }}"></script>
@endsection


@section('script')
    <script>
        $(document).ready(function() {
            $('#users').DataTable();
        } );

    </script>
@endsection


