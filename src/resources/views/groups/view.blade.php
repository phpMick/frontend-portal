@extends('layouts.fullscreen')

@section('title', 'View Report')

@section('content')

<iframe id="iframe_pbi" width="100%" height="100%" src="" frameborder="0" allowFullScreen="true"></iframe>

@endsection


@section('scripts')
<script type="application/javascript">
<!--
    var token = '{{ $report->token }}';
    var auth = '{{ md5($report->token . time() . str_random(16)) }}';

    var pbi_url = 'https://app.powerbi.com/view?r='+token+'&auth='+auth;

    document.getElementById('iframe_pbi').src = pbi_url;
-->
</script>

@endsection

