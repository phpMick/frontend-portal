{{--Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label required">Name</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$group->name) }}" required autofocus>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>

{{--Description--}}
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description" class="col-md-4 control-label">Description</label>

    <div class="col-md-6">
        <input id="description" type="text" class="form-control" name="description" value="{{ old('description',$group->description) }}" autofocus>

        @if ($errors->has('description'))
            <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>

{{--Tenant--}}
<div class="form-group{{ $errors->has('tenant_id') ? ' has-error' : '' }}">
    <label for="tenant" class="col-md-4 control-label required">Tenant</label>

    <div class="col-md-6">

        <select class="form-control" name="tenant_id" required>
            <option value = "" selected="true" disabled="disabled">Please select ...</option>
            @foreach($tenants as $tenantid=>$tenantname)
                <option value="{{$tenantid}}"  @if(old('tenant_id', $group->tenant ? $group->tenant->getRouteKey() : NULL) == $tenantid) selected = 'selected'  @endif>{{$tenantname}}</option>
            @endforeach
        </select>

        @if ($errors->has('tenant_id'))
            <span class="help-block"><strong>{{ $errors->first('tenant') }}</strong></span>
        @endif
    </div>
</div>









