@extends('layouts.main')

@section('title', 'Tenant')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Tenant</div>
                    <div class="panel-body">

                        {{--Name--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Name</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$tenant->name}}</span>
                            </div>
                        </div>


                        {{--Type--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Type</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{ucfirst($tenant->typeString())}}</span>
                            </div>
                        </div>


                        {{--Allow MS Login--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Allow Microsoft Login</strong>
                            </div>
                            <div class="col-md-6">
                                <span>
                                    @if ($tenant->allow_ms_login ==1)
                                        True
                                    @else
                                        False
                                    @endif
                                </span>
                            </div>
                        </div>


                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-primary mr1" href="{{ route('tenants.edit', [$tenant]) }}">Edit Tenant</a>
                        <a class="btn btn-danger" onclick="destroy('{{$tenant->getRouteKey()}}')">Delete Tenant</a>
                    </div>
                </div> {{--panel--}}



                <div>
                    <a class="btn btn-default" a href="{{ route('tenants.index') }}"><span class="icon-return"></span>Back to Tenant list</a>
                </div>



            </div>
        </div> {{--container--}}
    </div>
@endsection

@section('script')

    <script src="{{ mix('js/tenants-common.js') }}"></script>

@endsection