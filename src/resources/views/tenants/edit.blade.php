@extends('layouts.main')

@section('title', 'Edit a Tenant')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Tenant</div>
                    <div class="panel-body">
                        @include('partials/forms/errors')
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ route('tenants.update',[$tenant->getRouteKey()]) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}

                            @include('tenants/partials/fields')

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Tenant
                                    </button>

                                    <a class="btn btn-link" href="{{ route('tenants.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div> {{--panel body--}}
            </div> {{--panel--}}
            </div>
        </div> {{--row--}}
    </div> {{--container--}}

@endsection