{{--Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label required">Name</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control required" name="name" value="{{ old('name',$tenant->name) }}"
               required autofocus>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>

{{--Type--}}
<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
    <label for="type" class="col-md-4 control-label">Type</label>

    <div class="col-md-6">

        <select class="form-control" name="type">
            <option value = "" selected="true" disabled="disabled">Please select ...</option>
                @foreach($types as $typeid=>$typename)
                    <option value="{{$typeid}}"  @if(old('type', $tenant->type ? $tenant->type : NULL) == $typeid) selected = 'selected'  @endif>{{$typename}}</option>
                @endforeach
        </select>

        @if ($errors->has('type'))
            <span class="help-block"><strong>{{ $errors->first('type') }}</strong></span>
        @endif
    </div>
</div>

{{--MS Login--}}

<div class="form-group">
    <label for="allow_ms_login" class="col-md-4 control-label">Allow Microsoft Login</label>

    <div class="col-md-6">
        <input id="allow_ms_login" type="checkbox" class="" name="allow_ms_login" value="1" @if(old('allow_ms_login', $tenant->allow_ms_login) == 1) checked = 'checked'  @endif>
    </div>
</div>





