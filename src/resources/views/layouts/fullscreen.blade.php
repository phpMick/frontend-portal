<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(View::hasSection('title')) {{ 'Cloud Portal - '  }} @yield('title') @else {{ 'Cloud Portal' }} @endif</title>

    <!-- Styles -->
    <link href="/css/bootstrap-3.3.7.css" rel="stylesheet" />

    <link href="{{ mix('/css/site.css') }}" rel="stylesheet" />

    <style type="text/css">
        html,body {
            height:100%;
        }

        div.container-fullscreen {
            height:100%; min-height:100%;
        }

        div.fill {
            width:100%; height:100%; min-height:100%;
        }

        nav.navbar {
            position: absolute; top:0px; height:50px; left:0px; width:100%;
        }

        div.content-fullscreen {
            height:100%; padding-top:50px;
        }
    </style>
    @yield('include-css')



    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>



</head>
<body>

<div class="container-fullscreen">
    <div class="fill">
        @include('partials.nav')

        <div class="content-fullscreen">
            @yield('content')
        </div>
    </div>
</div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>

    @yield('scripts')
    @yield('script')
</body>
</html>
