<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(View::hasSection('title')) {{ 'Cloud Portal - '  }} @yield('title') @else {{ 'Cloud Portal' }} @endif</title>

    <!-- Styles -->
    <link href="/css/bootstrap-3.3.7.css" rel="stylesheet" />

    <link href="{{ mix('/css/site.css') }}" rel="stylesheet" />

    @yield('include-css')


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>



</head>
<body>
    <div style="margin-bottom:120px;" id="app"> {{--The Vue application--}}
        @include('partials.nav')

        @include('partials.message-bar')

        @yield('content')

    </div>


    @include('partials.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>

    @yield('scripts')
    @yield('script')
</body>
</html>
