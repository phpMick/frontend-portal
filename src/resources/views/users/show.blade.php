@extends('layouts.main')

@section('title', 'User')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">User</div>
                    <div class="panel-body">

                        {{--Name--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Name</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->name}}</span>
                            </div>
                        </div>

                        {{--Email--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Email</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->email}}</span>
                            </div>
                        </div>

                        {{--Tenant--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Tenant</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->defaultTenant()->name}}</span>
                            </div>
                        </div>

                        {{--Role--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Role</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->roleString()}}</span>
                            </div>
                        </div>


                        {{--Groups--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Groups</strong>
                            </div>
                            <div class="col-md-6">
                                <span>
                                    {{$user->groups->pluck('name')->implode(',')}}
                                </span>
                            </div>
                        </div>



                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-primary mr1" href="{{ route('users.edit', [$user]) }}">Edit User</a>

                        <a class="btn btn-danger" onclick="destroy('{{$user->getRouteKey()}}')">Delete User</a>

                    </div>
                </div> {{--panel--}}



                <div>
                    <a class="btn btn-default" a href="{{ route('users.index') }}"><span class="icon-return"></span>Back to User list</a>
                </div>



            </div>
        </div> {{--container--}}
    </div>
@endsection

@section('script')

    <script src="{{ mix('js/users-common.js') }}"></script>

@endsection