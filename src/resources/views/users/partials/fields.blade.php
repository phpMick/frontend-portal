{{--Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label required">Name</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control required" name="name" value="{{ old('name',$user->name) }}"
               required autofocus>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>

{{--Email--}}
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label required">Email</label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email',$user->email) }}" required autofocus>

        @if ($errors->has('email'))
            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
</div>

{{--Role--}}
<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
    <label for="role" class="col-md-4 control-label required">Role</label>

    <div class="col-md-6">

        <select class="form-control" name="role">
            @foreach($roles as $roleid=>$rolename)
                <option value="{{$roleid}}"  @if(old('role', $user->role) == $roleid) selected = 'selected'  @endif>{{$rolename}}</option>
            @endforeach
        </select>

        @if ($errors->has('role'))
            <span class="help-block"><strong>{{ $errors->first('role') }}</strong></span>
        @endif
    </div>
</div>

{{--Tenant--}}
<input type="hidden" name="tenant_id" value="{{ $currentTenant->getObfuscatedId() }}" />
<div class="form-group">
    <label for="tenant" class="col-md-4 control-label">Tenant</label>

    <div class="col-md-6" style="padding-top:7px;">
        {{ $currentTenant->name }}
    </div>
</div>
{{--
<div class="form-group{{ $errors->has('tenant_id') ? ' has-error' : '' }}">
    <label for="tenant" class="col-md-4 control-label required">Tenant</label>

    <div class="col-md-6">

        <select class="form-control" name="tenant_id">
            <option value = "" selected="true" disabled="disabled">Please select ...</option>
            @foreach($tenants as $id=>$name)
                <option value="{{$id}}"  @if(old('tenant_id', $user->defaultTenant() ? $user->defaultTenant()->getRouteKey() : NULL) == $id) selected = 'selected'  @endif>{{$name}}</option>
            @endforeach
        </select>

        @if ($errors->has('tenant_id'))
            <span class="help-block"><strong>{{ $errors->first('tenant') }}</strong></span>
        @endif
    </div>
</div>
--}}

{{--Groups--}}
<div class="form-group">
    <label for="groups" class="col-md-4 control-label">Groups</label>

    <div class="col-md-6">

        @foreach($currentTenant->groups as $group)
            <input type="checkbox" name="groups[]" value="{{$group->id}}" {{ $assignedGroups->contains($group) ? 'checked' : ''}}> {{$group->name}} <br>
        @endforeach

    </div>
</div>




