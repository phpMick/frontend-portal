@extends('layouts.main')

@section('title', 'List Users')


@section('include-css')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/responsive.dataTables.min.css') }}" rel="stylesheet"/>
@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">
            {{--The column for search and actions--}}
            <div class="col-xs-12 col-md-2   col-md-push-10  col-lg-2 col-lg-push-10 hide-while-loading">

                {{--The search panel--}}
                    <div class="panel panel-default">
                        <div class="panel-heading">Search Users</div>
                        <div class="panel-body">
                            <div class="form-group">
                                {{--<label for="search">Search:</label>--}}
                                <input id= "datatable-search" type="search" class="form-control" id="email">
                            </div>
                        </div> {{--body--}}
                    </div> {{--panel--}}

                    {{--The actions panel--}}
                    <div class="panel panel-default">
                        <div class="panel-heading">Actions</div>
                        <div class="panel-body">
                            <a href="{{ route('users.create') }}" class="btn btn-success btn-block">New</a>
                            <button type="button" id="edit-button" class="btn btn-primary btn-block" onclick="btnEditClick()" disabled>Edit</button>
                            <button type="button" id="delete-button" class="btn btn-danger btn-block" onclick="btnDeleteClick()" disabled>Delete</button>
                            <button type="button" id="view-button" class="btn btn-primary btn-block" onclick="btnViewClick()" disabled>View Details</button>
                            <br>
                        </div> {{--body--}}
                    </div> {{--panel--}}

            </div> {{--col--}}


            {{--The Datatable panel--}}
            <div class="col-xs-12 col-md-pull-2 col-md-10 col-lg-offset-2 col-lg-8 col-lg-pull-2">
                <table class="table table-striped table-bordered dataTable hide-while-loading" id="users" style="width:100%">
                    <thead>
                        <tr>
                            <th class="negative" >ID</th>
                            <th class="negative" >Name</th>
                            <th class="negative" >Role</th>
                            <th class="negative" >Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->getRouteKey() }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->roleString() }}</td>
                            <td>{{ $user->statusString() }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> {{--Datatable panel--}}

        </div> {{--row--}}

    </div> {{--container--}}

@endsection

@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datatables.select.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>


    <script src="{{ mix('js/users-common.js') }}"></script>
    <script src="{{ mix('js/users-index.js') }}"></script>
    <script type="text/javascript">
        var selectedTenantName = "{{ optional(App\Models\Tenant::selectedTenant())->name }}";
    </script>
@endsection

