
@extends('layouts.main')

@section('title', 'Bytes ')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center" style="margin-bottom:80px;">
                    <img src="/ui/bss-logo.png" height="187" width="535"/>
                    <h1>Cloud Insights Portal</h1>
                </div>



                <h3 class ="text-center">{{ Session::get('message-title') }}</h3>
                <h4 class ="text-center">{{ Session::get('message-detail') }}</h4>

            </div>
        </div>
    </div>
    </div>
@endsection






