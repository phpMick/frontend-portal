@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                <div class="text-center" style="margin-bottom:80px;">
                    <img src="/ui/bss-logo.png" height="187" width="535"/>
                    <h1>Cloud Insights Portal</h1>
                </div>
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    @include('partials/forms/login')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
