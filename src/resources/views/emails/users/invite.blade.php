@component('mail::message')
# Hello {{$user->name}}

You have been invited to use Bytes Cloud Insights Portal.

To access the system, please accept this invitation by clicking the button below.


@component('mail::button', ['url' => route('password.create', ['token ' => $token]), 'color'=>'green'])
    Accept Invite
@endcomponent

@component('mail::button', ['url' => route('invite.reject',$user), 'color'=>'red'])
    Reject Invite
@endcomponent

Once you have accepted this invite, you may access the system using the button below.

@component('mail::button', ['url' => route('home'), 'color'=>'blue'])
    Access Bytes Cloud Insights Portal
@endcomponent



Thanks,<br/>
The Cloud Insights Portal Team

@endcomponent
