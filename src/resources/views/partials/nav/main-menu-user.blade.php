{{--Reports--}}
@if (count($reports) > 0)
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">Reports<span class="caret"></span></a>
        <ul class="dropdown-menu">

            @foreach($reports as $report)
                <li><a href="{{ route('reports.view', $report) }}">{{$report->name}}</a></li>
            @endforeach

        </ul>
    </li>
@else
    <li><a href="{{route('dashboard')}}">Dashboard</a></li>
@endif
