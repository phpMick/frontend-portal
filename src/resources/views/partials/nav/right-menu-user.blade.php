


    <ul class="nav navbar-nav navbar-right">
{{--
        @can('seeAdminMenuOptions')
            <li class="user-group-selector" >
                <bytes-user-group-selector
                        options-string="{{$userGroups}}"
                        title="User Group"
                        initial-id="{{$selectedUserGroupID}}">
                </bytes-user-group-selector>
            </li> --}}{{--app--}}{{--

        @endcan--}}

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="glyphicon glyphicon-user" aria-hidden="true"
                                                   style="margin:0px 10px;"></span> <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout
                        </a>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    {{-- only show list of tenants if user can access more than one --}}
                    @if (isset($allowedTenants) AND count($allowedTenants)>1)
                        <hr>
                        @foreach($allowedTenants as $tenant)
                            <li>
                                <a href="{{ route('tenant.select',$tenant) }}">
                                    {{$tenant->name}}
                                    @if(session('selectedTenant') == $tenant->id)
                                        <span class="selected-tenant">*</span>
                                    @endif

                                </a>
                            </li>
                        @endforeach
                    @endif


                </ul>
            </li>
    </ul>

