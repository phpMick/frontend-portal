
{{--Administration--}}
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="/users">Users</a></li>
        <li><a href="/tenants">Tenants</a></li>
        <li><a href="/reports">Reports</a></li>
        <li><a href="/groups">Groups</a></li>

    </ul>
</li>
