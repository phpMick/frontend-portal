

<nav class="navbar navbar-default navbar-static-top" style="padding: 0px 30px;">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}" style="margin:0px; padding:0px;">&nbsp;</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if (Auth::check())
                    @include('partials/nav/main-menu-user')
                    @if (Auth::user()->isGlobalAdmin())
                        @include('partials/nav/main-menu-globaladmin')
                    @endif
                @else
                    @include('partials/nav/main-menu-guest')
                @endif
            </ul>

        <!-- Right Side Of Navbar -->

            <!-- Authentication Links -->
            @if (Auth::guest())
                @include('partials/nav/right-menu-guest')
            @else

                @include('partials/nav/right-menu-user')

            @endif

        </div>






</nav>
