{{--This is the message bar--}}
<div class="container-fluid">

    {{--Message Bar--}}

    <div class="row">

        <div class="col-xs-8 col-lg-offset-2">

            @if(session()->has('message'))
                <div class="alert alert-info alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('message') }}.
                </div>
            @endif

            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('success') }}.
                </div>
            @endif


            @if(session()->has('info'))
                <div class="alert alert-info alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('info') }}.
                </div>
            @endif


            @if(session()->has('warning'))
                <div class="alert alert-warning alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('warning') }}.
                </div>
            @endif


            @if(session()->has('danger'))
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('danger') }}.
                </div>
            @endif

        </div>

    </div>


</div>