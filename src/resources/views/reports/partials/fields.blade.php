{{--Name--}}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label required">Name</label>

    <div class="col-md-7">
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$report->name) }}" required autofocus>

        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>

{{--Description--}}
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description" class="col-md-4 control-label">Description</label>

    <div class="col-md-7">
        <input id="description" type="text" class="form-control" name="description" value="{{ old('description',$report->description) }}" autofocus>

        @if ($errors->has('description'))
            <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>




{{--PBI Type--}}
<div class="form-group{{ $errors->has('pbi_type') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label required">Power BI Report</label>

    <div class="col-md-7" id="pbi-type">
        {{-- select report type --}}
        <select class="form-control" name="pbi_type" onchange="pbitype_select()">
            <option value="-" @if (in_array(old('pbi_type', $report->type()),['','-'])) {{'selected="selected"'}} @endif>-- Select --</option>
            <option value="public" @if (old('pbi_type', $report->type())=='public') {{'selected="selected"'}} @endif>{{ \App\Models\Report::typeName(\App\Models\Report::TYPE_PBI_PUBLIC) }}</option>
            <option value="private" @if (old('pbi_type', $report->type())=='private') {{'selected="selected"'}} @endif>{{ \App\Models\Report::typeName(\App\Models\Report::TYPE_PBI_PRIVATE) }}</option>
        </select>
        @if ($errors->has('pbi_type'))
            <span class="help-block"><strong>{{ $errors->first('pbi_type') }}</strong></span>
        @endif
    </div>
</div>


{{--PBI URL--}}
<div id="pbi-url" class="form-group hidden{{ $errors->has('pbi_url') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label required">Power BI Shared Link URL</label>

    <div class="col-md-7">
        <div class="input-group input-group-sm">
            <span class="input-group-addon">URL:</span>
            <textarea type="text" class="form-control" name="pbi_url" style="resize:none;height:105px;">{{ old('pbi_url',$report->getUrl()) }}</textarea>
        </div>
        @if ($errors->has('pbi_url'))
            <span class="help-block"><strong>{{ $errors->first('pbi_url') }}</strong></span>
        @endif
    </div>
</div>


{{--PBI Workspace--}}
<div id="pbi-group" class="form-group hidden{{ $errors->has('pbi_group') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label required">Power BI Embedded Workspace</label>

    <div class="col-md-7">
        <div class="input-group">
            <input type="hidden" name="pbi_group_old" value="{{ old('pbi_group', $report->pbi_group) }}" />
            <span class="input-group-addon">Workspace:&nbsp;<span class="glyphicon" style="width:1em;"></span></span>
            <select class="form-control" name="pbi_group" onchange="pbigroups_select()">
                <option value="-">Loading...</option>
            </select>
            <span class="input-group-btn">
                    <button class="btn btn-success" type="button" onclick="pbigroups_load(true)"><span class="glyphicon glyphicon-refresh"></span></button>
                </span>
        </div>
        @if ($errors->has('pbi_group'))
            <span class="help-block"><strong>{{ $errors->first('pbi_group') }}</strong></span>
        @endif
    </div>
</div>

{{--PBI Report--}}
<div id="pbi-report" class="form-group hidden{{ $errors->has('pbi_report') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label required">Power BI Embedded Report</label>

    <div class="col-md-7">
        <div class="input-group">
            <input type="hidden" name="pbi_report_old" value="{{ old('pbi_report', $report->pbi_report) }}" />
            <span class="input-group-addon">Report:&nbsp;<span class="glyphicon" style="width:1em;"></span></span>
            <select class="form-control" name="pbi_report">
                <option value="-">Loading...</option>
            </select>
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" onclick="pbireports_load(true)"><span class="glyphicon glyphicon-refresh"></span></button>
            </span>
        </div>
        @if ($errors->has('pbi_report'))
            <span class="help-block"><strong>{{ $errors->first('pbi_report') }}</strong></span>
        @endif
    </div>
</div>

{{-- Tenant (this will need to be changed if we ever allow users to create their own reports) --}}
<div id="grpTenant" class="form-group{{ $errors->has('tenant_id') ? ' has-error' : '' }}">
    <label for="tenant_id" class="col-md-4 control-label required">Tenant</label>

    <div class="col-md-7">
        <div id="selTenant" class="input-group">
            <input type="hidden" name="tenant_id_old" value="{{ old('tenant_id', optional(optional($report->group)->tenant)->getRouteKey()) }}" />
            <span class="input-group-addon"><span class="glyphicon" style="width:1em;"></span></span>
            <select class="form-control" name="tenant_id" onchange="tenant_select()">
                <option value="-" selected="selected">Loading...</option>
            </select>
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" onclick="tenants_load()"><span class="glyphicon glyphicon-refresh"></span></button>
            </span>
        </div>

        @if ($errors->has('tenant_id'))
            <span class="help-block"><strong>{{ $errors->first('tenant_id') }}</strong></span>
        @endif

        {{-- basic new tenant form --}}
        <div id="newTenant" class="hidden{{ $errors->has('tenant_name') ? ' has-error' : '' }}">
            <div class="input-group input-group-sm input-nested">
                <span class="input-group-addon">Tenant Name:</span>
                <input type="text" class="form-control" name="tenant_name" value="{{ old('tenant_name') }}" />
            </div>
            @if ($errors->has('tenant_name'))
                <span class="help-block input-nested"><strong>{{ $errors->first('tenant_name') }}</strong></span>
            @endif
        </div>
    </div>
</div>

{{--Group--}}
<div id="grpGroup" class="hidden form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
    <label for="group_id" class="col-md-4 control-label required">Group</label>

    <div class="col-md-7">
        <div id="selGroup" class="input-group">
            <input type="hidden" name="group_id_old" value="{{ old('group_id', optional($report->group)->getRouteKey()) }}" />
            <span class="input-group-addon"><span class="glyphicon" style="width:1em;"></span></span>
            <select id="group_id" class="form-control" name="group_id" onchange="groups_select()">
                <option value="-">-- Select --</option>
            </select>
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" onclick="groups_load()"><span class="glyphicon glyphicon-refresh"></span></button>
            </span>
        </div>

        @if ($errors->has('group_id'))
            <span class="help-block"><strong>{{ $errors->first('group_id') }}</strong></span>
        @endif

        <div id="newGroup" class="hidden{{ $errors->has('group_name') ? ' has-error' : '' }}">
            <div class="input-group input-group-sm input-nested">
                <span class="input-group-addon">Group Name:</span>
                <input type="text" class="form-control" name="group_name" value="{{ old('group_name') }}" />
            </div>

            @if ($errors->has('group_name'))
                <span class="help-block input-nested"><strong>{{ $errors->first('group_name') }}</strong></span>
            @endif
        </div>

    </div>
</div>








