@extends('layouts.fullscreen')

@section('title', 'View Report')

@section('content')

<div id="reportContainer" style="height:100%"></div>

@endsection


@section('scripts')
<script src="/js/powerbi.min.js" type="text/javascript"></script>
<script type="application/javascript">
<!--

    // Get models. models contains enums that can be used.
    var models = window['powerbi-client'].models;

    var token_expiry = {{ isset($token) ? $token->expires : 0 }};

    var embedConfiguration = {
        type: 'report',
        embedUrl: '{{ $report->getUrl() }}',
        tokenType: models.TokenType.Embed,
        accessToken: '{{ isset($token) ? $token->token : '' }}'
    };

    var reportContainer = $('#reportContainer');

    if ({{ isset($token) ? 'true' : 'false' }}) {
        var report = powerbi.embed(reportContainer.get(0), embedConfiguration);
    } else {
        reportContainer.html('<div class="row"><div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1"><div class="panel panel-danger" style="margin-top:5em;"><div class="panel-heading"><h2 class="panel-title">ERROR</h2></div><div class="panel-body">Unfortunately there was an error producing your report. Please try again later</div></div></div></div>');
    }


-->
</script>

@endsection

