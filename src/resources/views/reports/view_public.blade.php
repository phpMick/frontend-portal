@extends('layouts.fullscreen')

@section('title', 'View Report')

@section('content')

<iframe id="iframe_pbi" width="100%" height="100%" src="" frameborder="0" allowFullScreen="true"></iframe>

@endsection


@section('scripts')
<script type="application/javascript">
<!--
    var token = '{{ md5($report->pbi_config . time() . str_random(16)) }}';

    var pbi_url = '{{ $report->getUrl() }}'+'&auth='+token;

    document.getElementById('iframe_pbi').src = pbi_url;
-->
</script>

@endsection

