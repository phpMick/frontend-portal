@extends('layouts/main')

@section('title', 'Edit Report')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Report</div>
                    <div class="panel-body">
                        @include('partials/forms/errors')
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ route('reports.update',[$report]) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}


                            @include('reports/partials/fields')
                          
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>

                                    <a class="btn btn-link" href="{{ route('reports.index') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script src="/js/reports-edit.js" type="text/javascript"></script>
@endsection