@extends('layouts/main')

@section('title', 'Report')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Report</div>
                    <div class="panel-body">

                        {{--Name--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Name</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$report->name}}</span>
                            </div>
                        </div>

                        {{--Description--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Description</strong>
                            </div>

                            <div class="col-md-6 link">
                                {{$report->description}}
                            </div>
                        </div>

                        {{--Type--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>PBI Report Type</strong>
                            </div>

                            <div class="col-md-6 link">
                                {{ \App\Models\Report::typeName($report->type()) }}
                            </div>
                        </div>

                        {{--URL--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>PBI Report URL</strong>
                            </div>

                            <div class="col-md-6 link">
                                {{$report->getUrl()}}
                            </div>
                        </div>


                        {{--Tenant--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Tenant</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$report->group->tenant->name}}</span>
                            </div>
                        </div>

                        {{--Group--}}
                        <div class="row mt1">
                            <div class="col-md-4 text-right">
                                <strong>Group</strong>
                            </div>
                            <div class="col-md-6">
                                <span>{{$report->group->name}}</span>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">

                        @can('update',$report)
                            <a class="btn btn-primary" href="{{route('reports.edit', [$report]) }}">Edit</a>
                        @endcan

                        @can('destroy',$report)
                            <a class="btn btn-danger" onclick="destroy('{{$report->getRouteKey()}}')">Delete</a>
                        @endcan

                        @can('view',$report)
                                <a class="btn btn-warning" href="{{route('reports.view', [$report]) }}">View Report</a>
                        @endcan


                    </div>
                </div> {{--panel--}}




                <div>
                    <a class="btn btn-default" a href="{{ route('reports.index') }}"><span class="icon-return"></span>Back</a>
                </div>


            </div>
        </div> {{--container--}}
    </div>
@endsection


@section('scripts')
    <script src="{{ mix('js/reports-common.js') }}"></script>
@endsection
