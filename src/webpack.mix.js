let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


//Thoughts
/*
//should the files be in resources, not public?
*/




//MB
//CSS------------------------------------------------
mix.styles([
    'resources/assets/css/site.css',
], 'public/css/site.css');


//JS------------------------------------------------
//groups
mix.scripts('resources/assets/js/groups-common.js', 'public/js/groups-common.js');
mix.scripts('resources/assets/js/groups-index.js', 'public/js/groups-index.js');
//reports
mix.scripts('resources/assets/js/reports-common.js', 'public/js/reports-common.js');
mix.scripts('resources/assets/js/reports-index.js', 'public/js/reports-index.js');
mix.scripts('resources/assets/js/reports-edit.js', 'public/js/reports-edit.js');
//tenants
mix.scripts('resources/assets/js/tenants-common.js', 'public/js/tenants-common.js');
mix.scripts('resources/assets/js/tenants-index.js', 'public/js/tenants-index.js');
//users
mix.scripts('resources/assets/js/users-common.js', 'public/js/users-common.js');
mix.scripts('resources/assets/js/users-index.js', 'public/js/users-index.js');



//now version all those files
mix.version(['public/css/site.css',
    'public/js/groups-common.js',
    'public/js/groups-index.js',
    'public/js/reports-common.js',
    'public/js/reports-index.js',
    'public/js/reports-edit.js',
    'public/js/tenants-common.js',
    'public/js/tenants-index.js',
    'public/js/users-common.js',
    'public/js/users-index.js',


]);






//OLD
/*
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
*/
