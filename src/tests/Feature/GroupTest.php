<?php

namespace Tests\Feature;

use Tests\TestCase;


use App\Models\Tenant;
use App\Models\Group;
use App\Models\Report;


class GroupTest extends TestCase
{


    /**
     * @test
     * @throws \Exception
     */
    public function global_admin_gets_menu_option()
    {
        $this->signInGlobalAdmin();

        $this->get(route('dashboard'))
            ->assertSee('Groups');
    }


    /**
     * @test
     * @throws \Exception
     */
    public function global_admin_can_see_groups_list()
    {
        $this->signInGlobalAdmin();

        $this->get(route('groups.index'))
            ->assertSee('New');
    }

    /**
     * @test
     * @throws \Exception
     */
    public function user_cannot_see_groups_list()
    {
        $user = $this->signInUser();

        $this->get(route('groups.index'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function unauthenticated_cannot_see_groups_list()
    {
        $this->get(route('groups.index'))
            ->assertRedirect(route('login'));
    }

    //view create------------------------------------------------------------------------------------------------------------view create

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_create_groups()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('groups.create'))
            ->assertSee('Add a Group');
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_create_groups()
    {
        $user = $this->signInUser();

        $this->get(route('groups.create'))
            ->assertStatus(403);
    }
    //store------------------------------------------------------------------------------------------------------------store

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_create_group()
    {
        $this->signInGlobalAdmin();

        $group = make(Group::class);
        $group->tenant_id = Tenant::encodeId($group->tenant_id);

        $this->POST(route('groups.store'),$group->toArray());

        $this->assertDatabaseHas('groups',['name' => $group['name']]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_create_groups()
    {
        $this->signInUser();
        $group = make(Group::class);
        $group->tenant_id = Tenant::encodeId($group->tenant_id);

        $this->POST(route('groups.store'),$group->toArray())
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_create_groups()
    {
        $this->POST(route('groups.store'),[])
            ->assertRedirect(route('login'));
    }

    //show--------------------------------------------------------------------------------------------------------------show

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_groups()
    {
        $user = $this->signInGlobalAdmin();
        $group = create(Group::class);

        $response = $this->get(route('groups.show', ['id' => $group->getRouteKey()]))
            ->assertSee($group->name);

    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_groups()
    {
        $user = $this->signInUser();
        $group = create(Group::class);

        $this->get(route('groups.show', ['id' => $group->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_view_groups()
    {
        $group = create(Group::class);

        $this->get(route('groups.show', ['id' => $group->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //edit--------------------------------------------------------------------------------------------------------------edit

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_edit_groups()
    {
        $user = $this->signInGlobalAdmin();

        $group = create(Group::class);

        $this->get(route('groups.edit', ['id' => $group->getRouteKey()]))
            ->assertSee($group->name);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_edit_groups()
    {
        $user = $this->signInUser();

        $group = create(Group::class);

        $this->get(route('groups.edit', ['id' => $group->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_edit_groups()
    {
        $group = create(Group::class);

        $this->get(route('groups.edit', ['id' => $group->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //update------------------------------------------------------------------------------------------------------------update

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_update_groups()
    {
        $this->signInGlobalAdmin();

        $group = create(Group::class);

        $test = $this->PATCH("groups/{$group->getRouteKey()}", [
            'name' => 'changed',
            'description' => 'changed',
            'tenant_id' => Tenant::encodeId($group->tenant_id)
        ]);

        $this->assertDatabaseHas('groups',['name' => 'changed']);
    }


    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_update_groups()
    {
        $group = create(Group::class);

        $test = $this->PATCH("groups/{$group->getRouteKey()}", [])
            ->assertRedirect(route('login'));
    }

    //destroy-----------------------------------------------------------------------------------------------------------destroy


    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_delete_groups()
    {

        $this->signInGlobalAdmin();
        $group = create(Group::class);


        $response = $this->DELETE("/groups/{$group->getRouteKey()}");

        $this->assertSoftDeleted('groups',['id' => $group->id]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_delete_groups()
    {
        $user = $this->signInUser();

        $group = create(Group::class);


        $response = $this->DELETE("/groups/{$group->getRouteKey()}")
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_delete_groups()
    {
        $group = create(Group::class);

        $response = $this->DELETE("/groups/{$group->getRouteKey()}")
            ->assertRedirect('login');
    }


    /**
     * @test
     * @return void
     */
    public function groups_can_be_updated_with_new_tenant()
    {
        $this->signInGlobalAdmin();

        $group = create(Group::class);
        $newTenant = create(Tenant::class);

        $test = $this->PATCH("groups/{$group->getRouteKey()}", [
            'name' => $group->name,
            'description' => 'changed',
            'tenant_id' => Tenant::encodeId($newTenant->id)
        ]);

        $this->assertDatabaseHas('groups',['description' => 'changed' ,'tenant_id' => $newTenant->id]);
    }


    /**
     * @test
     * @return void
     */
    public function deleting_groups_cascades_reports()
    {
        $this->signInGlobalAdmin();

        $tenant = create(Tenant::class);
        $group = create(Group::class,['tenant_id' => $tenant->id]);
        $report = create(Report::class,['group_id' => $group->id]);

        $response = $this->DELETE("/groups/{$group->getRouteKey()}");


        $this->assertSoftDeleted('groups',['id' => $group->id]);
        $this->assertSoftDeleted('reports',['id' => $report->id]);
    }


/**
* @test
*/
public function groups_show_displays_users()
{
    $user = $this->signInGlobalAdmin();
    $group = create(Group::class);
    $user->groups()->attach($group->id);

    $response = $this->get(route('groups.show', ['id' => $group->getRouteKey()]))
        ->assertSee($group->name);


}


}


