<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

use App\Models\User;
use App\Models\Tenant;
use App\Models\Group;


class LoggingTest extends TestCase
{
    /**
     * Quick helper to make setting the mock a one liner
     * @param $type
     * @param $text
     */
    private function shouldReceive($type,$text)
    {
        Log::shouldReceive($type)
            ->once()
            ->withArgs(function ($message) use ($text) {
                return strpos($message, $text) !== false;
            });
    }

    /**
     * @test
     */
    public function login_gets_logged()
    {
        $user = $this->signInGlobalAdmin();

        $this->shouldReceive('info','user-login');
        auth()->login($user);

    }

    //Users-------------------------------------------------------------------------------------------------------------
/**
* @test
*/
public function creating_users_gets_logged()
{
    $this->signInGlobalAdmin();

    $user = make(User::class);
    $tenant = create(Tenant::class);
    $user->tenant_id = Tenant::encodeId($tenant->id);

    $this->shouldReceive('info','user-created');

    $response = $this->POST(route('users.store'),$user->toArray());

}

/**
 * @test
 */
public function updating_users_gets_logged()
{
    $this->signInGlobalAdmin();

    $user = $this->createUser();

    $this->shouldReceive('info','user-updated');

    //parameters
    $test = $this->PATCH("users/{$user->getRouteKey()}", [
        'name' => 'changed',
        'email' => 'changed@yahoo.com',
        'role' => User::ROLE_USER,
        'tenant_id' => Tenant::encodeId($user->defaultTenant()->id)
    ]);
}

    /**
     * @test
     */
    public function deleting_users_gets_logged()
    {
        $this->signInGlobalAdmin();
        $user = create(User::class);

        $this->shouldReceive('info','user-deleted');

        $response = $this->DELETE("/users/{$user->getRouteKey()}");


    }


//Groups-------------------------------------------------------------------------------------------------------------


    /**
     * @test
     */
    public function creating_groups_gets_logged()
    {
        $this->signInGlobalAdmin();

        $group = make(Group::class);
        $group->tenant_id = Tenant::encodeId($group->tenant_id);

        $this->shouldReceive('info','group-created');

        $this->POST(route('groups.store'),$group->toArray());
    }

    /**
     * @test
     */
    public function updating_groups_gets_logged()
    {

        $this->signInGlobalAdmin();

        $group = create(Group::class);

        $this->shouldReceive('info','group-updated');

        $test = $this->PATCH("groups/{$group->getRouteKey()}", [
            'name' => 'changed',
            'description' => 'changed',
            'tenant_id' => Tenant::encodeId($group->tenant_id)
        ]);

    }

    /**
     * @test
     */
    public function deleting_groups_gets_logged()
    {
        $this->signInGlobalAdmin();
        $group = create(Group::class);
        $this->shouldReceive('info','group-deleted');

        $response = $this->DELETE("/groups/{$group->getRouteKey()}");

    }



//Tenants-------------------------------------------------------------------------------------------------------------

    /**
     * @test
     */
    public function creating_tenants_gets_logged()
    {
        $user = $this->signInGlobalAdmin();

        //the above fails because no user loggedin

        $tenant = make(Tenant::class);
        $this->shouldReceive('info','tenant-created');

        $this->POST(route('tenants.store'),$tenant->toArray());
    }

    /**
     * @test
     */
    public function updating_tenants_gets_logged()
    {
        $this->signInGlobalAdmin();

        $group = create(Group::class);
        $tenant = $group->tenant;

        $this->shouldReceive('info','tenant-updated');

        $test = $this->PATCH("tenants/{$tenant->getRouteKey()}", [
            'name' => 'changed',
            'type' => $tenant->type

        ]);
    }

    /**
     * @test
     */
    public function deleting_tenants_gets_logged()
    {
        $this->signInGlobalAdmin();

        $tenant = create(Tenant::class);

        $this->shouldReceive('info','tenant-deleted');

        $response = $this->DELETE("/tenants/{$tenant->getRouteKey()}");

    }



//Reports-------------------------------------------------------------------------------------------------------------


}
