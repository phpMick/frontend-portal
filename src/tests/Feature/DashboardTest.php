<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\Report;
use Tests\TestCase;

use App\Models\User;
use App\Models\Tenant;


class DashboardTest extends TestCase
{
    /**
     * @test
     */
    public function user_can_see_dashboard()
    {
        $user = $this->signInUser();

        $this->get(route('dashboard'))
            ->assertSee('Dashboard');
    }

    /**
     * @test
     */
    public function user_can_see_reports_for_their_group()
    {
        $report = $this->createUserGroupReport(true);

        $this->get(route('dashboard'))
            ->assertRedirect(route('reports.view',$report));
    }


    //todo come back to this after adding no reports message
    /**
     * @test
     */
    /*public function user_cannot_see_reports_for_other_groups()
    {
        $report = $this->createUserGroupReport(false);

        $this->get(route('dashboard'))
            ->assertDontR('View Report');
    }*/









}


