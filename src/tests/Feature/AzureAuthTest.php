<?php

namespace Tests\Feature;


use App\Models\Tenant;
use Tests\TestCase;
use Mockery;

//Models

use App\Models\AadTenant;
use App\Models\AadUser;
use App\Models\User;


//Bytes
use App\CloudServices\Azure\AadLogin;
use App\Connectors\MicrosoftAzureActiveDirectoryAuth;


class AzureAuthTest extends TestCase
{
    //dummy data to retrieve from the MAADA
    private $userDetails = [
        'code' => '123456',
        'name' => 'joe',
        'username' => 'smith',
        'email' => 'joe@phpMick.co.uk',
        'domain' => 'phpMick.co.uk'
    ];

    private $authCode = '123456';

    private $tenant_id;

    public function tearDown()
    {
        Mockery::close();
    }

    //Helpers
    /**
     * Sets up the AadLogin
     * and attempts the login
     * @param $authCode
     * @param $MAADA
     * @throws \Exception
     */
    private function  simulateLogin( $MAADA){

        $requestArray = [];

        $aadLogin = new AadLogin($MAADA);

        $requestArray['code'] = $this->authCode;

        $aadLogin->attemptOAuthLogin($requestArray);

    }

    /**
     * Just does the assert for the session message
     * @param $expectedMessage
     */
    private function checkMessage($expectedMessage)
    {
        $sessionMessage = session('message-title');
        $this->assertEquals($expectedMessage,$sessionMessage);
    }



    /**
     * Mocks the MAADA
     *
     * @return Mockery\MockInterface
     */
    private function createMAADA()
    {
        //mock the MAADA
        $MAADA = Mockery::mock('App\Connectors\MicrosoftAzureActiveDirectoryAuth');
        //add the functions
        $MAADA->shouldReceive('getUserDetails')->once()->with($this->authCode)->andReturn($this->userDetails);

        return $MAADA;
    }

    /**
     * @test
     * @return void
     * @throws \Exception
     */
    public function aad_user_can_login()
    {
        //to pass, all we need is:
            //1, User record with the same email
            //2 All their Tenants must allow MS login

        //Arrange
        $user = create(User::class,['email' => $this->userDetails['email']]);
        $tenant = create(Tenant::class,['allow_ms_login' => 1]);
        $user->tenants()->attach($tenant->id);


        $MAADA = $this->createMAADA();

        //Act
        $this->simulateLogin($MAADA);

        //Assert
        $this->assertAuthenticated();

    }

    //Failure tests

    /**
    * @test
    */
    public function login_fails_without_user_record()
    {
        $MAADA = $this->createMAADA();

        $this->simulateLogin($MAADA);

        $this->checkMessage('Microsoft login not allowed.');
        $this->assertGuest();
    }

    /**
     * @test
     */
    public function login_fails_without_tenant_record()
    {
        $MAADA = $this->createMAADA();
        $user = create(User::class,['email' => $this->userDetails['email']]);

        $this->simulateLogin($MAADA);

        $this->checkMessage('Microsoft login not allowed.');
        $this->assertGuest();
    }

    /**
     * @test
     */
    public function login_fails_if_tenant_does_not_allow()
    {
        $MAADA = $this->createMAADA();
        $user = create(User::class,['email' => $this->userDetails['email']]);
        $tenant = create(Tenant::class,['allow_ms_login' => 0]);
        $user->tenants()->attach($tenant->id);

        $this->simulateLogin($MAADA);

        $this->checkMessage('Microsoft login not allowed.');
        $this->assertGuest();
    }


    /**
     * @test
     */
    public function login_fails_if_one_tenant_does_not_allow()
    {
        $MAADA = $this->createMAADA();
        $user = create(User::class,['email' => $this->userDetails['email']]);
        $tenantAllow = create(Tenant::class,['allow_ms_login' => 1]);
        $tenantNotAllow = create(Tenant::class,['allow_ms_login' => 0]);
        $user->tenants()->attach($tenantAllow->id);
        $user->tenants()->attach($tenantNotAllow->id);

        $this->simulateLogin($MAADA);

        $this->checkMessage('Microsoft login not allowed.');
        $this->assertGuest();
    }





}






