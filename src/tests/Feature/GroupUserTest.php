<?php

namespace Tests\Feature;

use App\Models\Group;
use Tests\TestCase;

use App\Models\User;
use App\Models\Tenant;


class GroupUserTest extends TestCase
{
    /**
     * @test
     */
    public function groups_can_be_edited_from_user()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('users.create'))
            ->assertSee('Groups');
    }


    /**
     * @test
     */
    public function groups_can_be_saved()
    {
        $this->signInGlobalAdmin();

        $user = $this->createUser();
        $groupOne = create(Group::class,['tenant_id'=>$user->defaultTenant()->id]);
        $groupTwo = create(Group::class,['tenant_id'=>$user->defaultTenant()->id]);

        //parameters
        $test = $this->PATCH("users/{$user->getRouteKey()}", [
            'name' => 'changed',
            'email' => 'changed@yahoo.com',
            'role' => User::ROLE_USER,
            'tenant_id' => Tenant::encodeId($user->defaultTenant()->id),
            'groups' => [$groupOne->id, $groupTwo->id]
        ]);

        $this->assertDatabaseHas('group_user',['group_id' => $groupOne->id]);
        $this->assertDatabaseHas('group_user',['group_id' => $groupTwo->id]);


    }






}


