<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\Models\Report;



class ReportTest extends TestCase
{


    /**
     * @test
     * @throws \Exception
     */
    public function user_can_view_their_reports()
    {
        $report = $this->createUserGroupReport(TRUE);

        $this->get(route('reports.view',$report))
            ->assertSee($report->name);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function user_cannot_view_other_groups_reports()
    {
        $report = $this->createUserGroupReport(FALSE);

        $this->get(route('reports.view',$report))
            ->assertStatus(403);
    }







    /**
     * @test
     * @throws \Exception
     */
    public function global_admin_can_see_reports_list()
    {
        $this->signInGlobalAdmin();

        $this->get(route('reports.index'))
            ->assertSee('New');
    }

    /**
     * @test
     * @throws \Exception
     */
    public function user_cannot_see_reports_list()
    {
        $user = $this->signInUser();

        $this->get(route('reports.index'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function unauthenticated_cannot_see_reports_list()
    {
        $this->get(route('reports.index'))
            ->assertRedirect(route('login'));
    }

    //view create------------------------------------------------------------------------------------------------------------view create

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_create_reports()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('reports.create'))
            ->assertSee('Add a Report');
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_create_reports()
    {
        $user = $this->signInUser();

        $this->get(route('reports.create'))
            ->assertStatus(403);
    }
    //store------------------------------------------------------------------------------------------------------------store

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_create_reports()
    {
        $this->signInGlobalAdmin();
        $report = make(Report::class);

        $this->POST(route('reports.store'),$report->toArray());
        $this->assertDatabaseHas('reports',['name' => $report['name']]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_create_reports()
    {
        $this->signInUser();
        $report = make(Report::class);

        $this->POST(route('reports.store'),$report->toArray())
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_create_reports()
    {
        $this->POST(route('reports.store'),[])
            ->assertRedirect(route('login'));
    }

    //show--------------------------------------------------------------------------------------------------------------show

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_reports()
    {
        $user = $this->signInGlobalAdmin();
        $report = create(Report::class);

        $response = $this->get(route('reports.show', ['id' => $report->getRouteKey()]))
            ->assertSee($report->name);

    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_reports()
    {
        $user = $this->signInUser();
        $report = create(Report::class);

        $this->get(route('reports.show', ['id' => $report->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_view_reports()
    {
        $report = create(Report::class);

        $this->get(route('reports.show', ['id' => $report->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //edit--------------------------------------------------------------------------------------------------------------edit

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_edit_reports()
    {
        $user = $this->signInGlobalAdmin();

        $report = create(Report::class);

        $this->get(route('reports.edit', ['id' => $report->getRouteKey()]))
            ->assertSee($report->name);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_edit_reports()
    {
        $user = $this->signInUser();

        $report = create(Report::class);

        $this->get(route('reports.edit', ['id' => $report->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_edit_reports()
    {
        $report = create(Report::class);

        $this->get(route('reports.edit', ['id' => $report->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //update------------------------------------------------------------------------------------------------------------update

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_update_reports()
    {
        $this->signInGlobalAdmin();

        $report = create(Report::class);

        $test = $this->PATCH("reports/{$report->getRouteKey()}", [
            'name' => 'changed',
            'token' => $report->token,
            'group_id' => $report->group_id,
        ]);

        $this->assertDatabaseHas('reports',['name' => 'changed']);
    }


    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_update_reports()
    {
        $report = create(Report::class);

        $test = $this->PATCH("reports/{$report->getRouteKey()}", [])
            ->assertRedirect(route('login'));
    }

    //destroy-----------------------------------------------------------------------------------------------------------destroy


    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_delete_reports()
    {

        $this->signInGlobalAdmin();
        $report = create(Report::class);


        $response = $this->DELETE("/reports/{$report->getRouteKey()}");

        $this->assertSoftDeleted('reports',['id' => $report->id]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_delete_reports()
    {
        $user = $this->signInUser();

        $report = create(Report::class);


        $response = $this->DELETE("/reports/{$report->getRouteKey()}")
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_delete_reports()
    {
        $report = create(Report::class);

        $response = $this->DELETE("/reports/$report->getRouteKey()}")
            ->assertRedirect('login');
    }


}


