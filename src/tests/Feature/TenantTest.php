<?php

namespace Tests\Feature;

use App\Models\Report;
use Tests\TestCase;

use App\Models\Tenant;
use App\Models\Group;


class TenantTest extends TestCase
{

    /**
     * @test
     * @throws \Exception
     */
    public function global_admin_can_see_tenants_list()
    {
        $this->signInGlobalAdmin();


        $this->get(route('tenants.index'))
            ->assertSee('New');
    }

    /**
     * @test
     * @throws \Exception
     */
    public function user_cannot_see_tenants_list()
    {
        $user = $this->signInUser();

        $this->get(route('tenants.index'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function unauthenticated_cannot_see_tenants_list()
    {
        $this->get(route('tenants.index'))
            ->assertRedirect(route('login'));
    }

    //view create------------------------------------------------------------------------------------------------------------view create

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_create_tenants()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('tenants.create'))
            ->assertSee('Add a Tenant');
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_create_tenants()
    {
        $user = $this->signInUser();

        $this->get(route('tenants.create'))
            ->assertStatus(403);
    }
    //store------------------------------------------------------------------------------------------------------------store

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_create_tenants()
    {
        $user = $this->signInGlobalAdmin();

        $tenant = make(Tenant::class);

        $this->POST(route('tenants.store'),$tenant->toArray());
        $this->assertDatabaseHas('tenants',['name' => $tenant['name']]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_create_tenants()
    {
        $this->signInUser();
        $tenant = make(Tenant::class);

        $this->POST(route('tenants.store'),$tenant->toArray())
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_create_tenants()
    {
        $this->POST(route('tenants.store'),[])
            ->assertRedirect(route('login'));
    }

    //show--------------------------------------------------------------------------------------------------------------show



    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_tenants()
    {
        $user = $this->signInGlobalAdmin();

        $group = create(Group::class);

        $response = $this->get(route('tenants.show', ['id' => $group->tenant->getRouteKey()]))
            ->assertSee($group->tenant->name);

    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_tenants()
    {
        $user = $this->signInUser();
        $tenant = create(Tenant::class);

        $this->get(route('tenants.show', ['id' => $tenant->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_view_tenants()
    {
        $tenant = create(Tenant::class);

        $this->get(route('tenants.show', ['id' => $tenant->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //edit--------------------------------------------------------------------------------------------------------------edit

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_edit_tenants()
    {
        $user = $this->signInGlobalAdmin();

        $tenant = create(Tenant::class);

        $this->get(route('tenants.edit', ['id' => $tenant->getRouteKey()]))
            ->assertSee($tenant->name);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_edit_tenants()
    {
        $user = $this->signInUser();

        $tenant = create(Tenant::class);

        $this->get(route('tenants.edit', ['id' => $tenant->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_edit_tenants()
    {
        $tenant = create(Tenant::class);

        $this->get(route('tenants.edit', ['id' => $tenant->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //update------------------------------------------------------------------------------------------------------------update

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_update_tenants()
    {
        $this->signInGlobalAdmin();

        $group = create(Group::class);
        $tenant = $group->tenant;

        $test = $this->PATCH("tenants/{$tenant->getRouteKey()}", [
            'name' => 'changed',
            'type' => $tenant->type

        ]);
        $this->assertDatabaseHas('tenants',['name' => 'changed']);
    }


    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_update_tenants()
    {
        $tenant = create(Tenant::class);

        $test = $this->PATCH("tenants/{$tenant->getRouteKey()}", [])
            ->assertRedirect(route('login'));
    }

    //destroy-----------------------------------------------------------------------------------------------------------destroy


    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_delete_tenants()
    {

        $this->signInGlobalAdmin();
        $tenant = create(Tenant::class);

        $response = $this->DELETE("/tenants/{$tenant->getRouteKey()}");
        $this->assertSoftDeleted('tenants',['id' => $tenant->id]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_delete_tenants()
    {
        $user = $this->signInUser();

        $tenant = create(Tenant::class);

        $response = $this->DELETE("/tenants/{$tenant->getRouteKey()}")
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_delete_tenants()
    {
        $tenant = create(Tenant::class);

        $response = $this->DELETE("/tenants/{$tenant->getRouteKey()}")
            ->assertRedirect('login');
    }


    /**
     * @test
     * @return void
     */
    public function new_tenants_have_AllowMS_option()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('tenants.create'))
            ->assertSee('Allow');
    }

    /**
    * @test
    */
    public function allow_MS_option_is_saved()
    {
        $user = $this->signInGlobalAdmin();

        $tenant = make(Tenant::class);
        $tenant->allow_ms_login = TRUE;

        $this->POST(route('tenants.store'),$tenant->toArray());
        $this->assertDatabaseHas('tenants',['name' => $tenant->name, 'allow_ms_login' => true]);

    }



    /**
     * @test
     * @return void
     */
    public function deleting_tenants_cascades_groups_and_reports()
    {
        $this->signInGlobalAdmin();

        $tenant = create(Tenant::class);
        $group = create(Group::class,['tenant_id' => $tenant->id]);
        $report = create(Report::class,['group_id' => $group->id]);

        $response = $this->DELETE("/tenants/{$tenant->getRouteKey()}");

        $this->assertSoftDeleted('tenants',['id' => $tenant->id]);
        $this->assertSoftDeleted('groups',['id' => $group->id]);
        $this->assertSoftDeleted('reports',['id' => $report->id]);
    }








}


