<?php

namespace Tests\Feature;

//Model
use App\Mail\NewUserMail;

use App\Models\Tenant;
use App\Models\User;



//use App\Mail\NewAADUser;


use Tests\TestCase;
use App\Events\UserCreated;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;


class AuthTest extends TestCase
{

    /**
     * @test
     * @throws \Exception
     */
    public function create_user_fires_event()
    {
        $newUser = $this->setupAndMakeNewUser();

        $this->expectsEvents(UserCreated::class);

        $response = $this->POST(route('users.store'),$newUser->toArray());


    }

    private function setupAndMakeNewUser()
    {
        $user = $this->signInGlobalAdmin();
        $newUser = make(User::class,['role' => User::ROLE_USER]);

        $tenant = create(Tenant::class);
        $newUser->tenant_id = Tenant::encodeId($tenant->id);


        return $newUser;
    }

    private function setupAndCreateNewUser()
    {
        $user = $this->signInGlobalAdmin();

        $newUser = create(User::class,['role' => User::ROLE_USER]);

        $tenant = create(Tenant::class);
        $newUser->tenant_id = Tenant::encodeId($tenant->id);

        return $newUser;
    }

    /**
     * @test
     * @throws \Exception
     */
    public function create_normal_user_sends_email()
    {
        Mail::fake();

        $newUser = $this->setupAndMakeNewUser();

        $this->POST(route('users.store'),$newUser->toArray());

        Mail::assertSent(NewUserMail::class, function ($mail) use ($newUser) {
            return $mail->hasTo($newUser['email']);
        });

    }

    /**
     * @test
     * @throws \Exception
     */
    public function new_user_can_view_reset_password_page()
    {
        //need a token
        $user = create(User::class,['role' => User::ROLE_USER]);
        $tenant = create(Tenant::class);
        $user->tenant_id = Tenant::encodeId($tenant->id);

        //just make token?
        $token = Password::getRepository()->create($user);

        $response = $this->GET(route('password.create',['token ' => $token]))
        ->assertSee('Confirm Password');

    }


    /**
     * @test
     * @throws \Exception
     */
    public function new_user_can_change_their_password()
    {
        $user = $this->setupAndCreateNewUser();

        $oldPassword = $user->password;

        $token = Password::getRepository()->create($user);

        $parameters = [
            'token' => $token,
            'email' => $user->email,
            'password' => 'changedpassword',
            'password_confirmation' => 'changedpassword'
        ];

        auth()->logout();

       $response =  $this->POST('password/reset',$parameters);

        //check password in the db
        $user = $user->refresh();

        $this->assertTrue(Hash::check('changedpassword',$user->password));

    }

    /**
     * @test
     * @throws \Exception
     */
    public function new_user_can_reject_invite()
    {
        $user = create(User::class,['status' => USER::STATUS_INVITED, 'password' => null]);

        $this->get(route('invite.reject',$user));

        $this->assertDatabaseHas('users', ['id' => $user->id, 'status' => User::STATUS_REJECTED]);
    }









}






