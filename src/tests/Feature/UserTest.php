<?php

namespace Tests\Feature;

use App\Models\Group;
use Tests\TestCase;


use App\Models\Tenant;
use App\Models\User;


class UserTest extends TestCase
{


    /**
     * @test
     * @throws \Exception
     */
    public function global_admin_can_see_users_list()
    {
        $this->signInGlobalAdmin();

        $this->get(route('users.index'))
            ->assertSee('New');
    }


    /**
     * @test
     * @throws \Exception
     */
    public function user_cannot_see_users_list()
    {
        $user = $this->signInUser();

        $this->get(route('users.index'))
            ->assertStatus(403);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function unauthenticated_cannot_see_users_list()
    {
        $this->get(route('users.index'))
            ->assertRedirect(route('login'));
    }

    //view create------------------------------------------------------------------------------------------------------------view create

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_create_users()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('users.create'))
            ->assertSee('Add a User');
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_create_users()
    {
        $user = $this->signInUser();

        $this->get(route('users.create'))
            ->assertStatus(403);
    }
    //store------------------------------------------------------------------------------------------------------------store

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_create_users()
    {
        $user = $this->signInGlobalAdmin();
        $this->saveAndSeeUser();
    }

    private function saveAndSeeUser()
    {
        $user = make(User::class);
        $tenant = create(Tenant::class);
        $user->tenant_id = Tenant::encodeId($tenant->id);


        $this->POST(route('users.store'),$user->toArray());

        $this->assertDatabaseHas('users',['name' => $user['name']]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_create_users()
    {
        $this->signInUser();

        $user = make(User::class);

        $this->POST(route('users.store'),$user->toArray())
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_create_users()
    {
        $this->POST(route('users.store'),[])
            ->assertRedirect(route('login'));
    }

    //show--------------------------------------------------------------------------------------------------------------show

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_users()
    {
        $this->signInGlobalAdmin();

        $user = $this->createUser();

        $response = $this->get(route('users.show', ['id' => $user->getRouteKey()]))
            ->assertSee($user->name);

    }




    /**
     * @test
     * @return void
     */
    public function a_user_cannot_view_other_users()
    {
        $user = $this->signInUser();

        $user = create(User::class);

        $this->get(route('users.show', ['id' => $user->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_view_users()
    {
        $user = create(User::class);

        $this->get(route('users.show', ['id' => $user->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //edit--------------------------------------------------------------------------------------------------------------edit

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_view_edit_users()
    {
        $user = $this->signInGlobalAdmin();

        $newUser = create(User::class);

        $this->get(route('users.edit', ['id' => $newUser->getRouteKey()]))
            ->assertSee($newUser->name);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_edit_users()
    {
        $user = $this->signInUser();

        $user = create(User::class,[]);

        $this->get(route('users.edit', ['id' => $user->getRouteKey()]))
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_edit_users()
    {
        $user = create(User::class,[]);

        $this->get(route('users.edit', ['id' => $user->getRouteKey()]))
            ->assertRedirect(route('login'));
    }

    //update------------------------------------------------------------------------------------------------------------update

    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_update_users()
    {
        $this->signInGlobalAdmin();

        $user = $this->createUser();

        //parameters
        $test = $this->PATCH("users/{$user->getRouteKey()}", [
            'name' => 'changed',
            'email' => 'changed@yahoo.com',
            'role' => User::ROLE_USER,
            'tenant_id' => Tenant::encodeId($user->defaultTenant()->id)
        ]);
        $this->assertDatabaseHas('users',['name' => 'changed']);
    }


    /**
     * @test
     * @return void
     */
    public function unauthenticated_cannot_update_customers_agreements()
    {
        $user = create(User::class);
        //parameters
        $this->PATCH("users/{$user->getRouteKey()}", [])
            ->assertRedirect(route('login'));
    }

    //destroy-----------------------------------------------------------------------------------------------------------destroy


    /**
     * @test
     * @return void
     */
    public function a_global_admin_can_delete_users(){

        $this->signInGlobalAdmin();
        $user = create(User::class);

        $response = $this->DELETE("/users/{$user->getRouteKey()}");
        $this->assertSoftDeleted('users',['id' => $user->id]);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_cannot_delete_users()
    {
        $user = $this->signInUser();

        $user = create(User::class);

        $response = $this->DELETE("/users/{$user->getRouteKey()}")
            ->assertStatus(403);
    }

    /**
     * @test
     * @return void
     */
    public function authenticated_cannot_delete_users()
    {
        $user = create(User::class);

        $response = $this->DELETE("/users/{$user->getRouteKey()}")
            ->assertRedirect('login');
    }



    //admin functions----------------------------------------------------------------------------------------------------admin

    //User does not get menu options

    /**
     * @test
     * @return void
     */
    public function user_does_not_get_admin_menu()
    {
        $user = $this->signInUser();

        $this->get(route('home'))
            ->assertDontSee('Administration');
    }
    //Admins gets options

    /**
     * @test
     * @return void
     */
    public function global_admin_does_get_admin_menu()
    {
        $user = $this->signInGlobalAdmin();

        $this->get(route('dashboard'))
            ->assertSee('Administration');
    }












}


