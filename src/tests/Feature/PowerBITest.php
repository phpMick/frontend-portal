<?php

namespace Tests\Feature;

use Bytes\ApiConnector\ApiRecorder;
use Bytes\MicrosoftPowerBIApiConnector\MicrosoftPowerBIApiConnector;
use Bytes\MicrosoftPowerBIApiConnector\MicrosoftPowerBiAuthProvider;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PowerBITest extends TestCase
{
    /**
     * @return \Illuminate\Cache\CacheManager
     */
    private function cache()
    {
        return app('cache');
    }

    /**
     * @return \Bytes\MicrosoftPowerBIApiConnector\MicrosoftPowerBiAuthProvider
     * @throws \Exception
     */
    private function makeAuthProvider($useCache=TRUE)
    {
        $authProvider = MicrosoftPowerBIApiConnector::createAuthProvider(
                env('PBIE_TENANT'),
                env('PBIE_CLIENTID'),
                env('PBIE_USERNAME'),
                env('PBIE_PASSWORD')
            );

        if ($useCache) {
            if ($this->cache()->has('token')) {
                $authProvider->setAccessToken($this->cache()->get('token'));
            }
        }
        return $authProvider;
    }

    private function updateTokenCache(MicrosoftPowerBiAuthProvider $authProvider)
    {
        try {
            $token = $authProvider->getAccessToken();
            if ($token instanceof AccessTokenInterface) {
                $this->cache()->forever('token', json_encode($token));
            }
        } catch (\Exception $e) {
            // do nothing
        }
    }

    private function makeRecorder($filename)
    {
        $recorder = new ApiRecorder();
        $recorder->writeTo($filename, app('filesystem')->disk('powerbi'));
        return $recorder;
    }

    private function writeRecording(ApiRecorder $recorder)
    {
        $recorder->store();
    }


    /**
     * @test
     */
    public function a_token_is_obtained_and_stored()
    {
        $authProvider = $this->makeAuthProvider(FALSE);
        $token = $authProvider->getAccessToken();

        $this->assertInstanceOf(AccessToken::class, $token);
        $this->assertNotEmpty($token->getToken());
        $this->assertNotEmpty($token->getRefreshToken());
        $nearlyAnHourFromNow = time()+(60*58);
        $this->assertGreaterThan($nearlyAnHourFromNow, $token->getExpires());

        $this->updateTokenCache($authProvider);
        $checkTokenJson = $this->cache()->get('token');
        $this->assertNotEmpty($checkTokenJson);
        $checkToken = json_decode($checkTokenJson);
        var_dump($checkToken);
        $this->assertGreaterThan($nearlyAnHourFromNow, $checkToken->expires_on);
        $this->assertNotEmpty($checkToken->access_token);
        $this->assertNotEmpty($checkToken->refresh_token);
    }

    /**
     * @test
     */
    public function a_list_of_reports_can_be_obtained()
    {
        // setup:
        $authProvider = $this->makeAuthProvider();
        $api = new MicrosoftPowerBIApiConnector($authProvider);
        $group = env('PBIE_GROUPID');
        $filename = 'organization/' . (empty($group) ? '' : 'groups/'.$group) . 'reports/'.time().'.log';
        $recorder = $this->makeRecorder($filename);
        $api->setRecorder($recorder);

        // do api call:
        $reports = $api->Reports()->getReports($group);

        // update state:
        $this->writeRecording($recorder);
        $this->updateTokenCache($authProvider);

        // tests:
        $reportList = $reports->decodeJsonResponse();
        $this->assertIsArray($reportList->value);
        $this->assertCount(1, $reportList->value);
        $this->assertNotEmpty($reportList->value[0]->embedUrl);

    }





}
