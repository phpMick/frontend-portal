<?php

namespace Tests;

use App\Models\Group;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\User;
use App\Models\Tenant;
use App\Models\Report;

use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;


    private function  createBytesTenant()
    {

        $bytes = new Tenant();
        $bytes->name = 'Bytes Software Services';
        $bytes->type = Tenant::TYPE_MASTER;
        $bytes->save();

        $group = new Group();
        $group->name = "Bytes Group";
        $group->description = "Description";
        $group->tenant_id = $bytes->id;
        $group->save();




        return $bytes;

    }

    protected function signInGlobalAdmin()
    {
        $user = create(User::class,[ 'role' => User::ROLE_GLOBAL_ADMIN]);

        $tenant = $this->createBytesTenant();

        $user->tenants()->attach($tenant->id);
        $user->groups()->attach($tenant->default_group_id);

        session()->put('selectedTenant', $tenant->id);
        $this->actingAs($user);


        return $user;
    }

    protected function signInResellerAdmin()
    {

        $user = create(User::class,[ 'role' => User::ROLE_RESELLER_ADMIN]);

        $this->actingAs($user);

        return $user;
    }

    protected function signInTenantAdmin()
    {

        $user = create(User::class,[ 'role' => User::ROLE_TENANT_ADMIN]);

        $this->actingAs($user);

        return $user;
    }


    protected function signInUser()
    {
        $tenant = $this->createBytesTenant();

        $user = create(User::class,[ 'role' => User::ROLE_USER]);

        session()->put('selectedTenant', $tenant->id);

        $this->actingAs($user);

        return $user;
    }


    protected function createUserGroupReport($attach)
    {
        $user = $this->signInUser();

        $group = create(Group::class,['tenant_id' => session('selectedTenant')]);
        if($attach){
            $user->groups()->attach($group->id);
        }
        $report = create(Report::class,['group_id' => $group->id]);

        return $report;
    }


    protected function  createUser()
    {
        $user = create(User::class);
        $tenant = create(Tenant::class);
        $user->tenants()->attach($tenant->id);

        return $user;
    }


}
