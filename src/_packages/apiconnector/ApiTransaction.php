<?php

namespace Bytes\ApiConnector;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\TransferStats;

/**
 * A wrapper for the Guzzle Response class to provide a couple of useful functions for API operations
 * @package App\Connectors\MicrosoftAzureApi
 *
 */
class ApiTransaction
{
    const DELIM_REQUEST = "REQUEST>>>>>>>>\r\n";
    const DELIM_RESPONSE = "\r\n\r\nRESPONSE<<<<<<<<\r\n";


    /**
     * @var array
     */
    protected $attributes = [];

    protected $body = NULL;


    /**
     * @var Response
     */
    protected $response;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ApiError|NULL
     */
    protected $error = NULL;

    /**
     * The Guzzle Client to use (must be set if a request is to be executed)
     * @var Client|NULL
     */
    protected $client = NULL;


    /**
     * @var string Response body override - used for caching
     */
    protected $response_body = NULL;

    protected $chainErrorResponse = NULL;

    protected $chainContinue = TRUE;

    protected $httpStatusCode;


    public function __construct(Client $client = NULL) {
        $this->client = $client;
    }

    /**
     * @param Client $client
     * @return $this
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Apply the headers to the request object
     * @param array $headers
     * @throws \Exception
     */
    protected function applyHeadersToRequest($headers)
    {
        if(!($this->request instanceof Request)) {
            throw new \Exception('Request must be instantiated before trying to apply headers');
        }
        if(!is_array($headers)) {
            throw new \Exception('HTTP headers must be an array of strings');
        }
        foreach($headers as $header=>$value) {
            $this->request = $this->request->withHeader($header, $value);
        }
    }

    /**
     * Called by on_stats callback function too allow capture of actual sent request object
     * @param TransferStats $stats
     */
    protected function onRequestSent(TransferStats $stats)
    {
        $this->request = $stats->getRequest();
    }

    /**
     * @param Request $request
     * @param array $options
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendRequest(Request $request, array $options = [])
    {
        if(!($this->client instanceof Client)) {
            throw new \Exception('Cannot make request - HTTP client not set');
        }
        // add on_stats callback to allow capture of complete request object
        $options['on_stats'] = function(TransferStats $stats) {
            $this->onRequestSent($stats);
        };

        $this->response = $this->client->send($request, $options);
        return $this;
    }


    public function sendRequestAsync(Request $request, array $options = [])
    {
        // TODO: add async functionality
        throw new \Exception('Not implemented');
    }

    /**
     * @return Response
     */
    public function response() {
        return $this->response;
    }

    /**
     * @return Request
     */
    public function request() {
        return $this->request;
    }

    public function getStatusCode() {
        return $this->httpStatusCode ?: $this->response->getStatusCode();
    }

    public function getResponseBody() {
        if(isset($this->response_body)) {
            return $this->response_body;
        } else {
            if(!$this->hasError()) {
                $this->response_body = $this->response->getBody()->getContents();
                $this->response->getBody()->rewind();
                return $this->response_body;
            } else {
                return NULL;
            }
        }
    }

    public function decodeJsonResponse($numbersAsStrings=TRUE, $asObject=TRUE)
    {
        $body = $this->getResponseBody();
        // fix up the UTF-8 Byte Order Mark if it's present:
        $utf8bom = implode('', [chr(0xEF), chr(0xBB), chr(0xBF)]);
        if(substr($body, 0, 3)==$utf8bom) {
            $body = substr($body, 3);
        }
        if($numbersAsStrings) {
            $body = preg_replace(
                /**
                 * REGEX PATTERN EXPLANATION:
                 * search for JSON object property names (will always end with [double-quote colon])
                 * then a numeric sequence (possibly with whitespace either side)
                 * followed by the end of the property value (denoted by comma)
                 * or the end of the object (denoted by right-curly-bracket)
                 *
                 * NOTE:
                 * ensures that the match is not in an encoded JSON string (by ensuring that the matching double-quote is not escaped with a backslash)
                 *
                 * LIMITATIONS:
                 * Only finds numeric values that are object properties:
                 *  - single numeric values are not encapsulated (e.g. JSON="123.987")
                 *  - arrays of numeric values are not encapsulated (e.g. JSON="[1,2,3,4,5]")
                 */
                '/([^\\\]"):\s*([0-9\.]+)\s*(,|})/',
                /**
                 * REPLACEMENT EXPLANATION:
                 * re-constitutes all elements with the numeric value encapsulated in double-quotes
                 */
                '$1:"$2"$3',
                $body
            );
        }
        return json_decode($body, !$asObject);
    }


    /**
     * @return boolean
     */
    public function hasError() {
        return !empty($this->error);
    }

    /**
     * @return ApiError|NULL
     */
    public function getError() {
        return $this->error;
    }

    /**
     * Ensures following chained functions give NULLed responses if the response was bad
     * @return $this
     */
    public function errorAsNull() {
        $this->chainErrorResponse = NULL;
        // when body has been set manually, there wasn't a response or status (it was set from cache)
        if(!empty($this->response_body)) {
            return $this;
        }
        if(!$this->response instanceof Response) {
            $this->chainContinue = FALSE;
        } else {
            $status = $this->response->getStatusCode();
            $this->chainContinue = ($status>=200 AND $status<300);
        }
        return $this;
    }

    /**
     * Ensures following chained functions give \stdClass() responses if the response was bad
     * @return $this
     */
    public function errorAsEmptyClass() {
        $this->chainErrorResponse = new \stdClass();
        // when body has been set manually, there wasn't a response or status (it was set from cache)
        if(!empty($this->response_body)) {
            return $this;
        }
        if(!$this->response instanceof Response) {
            $this->chainContinue = FALSE;
        } else {
            $status = $this->response->getStatusCode();
            $this->chainContinue = ($status>=200 AND $status<300);
        }
        return $this;
    }

    /**
     * Ensures following chained functions give empty array responses if the response was bad
     * @return $this
     */
    public function errorAsEmptyArray() {
        $this->chainErrorResponse = [];
        // when body has been set manually, there wasn't a response or status (it was set from cache)
        if(!empty($this->response_body)) {
            return $this;
        }
        if(!$this->response instanceof Response) {
            $this->chainContinue = FALSE;
        } else {
            $status = $this->response->getStatusCode();
            $this->chainContinue = ($status>=200 AND $status<300);
        }
        return $this;
    }

    public function setResponseBody($data, $statusCode=200) {
        $this->response_body = $data;
        $this->httpStatusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function toString()
    {
        // simple dump of response to text, with request and response delimited
        $data  = "REQUEST>>>>>>>>\r\n" . \GuzzleHttp\Psr7\str($this->request());
        $data .= "\r\n\r\nRESPONSE<<<<<<<<\r\n" . \GuzzleHttp\Psr7\str($this->response());
        $this->request()->getBody()->rewind();
        $this->response()->getBody()->rewind();
        return $data;
    }


    /**
     * @param string $data
     * @return ApiTransaction|null
     */
    static public function fromString($data)
    {
        if(strlen($data)>0) {
            // we only want to populate the response with the response part of the audit file:
            $pos = strpos($data, "\r\n\r\nRESPONSE<<<<<<<<\r\n");
            if($pos>0) {
                // now locate the end of the headers:
                $pos2 = strpos($data, "\r\n\r\n", $pos+4); // +4 to skip \r\n\r\n at beginning of RESPONSE line
                if($pos2>0) {
                    $instance = new self();
                    $instance->setResponseBody(substr($data, $pos2+4)); // +4 to skip \r\n\r\n found after headers
                    return $instance;
                }
            }
        }
        return NULL;
    }



}