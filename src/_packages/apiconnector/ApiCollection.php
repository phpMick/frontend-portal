<?php

namespace Bytes\ApiConnector;


class ApiCollection implements \Iterator
{

    /**
     * @var callable|NULL
     */
    protected $itemDecoderFunction = NULL;

    /**
     * @var string|NULL The class to instantiate as the model for each row
     */
    protected $itemClass = NULL;

    /**
     * @var ApiTransaction
     */
    protected $transaction = NULL;

    /**
     * @var array|NULL
     */
    protected $items = NULL;

    /**
     * @var integer|NULL The index in the current data array for the next read. NULL = not loaded
     */
    protected $currentRow = NULL;


    public function __construct($data=NULL)
    {
        $this->setData($data);
    }

    public function setData($data)
    {
        if ($data instanceof ApiTransaction) {
            $this->transaction = $data;
            $this->decodePage();
        } elseif (is_array($data)) {
            $this->items = $data;
        } elseif (!empty($data)) {
            throw new \Exception('Invalid data');
        }
    }

    /**
     * @return ApiTransaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param string $className
     * @throws \Exception
     */
    public function setItemModel($className)
    {
        if (!empty($className) AND class_exists($className) AND is_subclass_of($className, ApiModel::class)) {
            $this->itemClass = $className;
        } else {
            throw new \Exception('Invalid model class');
        }
    }

    /**
     * @return boolean
     */
    public function hasError()
    {
        return ($this->transaction instanceof ApiTransaction) AND $this->transaction->hasError();
    }

    public function getError()
    {
        return optional($this->transaction)->getError();
    }

    /**
     * called at the beginning of each loop
     */
    public function rewind()
    {
        $this->reset();
    }

    /**
     * called to check if there is an item available from the array
     * @return bool
     */
    public function valid()
    {
        //to exit the foreach, this must fail
        return (is_array($this->items) AND ($this->currentRow >= 0) AND ($this->currentRow < count($this->items)));
    }

    /**
     * called to get the current item from the array
     * @return mixed
     */
    public function current()
    {
        $data = $this->items[$this->currentRow];
        if (!empty($this->itemClass)) {
            $className = $this->itemClass;
            return new $className($data);
        }
        if (is_callable($this->itemDecoderFunction)) {
            $decoded = ($this->itemDecoderFunction)($data);
            return $decoded;

        }
        return $data;
    }

    /**
     * called to get the array pointer value
     * @return integer
     */
    public function key()
    {
        return ($this->currentRow <= 0) ? 0 : $this->currentRow;
    }

    /**
     * called to move the array pointer to the next item
     */
    public function next()
    {
        $this->currentRow++;
    }

    /**
     * Clear all data and start again - called in rewind() and also if query parameters are updated
     */
    public function reset()
    {
        $this->currentRow = -1;
    }

    /**
     * @return integer|NULL
     */
    public function count()
    {
        return count($this->items);
    }


    /**
     * @param ApiRecorder $recorder
     * @param string $filename
     * @return bool
     */
    public function saveResponseTo(ApiRecorder $recorder, $filename)
    {
        $this->recorder = $recorder;
        $this->filename = $filename;
    }

    /**
     * @param ApiTransaction $response
     */
    public function loadFromResponse($response)
    {
        if(!($response instanceof ApiTransaction)) {
            throw new \Exception('Not a valid response object');
        }
        $this->transaction = $response;
    }

    /**
     * @return bool|int
     */
    protected function saveResponse()
    {
        // abort if recording not set up
        if(empty($this->filename) OR !($this->recorder instanceof ApiRecorder)) {
            return FALSE;
        }
        return $this->recorder->append($this->transaction, $this->filename);
    }

    protected function decodePage()
    {
        $data = optional($this->transaction)->decodeJsonResponse();
        if (isset($data->items) AND is_array($data->items)) {
            $this->items = $data->items;
        } elseif (isset($data->data) AND is_array($data->data)) {
            $this->items = $data->data;
        }
        $count = NULL;
        // check if the count matches (if exists):
        if (isset($data->totalCount) AND is_numeric($data->totalCount)) {
            $count = intval($data->totalCount);
        } elseif (isset($data->count) AND is_numeric($data->count)) {
            $count = intval($data->count);
        }
        if(isset($count) AND (count($this->items) != $count)) {
            throw new \Exception('Item count does not match parsed array count');
        }
        return $data;
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    public function decodeItem($data)
    {
        if(is_callable($this->itemDecoderFunction)) {
            return ($this->itemDecoderFunction)($data);
        }
        return $data;
    }

    /**
     * @param callable $func
     */
    public function setItemDecoder(callable $func)
    {
        $this->itemDecoderFunction = $func;
    }

}