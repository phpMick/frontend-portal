<?php

namespace Bytes\ApiConnector;


class ApiRepository
{

    /**
     * @var ApiConnector
     */
    protected $apiConnector;

    public function __construct(ApiConnector $api) {
        $this->apiConnector = $api;
    }

    /**
     * @return ApiConnector
     */
    public function getApiConnector() {
        return $this->apiConnector;
    }


}