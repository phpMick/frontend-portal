<?php

namespace Bytes\ApiConnector;


use GuzzleHttp\Psr7\Request;

abstract class ApiAuthenticationProvider
{
    /**
     * @var array
     */
    protected $requiredConfigKeys = [];

    /**
     * @var array
     */
    protected $config = [];

    /**
     * BaseAuthenticationProvider constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config = [])
    {
        // ensure every required config key is present:
        foreach($this->requiredConfigKeys as $key) {
            if(!array_key_exists($key, $config)) {
                throw new \Exception('Required configuration key "'.$key.'" missing"');
            }
        }
        $this->config = $config;
    }

    abstract public function authenticateRequest(Request $request) : Request;

}