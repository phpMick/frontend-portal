<?php

namespace Bytes\ApiConnector;

use function GuzzleHttp\Psr7\str;
use Illuminate\Contracts\Filesystem\Filesystem;

class ApiRecorder
{


    /**
     * Filesystem to read/write, or NULL = local filesystem
     * @var Filesystem|NULL
     */
    private $disk = NULL;

    private $mode = '';

    private $filename = NULL;

    /**
     * @var resource|NULL
     */
    private $stream = NULL;

    private $count = 0;

    private $readBuffer = '';



    /**
     * Sets Recorder to read mode and specifies the source
     * @param resource|string $streamOrFilename If resource, read from stream, if string then it's the filename to use
     * @param Filesystem|NULL $disk If set, use filename from this disk, otherwise use filename from local filesystem
     */
    public function readFrom($streamOrFilename, ?Filesystem $disk=NULL)
    {
        if(!empty($this->mode)) {
            throw new \Exception('Mode of recorder cannot be changed once set');
        }
        if($disk instanceof Filesystem) {
            // read file on flysystem disk:
            if(!is_string($streamOrFilename) OR empty($streamOrFilename) OR !$disk->has($streamOrFilename)) {
                throw new \Exception('File does not exist');
            }
            $this->disk = $disk;
            $this->filename = $streamOrFilename;
            $this->stream = $disk->readStream($streamOrFilename);
        } elseif(is_string($streamOrFilename)) {
            // read local file:
            if(empty($streamOrFilename) OR !file_exists($streamOrFilename)) {
                throw new \Exception('File does not exist');
            }
            $this->disk = NULL;
            $this->filename = $streamOrFilename;
            $this->stream = fopen($streamOrFilename, 'r');
        } elseif(is_resource($streamOrFilename)) {
            // stream has been passed - check it's set up for reading:
            $meta = stream_get_meta_data($streamOrFilename);
            if(!is_array($meta) OR !array_key_exists('mode', $meta)) {
                throw new \Exception('Invalid stream');
            }
            if(strpos($meta['mode'], 'r')===FALSE AND strpos($meta['mode'], '+')===FALSE) {
                throw new \Exception('Stream not open for reading');
            }
            $this->disk = NULL;
            $this->filename = NULL;
            $this->stream = $streamOrFilename;
        } else {
            throw new \Exception('Invalid parameters');
        }
        // ensure that stream was successfully opened for read:
        if(is_null($this->stream) OR $this->stream===FALSE) {
            throw new \Exception('Failed to open file for reading');
        }
        $this->mode = 'read';
        $this->readBuffer = '';
        $this->count = 0;
    }

    /**
     * Sets the final destination stream/file and disk to store transactions to.
     * NOTE: This does NOT store any data to a file - store() must be called to write data to a destination file
     * @param string $streamOrFilename
     * @param Filesystem|null $disk
     */
    public function writeTo($streamOrFilename='temp', ?Filesystem $disk=NULL)
    {
        if(!empty($this->mode)) {
            throw new \Exception('Mode of recorder cannot be changed once set');
        }
        if($disk instanceof Filesystem) {
            // write file to flysystem disk:
            if(!is_string($streamOrFilename) OR empty($streamOrFilename)) {
                throw new \Exception('Filename not set');
            }
            $this->disk = $disk;
            $this->filename = $streamOrFilename;
            // because flysystem doesn't support write streams or appends, we need to write to a local temp stream
            // and wait for the user to call $this->store() to push to the specified file (10MB RAM buffer specified)
            $this->stream = fopen('php://temp/maxmemory:10000000', 'w+');
        } elseif(is_string($streamOrFilename)) {
            // write to local file:
            if(empty($streamOrFilename)) {
                throw new \Exception('File does not exist');
            }
            $this->disk = NULL;
            $this->filename = $streamOrFilename;
            $this->stream = fopen($streamOrFilename, 'w+');
        } elseif(is_resource($streamOrFilename)) {
            // stream has been passed - check it's set up for writing:
            $meta = stream_get_meta_data($streamOrFilename);
            if(!is_array($meta) OR !array_key_exists('mode', $meta)) {
                throw new \Exception('Invalid stream');
            }
            if(strpos($meta['mode'], 'r')!==FALSE) {
                throw new \Exception('Stream not open for writing');
            }
            $this->disk = NULL;
            $this->filename = NULL;
            $this->stream = $streamOrFilename;
        } else {
            throw new \Exception('Invalid parameters');
        }
        // ensure that stream was successfully opened for read:
        if(is_null($this->stream) OR $this->stream===FALSE) {
            throw new \Exception('Failed to open file for writing');
        }
        $this->mode = 'write';
        $this->readBuffer = '';
        $this->count = 0;
    }

    public function store()
    {
        if($this->mode!=='write') {
            throw new \Exception('Recorder not set up for writing');
        }
        // if using flysystem, push temp stream to disk:
        if($this->disk instanceof Filesystem) {
            if(!rewind($this->stream)) {
                throw new \Exception('Failed to rewind to start of temp stream to write to file');
            }
            $this->disk->writeStream($this->filename, $this->stream);
        }
        // for all cases, we now close the stream and assume this instance wil no longer will be used
        fclose($this->stream);
        $this->stream = NULL;
        $this->mode = 'closed';
    }

    public function __destruct()
    {
        $this->close();
    }

    public function close()
    {
        if(is_resource($this->stream)) {
            fclose($this->stream);
        }
        $this->mode = 'closed';
    }


    /**
     * @param Filesystem $disk
     */
//    public function useDisk(Filesystem $disk)
//    {
//        $this->disk = $disk;
//    }

    /**
     * Writes the response to the filesystem
     * @param ApiTransaction $response
     * @param string $filename
     * @return bool
     */
//    public function save(ApiTransaction $response, $filename)
//    {
//        return $this->disk->put($filename,$response->toFile());
//    }


    /**
     * @param string $filename
     * @return ApiTransaction
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
//    public function load($filename)
//    {
//        $fileContents = $this->disk->get($filename);
//        return ApiTransaction::fromFile($fileContents);
//    }

//    public function loadMany($filename)
//    {
//        $fileContents = $this->disk->get($filename);
//        // split out each response:
//        $responseTexts = explode("\r\n\r\nREQUEST>>>>>>>>\r\n", $fileContents);
//        $responses = array();
//        foreach($responseTexts as $responseText) {
//            if(strpos($responseText, "REQUEST>>>>>>>>\r\n")===FALSE) {
//                $responses[] = ApiTransaction::fromFile("REQUEST>>>>>>>>\r\n" . $responseText);
//            } else {
//                $responses[] = ApiTransaction::fromFile($responseText);
//            }
//        }
//        return $responses;
//    }


    /**
     * @param ApiTransaction $response
     * @param string $filename
     * @return bool|int
     */
//    public function append(ApiTransaction $response, $filename)
//    {
//        $writeBuffer = $response->toFile();
//        if($this->disk->exists($filename)) {
//            // if file already exists, then we need to add a separator between previous response and this new one:
//            $writeBuffer = "\r\n\r\n" . $writeBuffer;
//            return $this->disk->append($filename,$writeBuffer,'');
//        } else {
//            return $this->disk->put($filename,$writeBuffer);
//        }
//    }


    protected function bufferReadStream()
    {
        if(!feof($this->stream)) {
            $newBuffer = fread($this->stream, 8192);
            if($newBuffer!==FALSE) {
                $this->readBuffer .= $newBuffer;
            }
        }
    }

    /**
     * Get the next transaction as completely as possible from the source stream
     * @return string
     */
    protected function bufferTransaction()
    {
        $startPos = strlen(ApiTransaction::DELIM_REQUEST);
        // keep loading stream until we see no change in buffer length (i.e. end of stream reached, or other error), or start of new transaction found:
        $buffLen = -1;
        $endPos = strpos($this->readBuffer, ApiTransaction::DELIM_REQUEST, $startPos);
        while ($endPos===FALSE AND $buffLen!==strlen($this->readBuffer)) {
            $buffLen = strlen($this->readBuffer);
            $this->bufferReadStream();
            $endPos = strpos($this->readBuffer, ApiTransaction::DELIM_REQUEST, $startPos);
        }
        if($endPos===FALSE) {
            $data = $this->readBuffer;
            $this->readBuffer = '';
        } else {
            $data = substr($this->readBuffer, 0, $endPos);
            $this->readBuffer = substr($this->readBuffer, $endPos);
            if($this->readBuffer===FALSE) {
                $this->readBuffer = ''; // should never get here, but just in case we mess up the logic in future!
            }
        }
        if($data===FALSE) {
            $data = '';
        }
        return $data;
    }

    /**
     * Ensures the buffer has the start of a transaction. Returns TRUE if so, FALSE if not possible
     * @return bool
     */
    protected function bufferTransactionStart()
    {
        // does buffer hold start of transaction?
        if (substr($this->readBuffer, 0, strlen(ApiTransaction::DELIM_REQUEST))!==ApiTransaction::DELIM_REQUEST) {
            // no - try reading stream:
            $this->bufferReadStream();
            // check again - if it still doesn't hold the start of a transaction, then we assume there is no more available
            // this is a handy work-around for socket timeouts, left-open streams and so forth
            // however, in strange circumstances, we may only read a small number of bytes that don't complete the transaction start
            // but we ignore that as it's extremely unlikely
            return substr($this->readBuffer, 0, strlen(ApiTransaction::DELIM_REQUEST)) === ApiTransaction::DELIM_REQUEST;
        }
        return TRUE;
    }

    /**
     * Reads next transaction from source (if configuration allows). If none available, NULL is returned
     * @return ApiTransaction|NULL
     * @throws \Exception
     */
    public function readNext()
    {
        // check we're explicitly in read mode:
        if ($this->mode!='read') {
            throw new \Exception('Recorder is not in read mode');
        }
        // initialise return string:
        if (!$this->bufferTransactionStart()) {
//            throw new \Exception('No transactions available to read');
            return NULL;
        }
        return ApiTransaction::fromString($this->bufferTransaction());
    }

    /**
     * @param ApiTransaction $transaction
     * @return bool|int
     * @throws \Exception
     */
    public function writeNext(ApiTransaction $transaction)
    {
        if($this->mode!=='write') {
            throw new \Exception('Recorder not in write mode');
        }
        return fwrite($this->stream, $transaction->toString());
    }

    /**
     * @return string Returns '', 'read', 'write' or 'closed' depending on configuration/state
     */
    public function mode()
    {
        return $this->mode;
    }



}