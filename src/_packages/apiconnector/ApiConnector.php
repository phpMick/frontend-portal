<?php

namespace Bytes\ApiConnector;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ApiConnector
{

    /**
     * @var array
     */
    protected $repositories = array();

    /**
     * @var Client|NULL
     */
    protected $client = NULL;

    /**
     * The default options to apply to each request.
     *
     * @var array
     */
    protected $defaultOptions = [
        'http_errors' => FALSE
    ];

    /**
     * @var ApiAuthenticationProvider|NULL
     */
    protected $authProvider = NULL;

    /**
     * @var ApiRecorder|NULL
     */
    protected $recorder = NULL;


    /**
     *
     * @param ApiAuthenticationProvider|NULL $authProvider
     */
    public function __construct(ApiAuthenticationProvider $authProvider = NULL)
    {
        $this->authProvider = $authProvider;
    }

    /**
     * @return Client|NULL
     */
    public function getClient() {
        if(!isset($this->client)) {

            $this->client = new Client($this->defaultOptions);
        }
        return $this->client;
    }

    /**
     * @param string $className
     * @return ApiRepository
     * @throws \Exception
     */
    protected function getRepository($className)
    {
        // check if module has already been instantiated:
        if (!array_key_exists($className, $this->repositories)) {
            // split out logic for different possible HasModules scenarios
            // ensure we're always passing an instance of ApiConnector into the constructor:
            if ($this instanceof ApiConnector) {
                $this->repositories[$className] = new $className($this);
            } elseif ($this instanceof ApiRepository) {
                // allows us to have modules within modules, but they're always created with the ApiConnector instance
                $this->repositories[$className] = new $className($this->getApiConnector());
            } else {
                throw new \Exception('Modules not supported in this class');
            }
        }
        return $this->repositories[$className];
    }

    /**
     * Set up recording of all API operations using specified ApiRecorder instance
     * @param ApiRecorder $recorder
     */
    public function setRecorder(ApiRecorder $recorder)
    {
        $this->recorder = $recorder;
    }

    /**
     * Record an API transaction
     * @param ApiTransaction $transaction
     */
    protected function saveToRecorder(ApiTransaction $transaction)
    {
        if($this->recorder instanceof ApiRecorder) {
            $this->recorder->writeNext($transaction);
        }
    }

    protected function loadFromRecorder()
    {
        if(!($this->recorder instanceof ApiRecorder)) {
            throw new \Exception('API recorder source not set');
        }

    }

    /**
     * @param string $url
     * @param array $options
     * @return ApiTransaction
     */
    public function get($url, $options=[]) {
        $request = new Request('GET', $url);
        return $this->dispatchRequest($request, $options);
    }

    /**
     * @param string $url
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiTransaction
     */
    public function post($url, $body=NULL, $options=[]) {
        if(is_object($body) OR is_array($body)) {
            $body = json_encode($body);
        }
        $request = new Request('POST', $url, [], $body);
        return $this->dispatchRequest($request, $options);
    }

    /**
     * @param string $url
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiTransaction
     */
    public function put($url, $body=NULL, $options=[]) {
        if(is_object($body) OR is_array($body)) {
            $options['json'] = $body;
        }
        $request = new Request('PUT', $url);
        return $this->dispatchRequest($request, $options);
    }

    /**
     * @param string $url
     * @param array $options
     * @return ApiTransaction
     */
    public function delete($url, $options=[]) {
        $request = new Request('DELETE', $url);
        return $this->dispatchRequest($request, $options);
    }

    /**
     * @param string $url
     * @param string|object|array|NULL $body
     * @param array $options
     * @return ApiTransaction
     */
    public function patch($url, $body=NULL, $options=[]) {
        if(is_object($body) OR is_array($body)) {
            $body = json_encode($body);
        }
        $request = new Request('PATCH', $url, [], $body);
        return $this->dispatchRequest($request, $options);
    }

    /**
     * @param array $options
     * @param bool|ApiAuthenticationProvider|null $default
     * @return bool|ApiAuthenticationProvider|null
     */
    protected function getAuthOption(&$options, $default = TRUE)
    {
        $auth = NULL;
        if(is_array($options) AND array_key_exists('auth', $options)) {
            // guzzle allows string|array|null, we allow AuthProvider|boolean:
            if(is_bool($options['auth'])) {
                $auth = $options['auth'];
            } elseif($options['auth'] instanceof ApiAuthenticationProvider) {
                $auth = $options['auth'];
            }
        }
        // remove auth key if it was a type we understood, and return
        if(isset($auth)) {
            unset($options['auth']);
            return $auth;
        } else {
            return $default;
        }
    }

    /**
     * @param Request $request
     * @param array $options
     * @return ApiTransaction
     */
    protected function dispatchRequest(Request $request, $options=[]) {
        // check if recorder is in use and is in read mode:
        if(($this->recorder instanceof ApiRecorder) AND $this->recorder->mode()=='read') {
            // create the transaction and populate data from recorder source:
            $transaction = $this->recorder->readNext();
            // TODO: checks to ensure the read transaction is roughly equivalent to the request

        } else {
            // create our Transaction object:
            $transaction = new ApiTransaction($this->getClient());
            // add authentication if desired:
            $auth = $this->getAuthOption($options);
            if($auth instanceof ApiAuthenticationProvider) {
                $request = $auth->authenticateRequest($request);
            } elseif(is_bool($auth) AND $auth AND ($this->authProvider instanceof ApiAuthenticationProvider)) {
                $request = $this->authProvider->authenticateRequest($request);
            }
            // issue request:
            $transaction->sendRequest($request, $options);
            // save request & response if desired:
            if($this->recorder instanceof ApiRecorder) {
                $this->recorder->writeNext($transaction);
            }
        }
        return $transaction;
    }

    /**
     * @param string $url
     */
    public function setBaseUrl($url)
    {
        // set default option for client creation:
        if(!is_array($this->defaultOptions)) {
            $this->defaultOptions = [];
        }
        $this->defaultOptions['base_uri'] = $url;
        // delete client so next request forces recreation:
        $this->client = NULL;
    }

    /**
     * @param ApiAuthenticationProvider $authProvider
     */
    public function setAuthenticationProvider(ApiAuthenticationProvider $authProvider)
    {
        $this->authProvider = $authProvider;
    }

}