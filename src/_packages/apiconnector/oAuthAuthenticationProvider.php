<?php

namespace Bytes\ApiConnector;


use League\OAuth2\Client\Token\AccessToken;

abstract class oAuthAuthenticationProvider extends ApiAuthenticationProvider
{

    /**
     * @var array
     */
    protected $requiredConfigKeys = ['clientId', 'clientSecret', 'redirectUri'];


    /**
     * @var AccessToken
     */
    protected $oauthToken = NULL;


    abstract public function getOAuthClient();


    /**
     * @param string $attribute
     * @param mixed $default
     * @return mixed
     */
    protected function getConfig($attribute, $default=NULL)
    {
        return array_key_exists($attribute, $this->config) ? $this->config[$attribute] : $default;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     */
    protected function setConfig($attribute, $value)
    {
        $currentValue =  array_key_exists($attribute, $this->config) ? $this->config[$attribute] : NULL;
        if($currentValue!==$value) {
            $this->config[$attribute] = $value;
            // reset client to force re-authentication & application of changed value
            $this->oauthClient = NULL;
            $this->oauthToken = NULL;
        }
    }


    /**
     * @param string|array $scope
     * @return $this
     */
    public function setScope($scope)
    {
        $delimiter = optional($this->getOAuthClient())->getScopeSeparator();
        if(empty($delimiter)) {
            $delimiter = ',';
        }
        if(is_string($scope)) {
            $scope = explode($delimiter, $scope);
        }
        if(!is_array($scope)) {
            throw new \Exception('Invalid scope');
        }
        $this->setConfig('scope', $scope);
        return $this;
    }

    /**
     * @return array
     */
    public function getScope()
    {
        return $this->getConfig('scope',[]);
    }



    /**
     * @param string $grant_type
     * @param array $options
     * @return AccessToken
     */
    protected function requestAccessToken($grant_type='client_credentials', $options=[])
    {
        return $this->getOAuthClient()->getAccessToken($grant_type, $options);
    }

    /**
     * @return AccessToken|NULL
     */
    public function getAccessToken() {
        if(($this->oauthToken instanceof AccessToken) AND $this->oauthToken->hasExpired()) {
            $refreshToken = $this->oauthToken->getRefreshToken();
            if(!empty($refreshToken)) {
                $this->oauthToken = $this->requestAccessToken('refresh_token', ['refresh_token'=>$refreshToken]);
            } else {
                $this->oauthToken = NULL;
            }
        }
        // have we got a valid token?
        if($this->oauthToken instanceof AccessToken) {
            return $this->oauthToken;
        }

        $this->oauthToken = $this->requestAccessToken();
        return $this->oauthToken;
    }

    /**
     * @param AccessToken|array|object|string $token
     * @return $this
     */
    public function setAccessToken($token)
    {
        if(is_string($token)) {
            $token = json_decode($token);
        }
        if($token instanceof AccessToken) {
            $this->oauthToken = $token;
            return $this;
        }
        if(is_object($token)) {
            // generic object, check for parameters:
            $tokenArray = [];
            foreach(['access_token','resource_owner_id','refresh_token','expires'] as $attr) {
                if(!empty($token->$attr)) {
                    $tokenArray[$attr] = $token->$attr;
                }
            }
            if(array_key_exists('access_token', $tokenArray)) {
                $token = $tokenArray;
            }
        }
        if(is_array($token)) {
            try {
                $this->oauthToken = new AccessToken($token);
                return $this;
            } catch (\Exception $e) {
                // do nothing - NULL token forces everything to try logging in again
            }
        }
        $this->oauthToken = NULL;
        return $this;
    }



    protected function loadState(array $state)
    {
        // get token out of state array:
        $token = NULL;
        if(array_key_exists('token', $state)) {
            $token = $state['token'];
            unset($state['token']);
        }
        // save values to config array which is used when the oauth client is instantiated:
        foreach($state as $attribute=>$value) {
            if(in_array($attribute, ['clientId', 'clientSecret', 'redirectUri'])) {
                continue;
            }
            $this->config[$attribute] = $value;
        }
        // ensure oauth stuff is reset:
        $this->oauthClient = NULL;
        $this->oauthToken = NULL;
        // load token if available:
        if(is_array($token)) {
            if(array_key_exists('id_token', $token) AND !empty($token['id_token'])) {
                $this->config['id_token'] = $token['id_token'];
            }
            try {
                // check for token validity:
                if(array_key_exists('expires', $token) AND time()>=$token['expires']) {
                    // not valid any more... check for refresh token:
                    if(array_key_exists('refresh_token', $token)) {
                        $this->oauthToken = $this->refreshOAuthToken($token['refresh_token']);
                    }
                } else {
                    $this->oauthToken = new AccessToken($token, $this->getOAuthClient());
                }
            } catch (\Exception $e) {
                // do nothing - NULL token forces everything to try logging in again
            }
        }
        return $this;
    }

    /**
     * @param string|array $state
     * @param array $config
     * @return static
     * @throws \Exception
     */
    static public function createFromSavedState($state, $config)
    {
        if(is_string($state)) {
            $state = json_decode($state, TRUE);
        }
        if(is_array($state)) {
            // check both arrays have clientId specified:
            if(!array_key_exists('clientId', $state) OR !array_key_exists('clientId', $config)) {
                throw new \Exception('clientId not specified');
            }
            // check clientId is not empty & matches:
            if(empty($state['clientId']) OR $state['clientId']!=$config['clientId']) {
                throw new \Exception('clientId does not match');
            }
            return (new static($config))->loadState($state);
        } else {
            return new static($config);
        }
    }

    /**
     * @return array
     */
    public function getState($encodeAsJson=TRUE)
    {
        $state = $this->config;
        // remove secrets from state array:
        unset($state['clientSecret']);
        unset($state['redirectUri']);
        unset($state['scope']);
        // add token:
//        $state['token'] = $this->oauthToken;
        return $encodeAsJson ? json_encode($state) : $state;
    }


    public function getUser()
    {
        return $this->getOAuthClient()->getResourceOwner($this->getAccessToken());
    }



}