<?php


namespace Bytes\ApiConnector;


use Ramsey\Uuid\Uuid;

class ApiModel implements \JsonSerializable
{

    /**
     * @var ApiTransaction|NULL
     */
    protected $transaction = NULL;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var mixed|null
     */
    protected $data = NULL;

    public $decodeNumbersAsStrings = TRUE;

    protected $isValid = NULL;


    /**
     * @var callable|NULL
     */
    protected $decodeTransactionFunc = NULL;

    /**
     * @var callable|NULL
     */
    protected $decodeDataFunc = NULL;

    public function __construct($data = [], $decodeDataFunction=NULL, $decodeTransactionFunction=NULL)
    {
        if(is_callable($decodeDataFunction)) {
            $this->decodeDataFunc = $decodeDataFunction;
        }
        if(is_callable($decodeTransactionFunction)) {
            $this->decodeTransactionFunc = $decodeTransactionFunction;
        }
        if(!empty($data)) {
            $this->load($data);
        }
    }

    protected function decodeTransactionBody()
    {
        if(isset($this->transaction) AND !$this->transaction->hasError()) {
            $contentType = $this->transaction->response()->getHeader('Content-Type');
            if(is_array($contentType) AND count($contentType)==1) {
                $contentType = $contentType[0];
            }
            if(is_string($contentType)) {
                if(str_contains(strtolower($contentType), '/json')) {
                    $this->data = $this->transaction->decodeJsonResponse($this->decodeNumbersAsStrings);
                }
            }
        }
    }

    /**
     * @param ApiTransaction|array|object $data
     * @return $this|null
     * @throws \Exception
     */
    public function load($data)
    {
        // clear internal state:
        $this->data = NULL;
        $this->transaction = NULL;
        $this->isValid = FALSE;
        if($data instanceof ApiTransaction) {
            $this->transaction = $data;
            if(is_callable($this->decodeTransactionFunc)) {
                $this->data = ($this->decodeTransactionFunc)($data);
            } else {
                $this->decodeTransactionBody();
            }
        } elseif(is_object($data) OR is_array($data)) {
            $this->data = $data;
        } else {
            throw new \Exception('Unsupported data format');
        }
        if(empty($this->data)) {
            return NULL;
        }
        if(is_callable($this->decodeDataFunc)) {
            $this->attributes = ($this->decodeDataFunc)($this->data);
            // check that the data decoder function returned an array:
            if(!is_array($this->attributes)) {
                $this->attributes = [];
            }
        } else {
            if(!$this->validate()) {
                return NULL;
            }
            $this->loadAttributes();
            $this->isValid = TRUE;
        }
        return $this;
    }

    protected function validate()
    {
        foreach($this->validationMap() as $prop=>$validators) {
            $value = $this->getData($prop);
            if(is_string($validators)) {
                $validators = explode('|', $validators);
            }
            if(!is_array($validators)) {
                throw new \Exception('Invalid validator');
            }
            foreach($validators as $validator=>$params) {
                if(is_string($params)) {
                    $params = explode(',', $params);
                }
                if(is_string($validator)) {
                    $vfunc = 'validate' . ucfirst($validator);
                } elseif(count($params)==1) {
                    $vfunc = 'validate' . ucfirst($params[0]);
                    $params = NULL;
                } else {
                    throw new \Exception('Invalid validator configuration');
                }
                if(!method_exists($this, $vfunc)) {
                    throw new \Exception('Validator does not exist');
                }
                if(is_array($params)) {
                    if(!$this->$vfunc($value, $params)) {
                        return FALSE;
                    }
                } else {
                    if(!$this->$vfunc($value)) {
                        return FALSE;
                    }
                }
            }
        }
        return TRUE;
    }


    protected function loadAttributes()
    {
        $this->attributes = [];
        foreach($this->propertyMap() as $prop=>$transformer) {
            if(is_string($transformer)) {
                $this->attributes[$prop] = $this->getData($transformer);
            } elseif(is_array($transformer) AND count($transformer)>=2) {
                // transformer defines one or more transform functions to call:
                $value = $this->getData($transformer[0]);
                // second key is an array of functions (maybe with parameter array)
                if(is_array($transformer[1])) {
                    foreach($transformer[1] as $func=>$xform) {
                        // if $func is a string index, then it's the method name to call, and $xform is an array of parameters
                        // if $func is an integer, then $xform is the method name to call with no parameters
                        if(is_string($func)) {
                            $xfunc = 'transform' . ucfirst($func);
                        } else {
                            $xfunc = 'transform' . ucfirst($xform);
                        }
                        if(substr($xfunc,-2)=='[]') {
                            $xfunc = substr($xfunc, 0, -2) . 'Array';
                        }
                        if(!method_exists($this, $xfunc)) {
                            throw new \Exception('Transformer method "' . $xfunc . '" not implemented');
                        }
                        if(is_array($xform)) {
                            $value = $this->$xfunc($value, $xform);
                        } else {
                            $value = $this->$xfunc($value);
                        }
                    }
                } elseif(is_callable($transformer[1])) {
                    $func = $transformer[1];
                    $value = $func($value);
                }
                $this->attributes[$prop] = $value;
            }
        }
        $this->afterLoad();
    }


    /**
     * @return array
     */
    protected function propertyMap()
    {
        return [];
    }

    protected function validationMap()
    {
        return [];
    }

    protected function getData($path, $current=NULL)
    {
        $parts = explode('.', $path, 2);
        if(!isset($current)) {
            $current = $this->data;
        }
        if(!empty($parts[0])) {
            // get something from this level:
            if(is_array($current) AND array_key_exists($parts[0], $current)) {
                if(empty($parts[1])) {
                    return $current[$parts[0]];
                } else {
                    return $this->getData($parts[1], $current[$parts[0]]);
                }
            } elseif(is_object($current) AND property_exists($current, $parts[0])) {
                $attr = $parts[0];
                if(empty($parts[1])) {
                    return $current->$attr;
                } else {
                    return $this->getData($parts[1], $current->$attr);
                }
            }
        }
        return NULL;
    }

    protected function afterLoad()
    {
        return NULL;
    }


    public function jsonSerialize()
    {
        $object = new \StdClass();
        foreach($this->propertyMap() as $attr=>$whereTo) {
            if(is_array($whereTo) AND count($whereTo)>0) {
                $whereTo = $whereTo[0];
            }
            if(!is_string($whereTo)) {
                throw new \Exception('Invalid transform for "'.$attr.'"');
            }
            $wherePath = explode('.', $whereTo);
            $current = $object;
            $i = 0;
            // iterate over all array elements except the last one (which is the property name, not an object name)
            while($i < (count($wherePath)-1)) {
                $prop = $wherePath[$i];
                if(property_exists($current, $prop)) {
                    if(!is_object($current->$prop)) {
                        throw new \Exception('Property re-defined as object');
                    } // otherwise leave as-is - it's already an array.
                } else {
                    $current->$prop = new \StdClass();
                }
                $current = $current->$prop;
                $i++;
            }
            // $i should now point to the last element of the path (the property name)
            // but it is possible the path is an empty array/string
            if($i < count($wherePath) AND !empty($wherePath[$i])) {
                $prop = $wherePath[$i];
                $current->$prop = array_key_exists($attr, $this->attributes) ? $this->attributes[$attr] : NULL;
            }
        }
        return $object;
    }


    public function isValid()
    {
        return $this->isValid;
    }

    public function __get($attribute)
    {
        if(array_key_exists($attribute, $this->attributes)) {
            return $this->attributes[$attribute];
        }
        return NULL;
    }

    public function __isset($attribute)
    {
        if(array_key_exists($attribute, $this->attributes)) {
            return isset($this->attributes[$attribute]);
        }
        return FALSE;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return ApiTransaction|NULL
     */
    public function getTransaction()
    {
        return $this->transaction;
    }


    /**
     * This is where the callback os set
     * @param callable $func
     */
    public function setTransactionDecoder(callable $func)
    {
        $this->decodeTransactionFunc = $func;
    }

    public function setDataDecoder(callable $func)
    {
        $this->decodeDataFunc = $func;
    }



    /**
     * @param ApiRecorder $recorder
     * @param string $filename
     * @return bool
     */
    public function saveTransaction(ApiRecorder $recorder, $filename)
    {
        return $recorder->save($this->transaction, $filename);

    }

    /**
     * @param ApiRecorder $recorder
     * @param string $filename
     * @return static
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    static public function loadTransaction(ApiRecorder $recorder, $filename)
    {
        return new static($recorder->load($filename));
    }

    /**
     * @param mixed $value
     * @return string
     */
    protected function transformLowercase($value)
    {
        return is_string($value) ? strtolower($value) : $value;
    }

    /**
     * @param mixed $value
     * @return int
     */
    protected function transformInteger($value)
    {
        return intval($value);
    }

    /**
     * @param mixed $value
     * @param array $args
     * @return mixed
     */
    protected function transformRegex($value, $args=[])
    {
        if(is_string($value) AND is_array($args) AND array_key_exists('pattern', $args)) {
            $m = [];
            if(preg_match($args['pattern'], $value, $m)) {
                if(array_key_exists('match', $args)) {
                    if(count($m)>=$args['match']) {
                        return $m[$args['match']];
                    }
                } else {
                    return $m[0];
                }
            } else {
                return '';
            }
        }
        return $value;
    }

    /**
     * @param array $value
     * @return string[]
     */
    protected function transformStringArray($value)
    {
        if(is_array($value)) {
            $out = [];
            foreach($value as $val) {
                if(is_string($val)) {
                    $out[] = $val;
                }
            }
            return $out;
        }
        return $value;
    }

    /**
     * @param string $value
     * @param array $params
     * @return array
     * @throws \Exception
     */
    protected function transformSet($value, $params=[])
    {
        if(!is_array($params)) {
            throw new \Exception('Set transform requires a parameter array');
        }
        $delimiter = array_key_exists('delimiter', $params) ? $params['delimiter'] : ',';
        $trim = array_key_exists('trim', $params) ? $this->boolVal($params['trim']) : TRUE;
        $valid = (array_key_exists('values', $params) AND is_array($params['values'])) ? $params['values'] : NULL;
        $parts = explode($delimiter, $value);
        $output = [];
        foreach($parts as $part) {
            $text = $trim ? trim($part) : $part;
            if(!empty($valid)) {
                if(array_search($text, $valid)) {
                    $output[] = $text;
                } // otherwise not valid - don't add - maybe throw an exception?
            } else {
                // no validation - just add it:
                $output[] = $text;
            }
        }
        return $output;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    protected function validateString($value)
    {
        return is_string($value);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    protected function validateRequired($value)
    {
        return !empty($value);
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function validateUuid($value)
    {
        return Uuid::isValid($value);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    protected function boolVal($value)
    {
        if(is_string($value)) {
            $value = strtolower($value);
            if($value=='false' OR $value=='off') {
                $value = FALSE;
            }
        }
        return(boolean) $value;
    }

    /**
     * @param mixed $value
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    protected function validateEquals($value, $params=[])
    {
        if(!is_array($params)) {
            $params = ['value' => $params];
        }
        $compare = array_key_exists('value', $params) ?  $params['value'] : '';
        if(array_key_exists('case', $params)) {
            switch(strtolower($params['case'])) {
                case 'lower':
                    $value = strtolower($value);
                    break;
                case 'upper':
                    $value = strtoupper($value);
                    break;
                default:
                    throw new \Exception('Unknown case parameter');
            }
        }
        // other modifiers here...
        $strict = array_key_exists('strict', $params) ? $this->boolVal($params['strict']) : TRUE;
        return $strict ? ($value===$compare) : ($value==$compare);
    }

    /**
     * @param string $value
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    protected function validateEnum($value, $params=[])
    {
        if(is_array($params) AND !array_key_exists('values', $params)) {
            $params = ['values'=>$params];
        }
        if(!is_array($params) OR !array_key_exists('values', $params)) {
            throw new \Exception('Enum values not defined');
        }
        return (bool) array_search($value, $params['values'], TRUE);
    }

    /**
     * @param string $value
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    protected function validateSet($value, $params=[])
    {
        if(is_array($params) AND !array_key_exists('values', $params)) {
            $params = ['values'=>$params];
        }
        if(!is_array($params) OR !array_key_exists('values', $params)) {
            throw new \Exception('Set values not defined');
        }
        $delimiter = array_key_exists('delimiter', $params) ? $params['delimiter'] : ',';
        $trim = array_key_exists('trim', $params) ? $this->boolVal($params['trim']) : TRUE;
        $parts = explode($delimiter, $value);
        foreach($parts as $part) {
            if(!array_search($trim ? trim($part) : $part, $params['values'])) {
                return FALSE;
            }
        }
        return TRUE;
    }


}