<?php

namespace Bytes\ApiConnector;


class ApiPaginatedCollection extends ApiCollection
{
    /**
     * @var ApiConnector
     */
    protected $apiConnector = NULL;

    /**
     * @var string|array|NULL
     */
    protected $nextUri = NULL;

    /**
     * @var integer The page number of the current set
     */
    protected $currentPage = 0;

    /**
     * @var integer The total number of rows on previously loaded pages (not including current page)
     */
    protected $previousRowsLoaded = 0;

    /**
     * @var integer|NULL The total number of rows returned by this query if known
     */
    protected $totalCount = NULL;

    /**
     * @var string|NULL The filename to save responses to (NULL = do not save)
     */
    protected $filename = NULL;


    public function __construct(ApiConnector $apiConnector=NULL)
    {
        parent::__construct();
        $this->apiConnector = $apiConnector;
    }

    /**
     * @param string|array $uri
     */
    public function setUri($uri)
    {
        $this->nextUri = $uri;
    }


    /**
     * @return bool
     */
    protected function validatePageIsLoaded()
    {

        if (!is_array($this->items) OR !($this->transaction instanceof ApiTransaction) OR $this->transaction->hasError()) {
            // no data loaded - make all array calls fail:
            $this->currentRow = NULL;
            return FALSE;
        }
        return TRUE;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getNextPageUrl()
    {
        $url = '';
        if (is_array($this->nextUri)) {
            if (array_key_exists('uri', $this->nextUri)) {
                $url = $this->nextUri['url'];
            } elseif (array_key_exists('uri', $this->nextUri)) {
                $url = $this->nextUri['uri'];
            } elseif (count($this->nextUri) > 0 AND is_string($this->nextUri[0])) {
                $url = $this->nextUri[0];
            }
        } elseif (is_string($this->nextUri)) {
            $url = $this->nextUri;
        }
        if(!is_string($url) OR empty($url)) {
            throw new \Exception('next page link invalid');
        }
        return $url;
    }

    /**
     * @param string $methodStr
     * @return bool|string
     */
    private function normaliseMethod($methodStr)
    {
        $methodStr = strtolower($methodStr);
        if (in_array($methodStr, ['get', 'post', 'put', 'patch', 'delete'])) {
            return $methodStr;
        }
        return FALSE;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getNextPageOptions()
    {
        $options = [];
        $headers = [];
        if (is_array($this->nextUri)) {
            if (array_key_exists('headers', $this->nextUri) AND is_array($this->nextUri['headers'])) {
                $headers = $this->nextUri['headers'];
            } elseif (count($this->nextUri)>1 AND is_array($this->nextUri[1])) {
                // check if it's an options array or a headers array
                // it's assumed that an options array will have a headers key - that's a bit silly, but will do for now
                if(array_key_exists('headers', $this->nextUri[1])) {
                    $options = $this->nextUri[1];
                } else {
                    $headers = $this->nextUri[1];
                }
            }
            if (array_key_exists('options', $this->nextUri) AND is_array($this->nextUri['options'])) {
                $options = $this->nextUri['options'];
            }
        }
        // check that we've not double-set headers:
        if (!empty($headers)) {
            if (array_key_exists('headers', $options)) {
                throw new \Exception('next page headers have been set twice');
            }
            $options['headers'] = $headers;
        }
        return $options;
    }

    /**
     * @return string
     */
    protected function getNextPageMethod()
    {
        if (is_array($this->nextUri) AND array_key_exists('method', $this->nextUri)) {
            $method = $this->normaliseMethod($this->nextUri['method']);
            return $method ? $method : 'get';
        }
        return 'get';
    }

    protected function getNextPage()
    {
        if (empty($this->nextUri)) {
            throw new \Exception('next page link invalid');
        }
        $url = $this->getNextPageUrl();
        $options = $this->getNextPageOptions();
        $method = $this->getNextPageMethod();
        if ($method!='get') {
            throw new \Exception('next page method is not supported');
        }
        $this->transaction = $this->apiConnector->get($url, $options);
    }

    protected function decodePage()
    {
        $data = parent::decodePage();
        // default is we don't have a clue how to get to the next page of results
        $this->nextUri = NULL;
        // try and guess next page url using common properties, or leave it to a child class
        if(!empty($data->nextLink)) {
            $this->nextUri = $data->nextLink;
        }
        return $data;
    }

    protected function morePagesAvailable()
    {
        return !empty($this->nextUri);
    }


    /**
     * @return bool
     */
    protected function loadNextPage()
    {
        // increment previousRowsLoaded by number of rows present in the most recently loaded page (zero before loading first page):
        $this->previousRowsLoaded += empty($this->items) ? 0 : count($this->items);
        //do the GET
        $this->getNextPage();
        $this->saveResponse();
        // always save the response immediately, before validation, as we still want to record bad responses:
        $this->decodePage();
        $this->validatePageIsLoaded();
        $this->currentRow = 0;
        $this->currentPage++;
    }

    /**
     * @return integer
     */
    public function rowsLoaded()
    {
        return $this->previousRowsLoaded + count($this->items);
    }

    /**
     * @return integer
     */
    public function rowsProcessed()
    {
        return $this->key();
    }

    /**
     * @return integer
     */
    public function currentPage()
    {
        return $this->currentPage;
    }


    /**
     * called at the beginning of each loop
     */
    public function rewind()
    {
        $this->reset();
        $this->loadNextPage();
    }


    /**
     * called to get the array pointer value
     * @return integer
     */
    public function key()
    {
        return $this->previousRowsLoaded + (($this->currentRow <= 0) ? 0 : $this->currentRow);
    }

    /**
     * called to move the array pointer to the next item
     */
    public function next()
    {
        $this->currentRow++;
        // if we have processed all the rows on this page
        if ($this->currentRow >= count($this->items)) {
            //get the next one - if we need to
            if($this->morePagesAvailable()) {
                $this->loadNextPage();
            } else {
                // reset some iterator variables to ensure no more iterations are possible (valid() fails):
                $this->currentRow = -1;
            }
        }
    }

    /**
     * Clear all data and start again - called in rewind() and also if query parameters are updated
     */
    public function reset()
    {
        $this->transaction = NULL;
        $this->items = NULL;
        $this->currentRow = -1;
        $this->previousRowsLoaded = 0;
        $this->totalCount = NULL;
    }

    /**
     * @return integer|NULL
     */
    public function count()
    {
        if (is_null($this->totalCount) AND !$this->morePagesAvailable()) {
            $this->totalCount = $this->previousRowsLoaded + count($this->items);
        }
        // return freshly calculated value, or pre-existing value (NULL if there are more rows to load)
        return $this->totalCount;
    }


    /**
     * @param ApiRecorder $recorder
     * @param string $filename
     * @return bool
     */
    public function saveResponsesTo(ApiRecorder $recorder, $filename)
    {
        $this->recorder = $recorder;
        $this->filename = $filename;
    }

    /**
     * @param ApiTransaction[] $responses
     */
    public function loadFromResponses($responses)
    {
        if(is_array($responses)) {
            foreach($responses as $response) {
                if(!($response instanceof ApiTransaction)) {
                    throw new \Exception('Not a valid response object');
                }
            }
            $this->transactions = $responses;
            $this->currentTransaction = 0;
        }
    }

    /**
     * @return bool|int
     */
    protected function saveResponse()
    {
        // abort if recording not set up
        if(empty($this->filename) OR !($this->recorder instanceof ApiRecorder)) {
            return FALSE;
        }
        return $this->recorder->append($this->transaction, $this->filename);
    }


}