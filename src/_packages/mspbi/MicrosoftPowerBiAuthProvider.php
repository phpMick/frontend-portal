<?php


namespace Bytes\MicrosoftPowerBIApiConnector;


use Bytes\ApiConnector\ApiAuthenticationProvider;
use GuzzleHttp\Psr7\Request;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use TheNetworg\OAuth2\Client\Provider\Azure;

class MicrosoftPowerBiAuthProvider extends ApiAuthenticationProvider
{

    const API_RESOURCE = 'https://analysis.windows.net/powerbi/api';

    protected $requiredConfigKeys = ['clientId', 'username', 'password'];

    /**
     * @var Azure|NULL
     */
    private $oAuthClient = NULL;

    /**
     * @var AccessToken|NULL
     */
    private $oAuthToken = NULL;



    public function authenticateRequest(Request $request): Request
    {
        return $request
            ->withHeader('Authorization', 'Bearer '.$this->getAccessToken()->getToken())
            ->withHeader('Cache-Control', 'no-cache');
    }


    /**
     * @return Azure
     */
    public function getOAuthClient()
    {
        if(is_null($this->oAuthClient)) {
            $this->oAuthClient = new Azure([
                'clientId' => $this->config['clientId']
            ]);
            $this->oAuthClient->tenant = array_key_exists('tenant', $this->config) ? $this->config['tenant'] : 'common';
            $this->oAuthClient->resource = self::API_RESOURCE;
            $this->oAuthClient->scope = 'openid';
        }
        return $this->oAuthClient;
    }

    /**
     * @param string $grant_type
     * @param array $options
     * @return AccessTokenInterface
     */
    protected function requestAccessToken($grant_type='password', $options=[])
    {
        if ($grant_type=='password') {
            // check both username and password are passed - if not, use pre-configured values:
            if (!array_key_exists('username', $options) OR !array_key_exists('password', $options)) {
                $options['username'] = $this->config['username'];
                $options['password'] = $this->config['password'];
            }
        }
        return $this->getOAuthClient()->getAccessToken($grant_type, $options);
    }

    /**
     * @return AccessToken|NULL
     */
    public function getAccessToken() {
        if(($this->oAuthToken instanceof AccessToken) AND $this->oAuthToken->hasExpired()) {
            $refreshToken = $this->oAuthToken->getRefreshToken();
            if(!empty($refreshToken)) {
                $this->oauthToken = $this->requestAccessToken('refresh_token', ['refresh_token'=>$refreshToken]);
            } else {
                $this->oauthToken = NULL;
            }
        }
        // have we got a valid token?
        if($this->oAuthToken instanceof AccessToken) {
            return $this->oAuthToken;
        }

        $this->oAuthToken = $this->requestAccessToken();
        return $this->oAuthToken;
    }

    /**
     * @param AccessToken|array|object|string $token
     * @return $this
     */
    public function setAccessToken($token)
    {
        if(is_string($token)) {
            $token = json_decode($token);
        }
        if($token instanceof AccessToken) {
            $this->oauthToken = $token;
            return $this;
        }
        if(is_object($token)) {
            // generic object, check for parameters:
            $tokenArray = [];
            foreach(['access_token','resource_owner_id','refresh_token','expires'] as $attr) {
                if(!empty($token->$attr)) {
                    $tokenArray[$attr] = $token->$attr;
                }
            }
            if(array_key_exists('access_token', $tokenArray)) {
                $token = $tokenArray;
            }
        }
        if(is_array($token)) {
            try {
                $this->oauthToken = new AccessToken($token);
                return $this;
            } catch (\Exception $e) {
                // do nothing - NULL token forces everything to try logging in again
            }
        }
        $this->oauthToken = NULL;
        return $this;
    }

}