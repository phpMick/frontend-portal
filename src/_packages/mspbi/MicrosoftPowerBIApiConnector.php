<?php


namespace Bytes\MicrosoftPowerBIApiConnector;


use Bytes\ApiConnector\ApiConnector;

class MicrosoftPowerBIApiConnector extends ApiConnector
{

    const API_URI = 'https://api.powerbi.com/v1.0/';

    public function __construct(MicrosoftPowerBiAuthProvider $authProvider = NULL)
    {
        parent::__construct($authProvider);
        $this->setBaseUrl(self::API_URI);
    }

    /**
     * @param string $tenant
     * @param string $appId
     * @param string $username
     * @param string $password
     * @return MicrosoftPowerBiAuthProvider
     * @throws \Exception
     */
    static public function createAuthProvider($tenant, $appId, $username, $password)
    {
        return new MicrosoftPowerBiAuthProvider([
            'tenant' => $tenant,
            'clientId' => $appId,
            'username' => $username,
            'password' => $password
        ]);
    }

    /**
     * @return \Bytes\MicrosoftPowerBIApiConnector\Repositories\Reports
     * @throws \Exception
     */
    public function Reports()
    {
        return $this->getRepository('\Bytes\MicrosoftPowerBIApiConnector\Repositories\Reports');
    }

    /**
     * @return \Bytes\MicrosoftPowerBIApiConnector\Repositories\Groups
     * @throws \Exception
     */
    public function Groups()
    {
        return $this->getRepository('\Bytes\MicrosoftPowerBIApiConnector\Repositories\Groups');
    }

    /**
     * @return \Bytes\MicrosoftPowerBIApiConnector\Repositories\EmbedTokens
     * @throws \Exception
     */
    public function EmbedTokens()
    {
        return $this->getRepository('\Bytes\MicrosoftPowerBIApiConnector\Repositories\EmbedTokens');
    }

}