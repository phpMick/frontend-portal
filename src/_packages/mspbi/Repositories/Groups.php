<?php


namespace Bytes\MicrosoftPowerBIApiConnector\Repositories;


use Bytes\ApiConnector\ApiRepository;

class Groups extends ApiRepository
{

    /**
     * @param string|NULL $filter The oData filter string
     * @return \Bytes\ApiConnector\ApiTransaction
     */
    public function getGroups($filter=NULL)
    {
        $url = 'myorg/groups';
        if (!empty($filter)) {
            $url .= '?$filter=' . urlencode($filter);
        }
        return $this->getApiConnector()->get($url);
    }

}