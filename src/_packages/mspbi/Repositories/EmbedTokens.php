<?php


namespace Bytes\MicrosoftPowerBIApiConnector\Repositories;


use Bytes\ApiConnector\ApiRepository;

class EmbedTokens extends ApiRepository
{

    /**
     * @param string $reportId
     * @param string|NULL $groupId
     * @return \Bytes\ApiConnector\ApiTransaction
     */
    public function getReportToken($reportId, $groupId=NULL)
    {
        // currently we only support read-only
        $body = [
            'accessLevel' => 'View',
            'allowSaveAs' => FALSE
        ];
        $url = 'myorg' . (empty($groupId) ? '' : '/groups/'.$groupId) . '/reports/'.$reportId.'/GenerateToken';
        return $this->getApiConnector()->post($url, NULL, ['json'=>$body]);
    }

}