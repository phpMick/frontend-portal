<?php


namespace Bytes\MicrosoftPowerBIApiConnector\Repositories;


use Bytes\ApiConnector\ApiRepository;

class Reports extends ApiRepository
{

    /**
     * @param string|NULL $groupId
     * @return \Bytes\ApiConnector\ApiTransaction
     */
    public function getReports($groupId=NULL)
    {
        if (empty($groupId)) {
            $url = 'myorg/reports';
        } else {
            $url = 'myorg/groups/' . $groupId . '/reports';
        }
        return $this->getApiConnector()->get($url);
    }

}