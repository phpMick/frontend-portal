<?php
//app.php
//MB these config calls are returning null
//$aad_auth_url = config('app.url') . '/';
//$aad_response_url = config('app.url') . '/login/with-aad/to-';
//https://stackoverflow.com/questions/37119762/using-config-helper-from-within-config-file
// I will just get the config from the .env
$aad_auth_url = 'https://'. env('APP_HOST') . '/';
$aad_response_url = 'https://' . env('APP_HOST') . '/login/with-aad/to-';

return [

    'portal' => [
        'url' => $aad_auth_url.'portal',
        'id' => env('AAD_PORTAL_ID'),
        'secret' => env('AAD_PORTAL_SECRET'),
        'response_url' => $aad_response_url . 'portal'
    ],

    'portal2' => [
        'url' => $aad_auth_url.'portal2',
        'id' => env('AAD_PORTAL2_ID'),
        'secret' => env('AAD_PORTAL2_SECRET'),
        'response_url' => $aad_response_url.'portal2',
    ],

    'deploy' => [
        'url' => $aad_auth_url.'deploy',
        'id' => env('AAD_DEPLOY_ID'),
        'secret' => env('AAD_DEPLOY_SECRET'),
        'response_url' => $aad_response_url.'deploy',
    ],

    'azure' => [
        'url' => $aad_auth_url.'azure',
        'id' => env('AAD_AZURE_ID'),
        'secret' => env('AAD_AZURE_SECRET'),
        'response_url' => $aad_response_url.'azure',
    ],

    'o365' => [
        'url' => $aad_auth_url.'o365',
        'id' => env('AAD_O365_ID'),
        'secret' => env('AAD_O365_SECRET'),
        'response_url' => $aad_response_url.'o365',
    ],

    'partner' => [
        'url' => $aad_auth_url.'partner',
        'id' => env('AAD_PARTNER_ID'),
        'secret' => env('AAD_PARTNER_SECRET'),
        'response_url' => $aad_response_url.'partner',
        'auth_tenant' => env('AAD_PARTNER_TENANT'),
    ],

];